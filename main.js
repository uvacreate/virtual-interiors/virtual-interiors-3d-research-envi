/**
* @file Entry point for Virtual Interiors 3D Research Environment (demonstrator)
* @version 1.0
* @author Hugo Huurdeman
* @copyright GPLv3 license
*/

import 'babylonjs-loaders';
import 'babylonjs-serializers';
import 'babylonjs-post-process';
import * as BABYLON from 'babylonjs';
import * as BABYLON_GUI from 'babylonjs-gui';

//use bootstrap for front-end layout
import 'bootstrap';
//load custom bootstrap css
import 'bootstrap/dist/css/bootstrap.min.css';
//custom css, overwriting some of bootstrap's default values
import './data/css/main-style.css';

//Load various custom helper classes
import Camera from './data/js/camera.js';
import Lighting from './data/js/lighting.js';
import GUI2D from './data/js/gui-2d.js';
import GUI3D from './data/js/gui-3d.js';
import MetadataObjectStorage from './data/js/metadata-object-storage';
import DataStorage from './data/js/data-storage.js';
import AnnotationDataObject from './data/js/annotation-data-object.js';
import SPARQLDataEcartico from './data/js/sparql-data-ecartico.js';
import SPARQLDataWikidata from './data/js/sparql-data-wikidata.js';
import SPARQLDataAdamnet from './data/js/sparql-data-adamnet.js';
import SPARQLDataGoldenAgents from './data/js/sparql-data-goldenagents.js';
import InputDataHelper from './data/js/input-data-helper';

//import jquery to more easily bind actions to e.g. buttons
var $ = require('jquery');

//renderQuality sets antialiasing quality ('high'/'low')
var HARDWARE_SCALING_LEVEL_ULTRA_LOW = 2;
var HARDWARE_SCALING_LEVEL_LOW = 1;
var HARDWARE_SCALING_LEVEL_HIGH = 0.5;

//global variables
var scene;

//initialize babylonjs engine
var engine = new BABYLON.Engine(renderCanvas, true, { stencil: true });

var activeUncertaintyFilter;

//manager for selection tools for meshes
var gizmoManager;

//create global variables for sky
var skybox;

//create array of interface elements (to be able to remove them later -- mesh labels)
var twoDimensionalInterfaceElementsArr = [];

//Create GUI renderCanvas (for 2D/3D GUI)
var advancedTexture;

//[fixme] sequence# of guided tour
var guidedTourPosCnt = 0;

//glow layer for uncertainty effects
var glowLayer;
//highlight layer for e.g. showing clickable objects
var highlightLayer;

//setting for selecting objects
var selectionMode;

//variable for storing whether mesh labels are active
var meshLabelsActive = false;

//array with selected meshes in scene
var selectedMeshesInSceneArray = [];

//variables / constants for optional settings and parameters
var showFPSCounter = false;
var allowSelectingAllMeshesProperty = false;
var ENABLE_LOW_QUALITY_MODE, CUSTOM_FPS, ACTIVATE_DEBUG_MODE, CAMERA_SETTING, desiredFps, lowQualityMode;

//Initialize variables for different helper classes
var Settings, settings, lighting, gui2D, camera, metadataObjectStorage, dataStorage, gui3D;

//initialize helper class for input data
var inputDataHelper = new InputDataHelper()
//change this parameter to adapt the default app
const DEFAULT_APP_NAME = 'sample'
//default: get settings # from browser parameter, otherwise load first app
const APP_NAME = inputDataHelper.checkParameter('app', DEFAULT_APP_NAME, String);

/**
  * @desc load app settings dynamically (based on input parameter, app=[name/directory of app], or load default) 
*/
async function loadAppSettings() {
	//webpack only allows for partially dynamic imports
	//https://webpack.js.org/api/module-methods/#import
	try{ 
		const { default: AppSettings } = await import(`./data/apps/${APP_NAME}/app-settings.js`);
		Settings = AppSettings;
		return true;
	} catch(err) {
		alert('App "' + APP_NAME + '" cannot be found (' + err + ')');
		return false;
	}
}

/**
  * @desc Initialize all helper classes 
*/
function initializeClasses() {
	settings = new Settings('./data/apps/' + APP_NAME + '/');
	//initialize new lighting object
	lighting = new Lighting();
	//initialize camera object, based on settings in apps/n/settings.js
	camera = new Camera(settings);
	//initialize new GUI 2D object
	gui2D = new GUI2D(settings, camera, function () { resizeApp() });
	//initialize new object storage for metadata objects
	metadataObjectStorage = new MetadataObjectStorage(settings);
	//initialize object for data storage
	dataStorage = new DataStorage();
	//initialize new GUI 3D object
	gui3D = new GUI3D(settings, lighting, function () { toggleUncertaintyFilter() });
}

/**
  * @desc Check for various browser parameters (optional settings)
*/
function initializeBrowserParameters() {
	//possibility to set default camera (e.g. for VR headset)
	var camParam = inputDataHelper.checkParameter('cam', '', String);
	if (camParam) {
		if (camParam == 'first-person' || camParam == 'orbit' || camParam == 'vr-first-person' || camParam == 'phone-first-person' || camParam == 'cardboard-vr-first-person') {
			CAMERA_SETTING = camParam;
		}
		//otherwise, uses camera defined in app-settings.js
	}

	var lowQualityMode = false;
	desiredFps = 60;
	//to make it work on e.g. a basic iphone 7, change the scaling level upon request
	//options: lq=true (low res, low framerate)
	ENABLE_LOW_QUALITY_MODE = inputDataHelper.checkParameter('lq', false, inputDataHelper.parseBoolean);
	if (ENABLE_LOW_QUALITY_MODE) {
		HARDWARE_SCALING_LEVEL_LOW = HARDWARE_SCALING_LEVEL_ULTRA_LOW;
		lowQualityMode = true;
		desiredFps = 10;
	}

	//options: fps=## (specify framerate)
	CUSTOM_FPS = inputDataHelper.checkParameter('fps', null, parseInt);
	//only allow valid FPS values
	if (CUSTOM_FPS && CUSTOM_FPS > 0 && CUSTOM_FPS <= 60) {
		lowQualityMode = true;
		desiredFps = CUSTOM_FPS;
	}

	//add "debugmode=true" in param to true to activate babylonjs debug layer
	ACTIVATE_DEBUG_MODE = inputDataHelper.checkParameter('debugmode', false, inputDataHelper.parseBoolean);
}
 
// /**
// * @desc /locally get Draco decompression libraries (instead of being dependent on preview.babylonjs.com server), see https://forum.babylonjs.com/t/include-the-draco-files-in-release/1835/2
// * @todo Does not work in BabylonJS version >=5.0.0
// */
// BABYLON.DracoCompression.Configuration = {
// 	decoder: {
// 	  wasmUrl: 'data/lib/draco_wasm_wrapper_gltf.js',
// 	  wasmBinaryUrl: 'data/lib/draco_decoder_gltf.wasm',
// 	  fallbackUrl: 'data/lib/draco_decoder_gltf.js'
// 	}
// }

/* ***** Main application loop ***** */

/**
  * @desc Starts the application
  * @return Raw text file
  * @todo Convert CSV directly into JS Object here (instead of each time information is needed via getMeshInformationMetadataObject(..)) 
*/
async function startApplication() {
	//first, wait until app settings have been initialized
	var settingsLoaded = await loadAppSettings();
	if(!settingsLoaded) { 
		loadingScreen.innerHTML = 'Error while loading application'; return; 
	}
	
	//initialize browser params, helper classes
	initializeBrowserParameters();
	initializeClasses();

	//unhide application contents (due to contents loading before CSS is initialized)
	gui2D.showUIElement(allApplicationContents);

	//read the dataset CSVs and create featureArray with the data
	var currentData;
	try {
		currentData = await inputDataHelper.parseCSV(settings.CONTEXT_DATA_URL);
	} catch (err) {
		alert('Error loading CSV data ("' + settings.CONTEXT_DATA_URL + '") :' + err);
		loadingScreen.innerHTML = 'Error while loading application';
		return;
	}

	//update loading screen
	loadingScreen.innerHTML = 'Finished loading contextual database';

	//save the mesh information as a MetadataObject
	metadataObjectStorage.saveMeshInformationMetadata(currentData);

	//create the babylonJS scene
	createScene();
}

/**
  * @desc Creates the babylonjs 3D scene 
*/
function createScene() {
	//create the basic scene (global)
	scene = new BABYLON.Scene(engine);

	//show babylonjs scene inspector
	if (ACTIVATE_DEBUG_MODE) {
		scene.debugLayer.show();
	}

	//set render quality (e.g. antialiasing)
	setRenderQuality('low');

	//add default lights
	lighting.activateLightsSetting(scene, settings, camera.setting, 0);

	//ground plane, for gravity, to prevent falling downwards (black for the moment)
	createGroundPlane();

	//initial camera setting (if brower param cam= -> use that camera, otherwise initial setting defined in settings.js)
	camera.set(scene, renderCanvas, CAMERA_SETTING);

	//define (global) glow layer for uncertainty effects
	//blurkernelsize: lower means lower quality
	glowLayer = new BABYLON.GlowLayer("glow", scene, { /*blurKernelSize : 16, mainTextureRatio: 0.4*/ });
	glowLayer.isEnabled = false;

	//highlight layer for e.g. showing clickable objects
	//https://forum.babylonjs.com/t/control-highlightlayer-alpha/16921/2
	highlightLayer = new BABYLON.HighlightLayer("hl", scene);
	highlightLayer.isEnabled = false;
	highlightLayer.outerGlow = false;
	highlightLayer.blurHorizontalSize = 1;

	//create fullscreen 2D UI via AdvancedDynamicTexture; to add optional elements in 3D space such as labels
	advancedTexture = BABYLON_GUI.AdvancedDynamicTexture.CreateFullscreenUI("ui1");

	//update loading screen
	loadingScreen.innerHTML = 'Loading meshes (2) from: ' + settings.MESH_NAME;
	BABYLON.SceneLoader.ShowLoadingScreen = true;
	BABYLON.SceneLoader.loggingLevel = BABYLON.SceneLoader.DETAILED_LOGGING;	
	BABYLON.SceneLoader.Append(settings.MESH_DIRECTORY, settings.MESH_NAME, scene, async function (newMeshes) {
		//update loading screen
		loadingScreen.innerHTML = 'Finished loading scene meshes (2)';
		gui2D.hideUIElement(loadingScreen);

		//preprocess the meshes (merge them if necessary)
		if (settings.PROCESS_MESH_IDS) {
			//merge meshed based on their prefix in the metadata
			//creates issue with visible line
			groupMeshesByPrefix(newMeshes.meshes);
		}

		//add interaction to (certain) meshes in glb scene
		addInteraction(newMeshes);

		//define which objects can be clicked (based on Google sheet column)
		setDefaultObjectClickableProperties(newMeshes.meshes);

		//change the transparency of certain meshes (based on Google sheet column)
		setObjectTransparencyProperties(newMeshes.meshes);

		//add functionality to Bootstrap buttons
		createUI(newMeshes, advancedTexture);

		//show introductory info text (viewer controls), if sidebar can be shown (i.e. larger viewports, desktop/tablet)
		if (gui2D.sideBarIsVisible()) {
			gui2D.showInfoBoxAlert('introText', true);
		}
		//[checkme!] If this works correctly (20210406)
		if (!ENABLE_LOW_QUALITY_MODE) {
			//default setting for object highlighting (onOver), disable for VR, since this causes issues in current version of babylonjs (5.0 alpha 3)
			setObjectHighlighting(scene.meshes, true);
		} else {
			setObjectHighlighting(scene.meshes, false);
		}

		//start main render loop
		runRenderLoop();

	}, gui2D.getProgress);
}

/**
  * @desc Add behaviors to all elements of the UI
  * @param scene {BABYLON.Scene}
  * @param advancedTexture {BABYLON_GUI.AdvancedDynamicTexture}
*/
function createUI(scene, advancedTexture) {
	//only show sidebar in certain screen sizes
	if(gui2D.sideBarCanBeShown()) {
		gui2D.initializeSideBarContents();
		//gui2D.showSideBar();
	} else {
		gui2D.hideSideBar();
	}

	if(!settings.SHOW_CONFIDENCE_VALUES) {
		//hide menu options in Analysis views menu
		gui2D.hideUIElement(buttonCertaintyIndexObject);
		gui2D.hideUIElement(buttonCertaintyIndexLocation);
	}

	//set button labels
	//rightmost navigation

	gui2D.renameButton(buttonGeneral, settings.BUTTON_GENERAL);
	gui2D.renameButton(buttonList, settings.BUTTON_LIST);
	gui2D.renameButton(buttonInspector, settings.BUTTON_INSPECTOR);
	//accordeon navigation"
	var chevronDown = ' <i class="fas fa-chevron-down chevronStyle"></i>';
	gui2D.renameButton(buttonReconstruction, settings.BUTTON_RECONSTRUCTION + chevronDown);
	gui2D.renameButton(buttonComparisons, settings.BUTTON_COMPARISONS + chevronDown);
	gui2D.renameButton(button3DModel, settings.BUTTON_3D_MODEL + chevronDown);

	//create function to detect viewport changes (to dynamically show sidebar)
	$(function () {
		let viewport = gui2D.getViewport()
		let debounce
		$(window).resize(() => {
			debounce = setTimeout(() => {
				if(gui2D.sideBarCanBeShown()) {
					gui2D.initializeSideBarContents();
					gui2D.showSideBar();
					//remove 3d UI panels as well
					gui3D.removeAllPanelsFrom3DGUI();
				}
			}, 500)
		})
		//run when page loads
		$(window).trigger('newViewport', viewport);
	});

	//reset input field
	inputFieldFilterInput.value = '';

	//show "about" modal when loading application
	textGeneralAbout.innerHTML = settings.DEFAULT_TAB_GENERAL_ABOUT;
	$('#demoIntroductionOverlay').modal('show');

	//Create a list of objects in the scene
	createObjectsInSceneList(scene.meshes, textObjectList);

	//make the list of objects in the scene respond to entered words in input field
	inputFieldFilterInput.oninput = function () {
		filterObjects();
	}

	//update which images are clickable
	gui2D.updateJQueryClickableImages();
	//update all dynamic Bootstrap tooltips
	gui2D.updateBootstrapTooltips();

	//remove content from media player (audio, video) modal upon closing it
	$('#mediaOverlay').on('hidden.bs.modal', function () {
		modalMediaPlayerBody.innerHTML = '';
	})

	//add behavior to "toggle sidebar" pill button (hide sidebar) [][][X]
	toggleSidebarHiddenButton.addEventListener('click', function () { gui2D.toggleSideBar() });

	//add interaction to bootstrap buttons
	//fullscreen [X][][] 
	//works on most platforms; also >= iOS 13 on iPad (not iPhone)
	buttonFullScreen.addEventListener('click', function () { toggleFullScreen() });
	buttonHelp.addEventListener('click', function () { gui2D.showInfoBoxAlert('introText', true); });

	//Camera [X][][]
	//apply "selected" style to applied to current default camera
	if(camera.currentSetting == 'first-person') { gui2D.setSelectedStyleToButton(buttonChangeCameraFirstPerson);
	} else if(camera.currentSetting == 'orbit') { gui2D.setSelectedStyleToButton(buttonChangeCameraOrbit);
	} else if(camera.currentSetting == 'vr-first-person') { gui2D.setSelectedStyleToButton(buttonChangeCameraFirstPersonVR);
	} else if(camera.currentSetting == 'phone-first-person') { gui2D.setSelectedStyleToButton(buttonChangeCameraFirstPersonPhone);
	} else if(camera.currentSetting == 'cardboard-vr-first-person') { gui2D.setSelectedStyleToButton(buttonChangeCameraFirstPersonCardboardVR); 
	}
	var cameraButtons = [buttonChangeCameraFirstPerson, buttonChangeCameraOrbit, buttonChangeCameraFirstPersonVR, buttonChangeCameraFirstPersonPhone, buttonChangeCameraFirstPersonCardboardVR];
	buttonChangeCameraFirstPerson.addEventListener('click', function () { gui2D.setButtonSelectionStyles(this, cameraButtons); camera.set(scene, renderCanvas, 'first-person'); gui2D.showSideBar();});
	buttonChangeCameraOrbit.addEventListener('click', function () { gui2D.setButtonSelectionStyles(this, cameraButtons); camera.set(scene, renderCanvas, 'orbit'); gui2D.showSideBar();  });
	buttonChangeCameraFirstPersonVR.addEventListener('click', function () { gui2D.setButtonSelectionStyles(this, cameraButtons); camera.set(scene, renderCanvas, 'vr-first-person'); gui2D.hideSideBar(); });
	buttonChangeCameraFirstPersonPhone.addEventListener('click', function () { gui2D.setButtonSelectionStyles(this, cameraButtons); camera.set(scene, renderCanvas, 'phone-first-person'); gui2D.hideSideBar(); });
	buttonChangeCameraFirstPersonCardboardVR.addEventListener('click', function () { gui2D.setButtonSelectionStyles(this, cameraButtons); camera.set(scene, renderCanvas, 'cardboard-vr-first-person'); gui2D.hideSideBar(); });

	//Lighting settings buttons [X][][]
	//apply "selected" style to default button
	gui2D.setSelectedStyleToButton(buttonLightsSetting0);
	var lightingButtons = [buttonLightsSetting0, buttonLightsSetting1 ,buttonLightsSetting2 ,buttonLightsSetting3];
	buttonLightsSetting0.addEventListener('click', function () { gui2D.setButtonSelectionStyles(this, lightingButtons); lighting.activateLightsSetting(scene, settings, camera.currentSetting, 0); });
	buttonLightsSetting1.addEventListener('click', function () { gui2D.setButtonSelectionStyles(this, lightingButtons); lighting.activateLightsSetting(scene, settings, camera.currentSetting, 1); });
	buttonLightsSetting2.addEventListener('click', function () { gui2D.setButtonSelectionStyles(this, lightingButtons); lighting.activateLightsSetting(scene, settings, camera.currentSetting, 2); });
	buttonLightsSetting3.addEventListener('click', function () { gui2D.setButtonSelectionStyles(this, lightingButtons); lighting.activateLightsSetting(scene, settings, camera.currentSetting, 3); });

	//Analysis layers [X][][]
	//apply "selected" style to default button
	gui2D.setSelectedStyleToButton(buttonCertaintyIndexOff);
	var analysisViewButtons = [buttonCertaintyIndexOff, buttonCertaintyIndexObject, buttonCertaintyIndexObjectEmbedded, buttonCertaintyIndexLocation, buttonCertaintyIndexObjectLocationEmbedded, buttonColorCodingCategories, buttonObjectHideOthers]
	buttonCertaintyIndexOff.addEventListener('click', function () {
		gui2D.setButtonSelectionStyles(this, analysisViewButtons);
		resetActiveVisualFilters();
		gui2D.hideInfoBoxAlert();
	});
	buttonCertaintyIndexObject.addEventListener('click', function () {
		gui2D.setButtonSelectionStyles(this, analysisViewButtons);
		setUncertaintyFilter('object');
		gui2D.showInfoBoxAlert('object', true);
	});
	//button embedded in obj details panel (merge with above)
	buttonCertaintyIndexObjectEmbedded.addEventListener('click', function () {
		gui2D.setButtonSelectionStyles(this, analysisViewButtons);
		toggleUncertaintyFilterByType('object');
	});
	buttonCertaintyIndexLocation.addEventListener('click', function () {
		gui2D.setButtonSelectionStyles(this, analysisViewButtons);
		setUncertaintyFilter('location');
		gui2D.showInfoBoxAlert('location', true);
	})
	//button embedded in obj details panel (merge with above)
	buttonCertaintyIndexObjectLocationEmbedded.addEventListener('click', function () {
		gui2D.setButtonSelectionStyles(this, analysisViewButtons);
		toggleUncertaintyFilterByType('location');
	})
	//color coding based on categories (currently missing: legenda)
	buttonColorCodingCategories.addEventListener('click', function () {
		gui2D.setButtonSelectionStyles(this, analysisViewButtons);
		gui2D.showResetViewAlert();
		setObjectCategoryFilter();
		gui2D.hideInfoBoxAlert();
	});
	//"x-ray" view
	buttonObjectHideOthers.addEventListener('click', function () {
		gui2D.setButtonSelectionStyles(this, analysisViewButtons);
		var selectedMesh = getSelectedMesh();
		if (selectedMesh != null) {
			//transparency for all meshes except selected one
			hideOtherMeshes(scene.meshes, selectedMesh.name, 0.2);
		} else {
			//transparency for all meshes 
			hideOtherMeshes(scene.meshes, null, 0.2);
		}
		gui2D.hideInfoBoxAlert();
		gui2D.showResetViewAlert();
	});

	//Settings [X][][]
	buttonHighlightingOff.addEventListener('click', function () { setObjectHighlighting(scene.meshes, false) });
	buttonHighlightingDynamic.addEventListener('click', function () { setObjectHighlighting(scene.meshes, true) });
	buttonMeshLabelsOff.addEventListener('click', function () { removeLabelsFromMeshes(advancedTexture); });
	buttonMeshLabelsOnSelection.addEventListener('click', function () { toggleMeshLabels(enableAnnotatedMeshLabels); });
	buttonMeshLabelsOn.addEventListener('click', function () { toggleMeshLabels(enableMeshNameLabels); });
	buttonFPSCounter.addEventListener('click', function () { showFPSCounter = !showFPSCounter; });
	buttonDebugMenu.addEventListener('click', function () { gui2D.toggleDebugMenu(); });

	//Object actions [][X][]
	buttonObjectOpacity0.addEventListener('click', function () { setMeshTransparency(getSelectedMesh(), 0); showResetViewAlert(); });
	buttonObjectOpacity20.addEventListener('click', function () { setMeshTransparency(getSelectedMesh(), 0.2); showResetViewAlert(); });
	buttonObjectOpacity50.addEventListener('click', function () { setMeshTransparency(getSelectedMesh(), 0.5); showResetViewAlert(); });
	buttonObjectOpacity100.addEventListener('click', function () { setMeshTransparency(getSelectedMesh(), 1); showResetViewAlert(); });
	buttonObjectCustomize.addEventListener('click', function () { activateGizmoManagerForMesh(getSelectedMesh(), true); gui2D.showInfoBoxAlert('customizeMesh', true); }.bind(this));
	buttonObjectReset.addEventListener('click', function () {
		resetActiveVisualFilters();
	});
	buttonObjectRecolor.addEventListener('click', function () { recolorMesh(getSelectedMesh()); });
	buttonObjectDelete.addEventListener('click', function () { deleteMesh(getSelectedMesh()); });

	buttonCloseObject.addEventListener('click', function () { resetActiveVisualFilters(); clearMeshSelections(); });

	buttonCameraPositionLog.addEventListener('click', function () {
		var camera = scene.activeCamera;
		console.log('[log] camera target: ' + camera.getTarget());
		console.log('[log] camera position: ' + camera.position);
		console.log('[log] camera rotation: ' + camera.rotation);
		console.log('var position = new BABYLON.Vector3(' + camera.position.x.toFixed(2) + ', ' + camera.position.y.toFixed(2) + ', ' + camera.position.z.toFixed(2) + ');');
		console.log('var rotation = new BABYLON.Vector3(' + camera.rotation.x.toFixed(2) + ', ' + camera.rotation.y.toFixed(2) + ', ' + camera.rotation.z.toFixed(2) + ');');
		console.log('var target = new BABYLON.Vector3(' + camera.getTarget().x.toFixed(2) + ', ' + camera.getTarget().y.toFixed(2) + ', ' + camera.getTarget().z.toFixed(2) + ');');
	});

	buttonGravity.addEventListener('click', function () {
		camera.disableGravity(scene, scene.activeCamera);
	});

	//make sure that the sidebar appears (when sidebar has been hidden by the user). Only applies to larger screen sizes where this menu appears.
	tabHome.addEventListener('click', function () {
		if (!gui2D.sideBarIsVisible() && gui2D.sideBarCanBeShown()) {
			gui2D.showSideBar();
		}
	});
	tabObjectList.addEventListener('click', function () {
		if (gui2D.sideBarIsVisible() && gui2D.sideBarCanBeShown()) {
			gui2D.showSideBar();
		}
	});
	tabObject.addEventListener('click', function () {
		if (!gui2D.sideBarIsVisible() && gui2D.sideBarCanBeShown()) {
			gui2D.showSideBar();
		}
	});

	buttonStartGuidedTour.addEventListener('click', function () {
		//start a guided tour, showing different elements of the model
		activateGuidedTour(true, true);
	});

	buttonGuidedTourPreviousPosition.addEventListener('click', function () {
		activateGuidedTour(false, false);
	});

	buttonGuidedTourNextPosition.addEventListener('click', function () {
		activateGuidedTour(true, false);
	});

	buttonGuidedTourMoreInfo.addEventListener('click', function () {
		gui2D.toggleSideBar();
	});

	buttonRenderQuality.addEventListener('click', function () {
		//set antialiasing quality
		toggleRenderQuality();
	});

	buttonStartSceneOptimizer.addEventListener('click', function () {
		startSceneOptimizer();
	});

	buttonSaveAnnotation.addEventListener('click', function (evt) {
		saveAnnotation(evt);
	});

	alertAnnotationField.addEventListener('click', function (evt) {
		gui2D.hideAnnotationAlert();
		var id = getSelectedMeshID();
		var annotationObj = new AnnotationDataObject(dataStorage, id);
		annotationObj.userAnnotation_text = null;
	});

	//various selection modes, mainly for debugging purposes
	buttonSelectionModeHideOthers.addEventListener('click', function () {
		//set object selection mode to hiding (making transparent) other items than the selected object
		setSelectionMode("transparent-other-meshes");
	});
	buttonSelectionModeHideMesh.addEventListener('click', function () {
		//set object selection mode to hiding (making transparent) other items than the selected object
		setSelectionMode("hide-mesh");
	});
	buttonSelectionModeTransparentMesh1.addEventListener('click', function () {
		//set object selection mode to hiding (making transparent) other items than the selected object
		setSelectionMode("transparent-mesh-1");
	});
	buttonSelectionModeTransparentMesh2.addEventListener('click', function () {
		//set object selection mode to hiding (making transparent) other items than the selected object
		setSelectionMode("transparent-mesh-2");
	});
	buttonSelectionModeHighlightMesh.addEventListener('click', function () {
		//highlight one mesh (glowfilter)
		setSelectionMode("highlight-mesh-all");
	});
	buttonSelectionModeActivateGizmo.addEventListener('click', function () {
		//select any mesh in the scene and change its properties (position, scaling etc)
		setSelectionMode("activate-gizmo");
	});

	buttonCollisionDetection.addEventListener('click', function () {
		//activate collision detection of (certain) meshes
		camera.activateCollisionDetection(scene.meshes);
	});

	buttonEnableAmbientOcclusion.addEventListener('click', function () {
		//(trying to) optimize the rendering performance
		camera.toggleAmbientOcclusion(scene, scene.activeCamera);
	});

	buttonEnableLensEffects.addEventListener('click', function () {
		//(trying to) optimize the rendering performance
		camera.toggleLensEffects(scene, scene.activeCamera);
	});

	buttonSelectionModeRecolor.addEventListener('click', function () {
		//random recolor of all meshes
		setSelectionMode("recolor-all-meshes");
	});

	buttonSelectionModeToggleSelectLockedMeshes.addEventListener('click', function () {
		//allow for selecting meshes which are normally not pickable (i.e. not part of the annotated set of meshes, or specified as not being clickable in contextual data)
		allowSelectingAllMeshesProperty = !allowSelectingAllMeshesProperty;
		setDefaultObjectClickableProperties(scene.meshes);
	});

	buttonToggleDoNotSetVRCameraDefaults.addEventListener('click', function () {
		//do not change the camera position/target to VR defaults when entering VR mode
		camera.doNotSetDefaultVRCameraProperty = !camera.doNotSetDefaultVRCameraProperty;
	});

	buttonSelectionModeShowMesh.addEventListener('click', function () {
		setSelectionMode("show-mesh");
	});
	buttonSelectionModeDeleteMesh.addEventListener('click', function () {
		setSelectionMode("delete-mesh");
	});
	buttonSelectionModeWireframes.addEventListener('click', function () {
		setSelectionMode("wireframes");
	});
	buttonSelectionModeReset.addEventListener('click', function () {
		setSelectionMode("highlight-mesh");
		clearMeshSelections();
	});

	//remove the (search) filter from the scene
	alertTextFilterInput.addEventListener('click', function () {
		gui2D.resetTextualSearchFilter();
		filterObjects();
	});
	//remove the (visual) filter from the scene
	alertResetView.addEventListener('click', function () {
		resetActiveVisualFilters();
	});
	//remove the (visual) filter from the scene when button in infobox alert is clicked
	alertInfoBoxClose.addEventListener('click', function () {
		resetActiveVisualFilters();
	});
	//remove the guided tour info
	alertGuidedTourInfoBoxClose.addEventListener('click', function () {
		//set camera/lights to default, reset filters
		resetActiveVisualFilters();
		lighting.activateLightsSetting(scene, settings, camera.currentSetting, 0);
		camera.set(scene, renderCanvas);
		gui2D.hideGuidedTourInfoAlert();
	});
}

/* ***** Guided tour-related functions ***** */

/**
  * @desc Activate a guided tour (defined in settings.js)
  * @param guidedTourPosition {int} position in guided tour to move to
  * @notes uses global var guidedTourPosCnt
*/
function activateGuidedTour(forwardGuidedTourDirection, initiateGuidedTour) {
	var guidedTourArray = settings.getGuidedTourArray(scene);
	var guidedTourLength = settings.getGuidedTourArray(scene).length;

	if (guidedTourPosCnt < 0 || guidedTourPosCnt > guidedTourArray.length) {
		console.log('[err] ' + guidedTourPosCnt + ' is not a valid guided tour position');
		return;
	}

	//advance pointer for guided tour
	if (initiateGuidedTour) {
		//initial position after "Guided tour" button is pressed (do not advance #)
		guidedTourPosCnt = 0;
	} else {
		if (forwardGuidedTourDirection) {
			//move forward in tour positions
			console.log('[log] guided tour: moving forward');
			if (guidedTourPosCnt < guidedTourArray.length - 1) {
				guidedTourPosCnt++;
			} else {
				guidedTourPosCnt = 0;
			}
		} else {
			//move backward in tour positions
			console.log('[log] guided tour: moving backward');
			if (guidedTourPosCnt > 0) {
				guidedTourPosCnt--;
			} else {
				//go to last tour position
				guidedTourPosCnt = guidedTourArray.length - 1;
			}
		}
	}

	//get the info for the current guided tour pos
	var currTourPosInfo = guidedTourArray[guidedTourPosCnt];
	//cancel if no info found
	if (currTourPosInfo == null) {
		console.log('[err] no guided tour data found');
		return;
	}

	//variable to specify which direction guided tour should have
	if (forwardGuidedTourDirection == null) {
		console.log('[warning] no guided tour direction specified');
	}

	//enable/disable lens effects --> ambient occlusion
	/*if(currTourPosInfo.lensEffectsEnabled == true) {
		//disableAmbientOcclusion();
		if(ssao==null) {
			camera.enableAmbientOcclusion(scene.activeCamera);
		}
	} else {
		camera.disableAmbientOcclusion();
	}*/

	if (camera.currentSetting == 'orbit') {
		//if in orbit mode, set the camera to first-person to make animations work correctly
		camera.set(scene, renderCanvas, 'first-person');
	}

	//set the next viewpoint, based on ext settings js
	setGuidedTourViewpoint(
		currTourPosInfo.cameraPosition, currTourPosInfo.cameraRotation, currTourPosInfo.lightsSetting, currTourPosInfo.selectedMeshInScene, currTourPosInfo.selectionMode, currTourPosInfo.clearFilter, currTourPosInfo.meshArrToHide, currTourPosInfo.meshArrToShow
	);

	var textToDisplay = '';
	var showReadMoreButton = false;
	//if a mesh has been selected, show text associated with that mesh
	if (currTourPosInfo.selectedMeshInScene != null) {
		var infObj = getMeshInformationMetadataObject(getIDFromMeshName(currTourPosInfo.selectedMeshInScene));
		if (infObj != null) {
			var txt = infObj.title.value; //+ '<br>';
			if (txt != '') {
				textToDisplay = '<span class="mediumText">Highlighted object: ' + txt + '</span><br>';
				//show the "read more" button for more info on the highlighted object
				showReadMoreButton = true;
			}
		}
	}

	//if no text has been found, show text associated to guided tour position
	if (currTourPosInfo.textToShow != null) {
		textToDisplay += currTourPosInfo.textToShow;
	}
	//show info box (with or without text for the current position)
	if (textToDisplay != '') {
		//show alert with Guided tour information
		gui2D.showGuidedTourInfoAlert(textToDisplay, showReadMoreButton, guidedTourPosCnt, guidedTourLength);
	} else {
		gui2D.showGuidedTourInfoAlert(null, showReadMoreButton, guidedTourPosCnt, guidedTourLength);
	}

	//hide the sidebar; if a selected mesh is specified in the guided tour array it will be shown again later
	gui2D.hideSideBar();
}

/**
  * @desc Set viewpoint for guided tour (defined in settings.js)
  * @param guidedTourPosition {int} position in guided tour to move to
*/
function setGuidedTourViewpoint(camPos, camRot, lightsSetting, selectedMeshInScene, selectionMode, clearFilter, meshArrToHide, meshArrToShow) {
	//reset search filters and reset search function
	gui2D.resetTextualSearchFilter();
	filterObjects();
	//set default transparencies
	setObjectTransparencyProperties(scene.meshes);
	//clear previously selected meshes
	clearMeshSelections(true);

	if (!camPos) {
		console.log('[err] no camera position specified');
		return;
	}

	if (lightsSetting != null) {
		lighting.activateLightsSetting(scene, settings, camera.currentSetting, lightsSetting);
	}

	//if clearFilter == true, we will clear selected filters
	if (clearFilter) {
		clearSelectionFilter(scene.meshes);
	}

	//manually enable / disable meshes (for PdG)
	if (meshArrToHide) {
		for (var i = 0; i < meshArrToHide.length; i++) {
			setMeshTransparency(scene.getMeshByName(meshArrToHide[i]), 0);
		}
	} else if (meshArrToShow) {
		for (var i = 0; i < meshArrToShow.length; i++) {
			setMeshTransparency(scene.getMeshByName(meshArrToShow[i]), 1);
		}
	}

	//hide the sidebar in the guided tour .. 
	if (selectedMeshInScene != null) {
		if (selectionMode != null) {
			//use selection mode specified in tour
			selectMeshInScene(selectedMeshInScene, selectionMode);
		} else {
			//use default selection mode
			selectMeshInScene(selectedMeshInScene, getSelectionMode());
		}
	}

	//in VR mode, only animate the position
	if (camera.xrIsActive) {
		camera.animateViewPoint(scene.activeCamera, camPos);
	} else {
		camera.animateViewPoint(scene.activeCamera, camPos, camRot);
	}
}

/* ****** UI related functions ****** */

/**
  * @desc Reset any visual filters currently active (and retain previously selected object)
*/
function resetActiveVisualFilters() {
	var currMesh = getSelectedMesh();
	//clear selections to revert to standard view of meshes
	clearSelectionFilter(scene.meshes);
	//make sure uncertainty filter is off
	if (getActiveUncertaintyFilter() != null) {
		hideUncertaintyFilter(scene.meshes);
	}
	if (currMesh != null) {
		//reselect the mesh that was previously selected (i.e. before activating the visual filter)
		selectMeshInScene(currMesh.name);
	}
	//hide the button itself
	gui2D.hideResetViewAlert();
	gui2D.hideInfoBoxAlert();
}

/**
  * @desc Save an annotation to the linked data storage
  * @todo revise function
*/
function saveAnnotation(evt) {
	var id = getSelectedMeshID();
	var annObj = new AnnotationDataObject(dataStorage, id);
	//for security purposes, convert user input to String
	annObj.userAnnotation_text = inputFieldAnnotationText.value;
	showAnnotationAlertForID(id);
}

/**
 * @desc Show a specific annotation (based on ID)
 * @param id {Str} ID of currently selected object 
 * @todo revise function
*/
function showAnnotationAlertForID(id) {
	var annotationObj = new AnnotationDataObject(dataStorage, id);
	var annotationText = annotationObj.userAnnotation_text;
	if (annotationText != null) {
		alertAnnotationField.innerHTML = annotationText + ' &nbsp<i class="fas fa-window-close"></i>';
		gui2D.showAnnotationAlert();
	} else {
		gui2D.hideAnnotationAlert();
	}
}

/**
  * @desc Create a <ul> list of objects in the scene to display in the "general information" tab. All objects which have a metadata values are added to the list (earlier only clickable objects)
  * @param meshes {arr}
  * @param interfaceItemID {DOM_element}
*/
function createObjectsInSceneList(meshes, interfaceItemID) {
	if (metadataObjectStorage.numberOfMetadataObjects > 0) {
		var htmlText = '';
		htmlText += '<ul id="ulObjectList">';

		var meshIDArr = [];

		for (var i = 0; i < meshes.length; i++) {
			var meshID = getIDFromMeshName(meshes[i].name);
			var infObj = getMeshInformationMetadataObject(meshID);
			//[quick fix] only add a certain ID once (since multiple meshes might be attached)
			if (infObj && meshIDArr.indexOf(meshID) == -1) {
				var currentListItemID = infObj.id.value;
				var currentListItemLabel = infObj.title.value;
				var currentListItemCategory = infObj.category.value;
				var currentListItemDesc = infObj.description.value;
				if (currentListItemDesc.length > 50) {
					currentListItemDesc = currentListItemDesc.substring(0, 45) + ' (...)';
				}
				htmlText += '<li>';
				htmlText += '<a href="#" class="boldText" id="' + currentListItemID + '">' + currentListItemLabel + ' (' + currentListItemCategory + ') </a>';
				htmlText += '</li>';
				meshIDArr.push(meshID);
			}
		}
		htmlText += '</ul>';
		interfaceItemID.innerHTML = htmlText;

		meshIDArr = [];

		//add eventlisteners to list items
		for (var i = 0; i < meshes.length; i++) {
			var meshID = getIDFromMeshName(meshes[i].name);
			var infObj = getMeshInformationMetadataObject(getIDFromMeshName(meshes[i].name));
			//[quick fix] only add a certain ID once (since multiple meshes might be attached)
			if (infObj && meshIDArr.indexOf(meshID) == -1) {
				//only add if clickable element
				var currentListItemLabel = infObj.id.value;
				var currentListItemID = document.getElementById(currentListItemLabel);
				currentListItemID.addEventListener('click', function (evt) {
					//fix for chrome/safari: use srcElement.id instead of evt.explicitOriginalTarget.id, which only works in firefox
					var id = evt.srcElement.id;
					if (id) {
						selectMeshInScene(id);
					}
				}, false);
				meshIDArr.push(meshID);
			}
		}
	} else {
		interfaceItemID.innerHTML = '[Error] No array found';
	}
}

/**
  * @desc Add labels to a set of objects (based on their clickable value)
  * @param meshes {arr}
  * @param advancedTexture {obj} 
  * @param labelSetting {string} ('annotated-objects', 'all-labels')
*/
function addLabelsToMeshes(meshes, advancedTexture, labelSetting) {
	//2D UI labels, linked to meshes (add)
	//these will stay the same size, regardless of position in scene

	//first remove all previous labels from the UI
	removeLabelsFromMeshes(advancedTexture);

	for (var i = 0; i < meshes.length; i++) {
		let currMesh = meshes[i];
		var infObj = getMeshInformationMetadataObject(getIDFromMeshName(currMesh.name));

		//quick fix: check for "primitives" to avoid multiple labels for the same object.. //only support ES6 and up
		if (infObj && labelSetting != 'all-labels') {
			if (labelSetting == 'annotated-objects' && !currMesh.name.includes('primitive')) {
				show2DLabel(currMesh, advancedTexture, labelSetting, infObj.title.value);
			}
		} else if(labelSetting == 'all-labels') {
			//also show mesh name (if no label available)
			show2DLabel(currMesh, advancedTexture, labelSetting, currMesh.name);
		}
	}
}

/**
  * @desc Show a 2D label for a single mesh
  * @param mesh {BABYLON.Mesh}
  * @param advancedTexture {obj}
  * @param labelSetting {str} ('annotated-objects', 'all-labels')
  * @param currentLabel {str} [label to add]
*/
function show2DLabel(mesh, advancedTexture, labelSetting, currentLabel) {
	var label = new BABYLON_GUI.Rectangle("label for " + mesh.name);

	var randomColor = "#";
	for (var j = 0; j < 6; j++) {
		randomColor += Math.floor(Math.random() * 10);
	}

	label.alpha = 0.5;
	label.cornerRadius = 20;
	label.thickness = 1;
	label.linkOffsetY = 30;
	label.fontSize = '24pt';

	//store the interface elements in a global array, since we don't know yet how to access them..
	twoDimensionalInterfaceElementsArr.push(label);
	advancedTexture.addControl(label);

	label.linkWithMesh(mesh);

	var labelText = new BABYLON_GUI.TextBlock();
	labelText.color = "white";
	labelText.textWrapping = true;
	label.addControl(labelText);

	if (labelSetting == 'all-labels') {
		labelText.text = mesh.name;
		label.height = '30px';
		label.width = '400px';

		label.background = randomColor;
		if (labelText.text.length > 25) {
			labelText.text = labelText.text.substring(0, 25);
		}
	} else if (labelSetting == 'annotated-objects') {
		//only annotated objects
		labelText.text = currentLabel; //transcriptie
		label.height = '60px';
		label.width = '200px';
		label.fontSize = '24pt';
		label.background = '#000000';
	}
}

/**
  * @desc Toggle whether interface is shown in fullscreen or not
*/
function toggleFullScreen() {
	//firefox implementation
	var doc = window.document;
	var docEl = doc.documentElement;

	var requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen;
	var cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen;

	if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
		requestFullScreen.call(docEl);
	} else {
		cancelFullScreen.call(doc);
	}
}

/**
  * @desc Toggle visibility of mesh labels (annotated or off). Uses integrated BJS functionality.
  * @param annotatedMeshLabelsFunction {function} Function which enables mesh labels (i.e. enableAnnotatedMeshLabels() or enableMeshLabels())
*/
function toggleMeshLabels(annotatedMeshLabelsFunction) {
	if (meshLabelsActive == false) {
		annotatedMeshLabelsFunction();
	} else {
		removeLabelsFromMeshes(advancedTexture);
	}
	meshLabelsActive = !meshLabelsActive;
}

/**
  * @desc Enable mesh labels based on object types (e.g. object category)
*/
function enableAnnotatedMeshLabels() {
	addLabelsToMeshes(scene.meshes, advancedTexture, 'annotated-objects');
}

/**
  * @desc Enable mesh labels based on mesh IDs (for debugging)
*/
function enableMeshNameLabels() {
	addLabelsToMeshes(scene.meshes, advancedTexture, 'all-labels');
}

/**
  * @desc Bool indicating if guided tour is currently active
*/
function guidedTourIsActive() {
	if (window.getComputedStyle(spanGuidedTourControls).display == 'block') {
		return true;
	} else {
		return false;
	}
}

/****************************** Mesh-related properties ********************************** */

/**
  * @desc Deactivate gizmo manager (to manage resizing etc of a mesh)
*/
function deactivateGizmoManager() {
	if (gizmoManager) {
		gizmoManager.attachToMesh();
		gizmoManager.attachableMeshes = [];
		gizmoManager.boundingBoxGizmoEnabled = false;
	}
}

/**
  * @desc Return a mesh group
*/
function getMeshGroupByMeshName(selectedMeshName) {
	return scene.getMeshByName('group_' + getIDFromMeshName(selectedMeshName));
}

/**
  * @desc Gizmo manager makes it possible to move meshes
  * @todo issues: currently not including _primitives (see e.g. BG radio) when moving an object
*/
function activateGizmoManagerForMesh(selectedMesh, selectSingleMeshProperty) {
	if (gizmoManager == null) {
		gizmoManager = new BABYLON.GizmoManager(scene, 5);
		//clear gizmo when not selecting any mesh
		gizmoManager.clearGizmoOnEmptyPointerEvent = true;
	}

	//manually selecting a mesh for the gizmo vs automatic selection based on selected meshes in scene
	if (selectSingleMeshProperty == true) {
		gizmoManager.usePointerToAttachGizmos = false;
		gizmoManager.attachToMesh(getMeshGroupByMeshName(selectedMesh.name));
	} else {
		gizmoManager.usePointerToAttachGizmos = true;
		gizmoManager.attachableMeshes = getMeshArrayByPrefix(getIDFromMeshName(selectedMesh.name)); //[selectedMesh];
	}
	gizmoManager.positionGizmoEnabled = true;
	document.onkeydown = (e) => {
		if (e.key == 'w') {
			gizmoManager.positionGizmoEnabled = !gizmoManager.positionGizmoEnabled;
			gizmoManager.rotationGizmoEnabled = false;
			gizmoManager.scaleGizmoEnabled = false;
			gizmoManager.boundingBoxGizmoEnabled = false;
		}
		if (e.key == 'e') {
			gizmoManager.rotationGizmoEnabled = !gizmoManager.rotationGizmoEnabled;
			gizmoManager.positionGizmoEnabled = false;
			gizmoManager.scaleGizmoEnabled = false;
			gizmoManager.boundingBoxGizmoEnabled = false;
		}
		if (e.key == 'r') {
			gizmoManager.scaleGizmoEnabled = !gizmoManager.scaleGizmoEnabled;
			gizmoManager.positionGizmoEnabled = false;
			gizmoManager.rotationGizmoEnabled = false;
			gizmoManager.boundingBoxGizmoEnabled = false;
		}
		if (e.key == 'q') {
			gizmoManager.boundingBoxGizmoEnabled = !gizmoManager.boundingBoxGizmoEnabled;
			gizmoManager.positionGizmoEnabled = false;
			gizmoManager.rotationGizmoEnabled = false;
			gizmoManager.scaleGizmoEnabled = false;
		}
	}
}

/**
  * @desc Merge meshes by their prefix (e.g. INV_512..). Only tested w/PdG data at the moment. For performance reasons.
*/
function groupMeshesByPrefix(meshes) {
	//1. create array to store only unique meshnames
	var meshNameArr = [];
	for (var i = 0; i < meshes.length; i++) {
		var meshID = getIDFromMeshName(meshes[i].name);
		if (meshNameArr.indexOf(meshID) == -1) {
			meshNameArr.push(meshID);
		}
	}

	//2. merge meshes based on their prefix (e.g. INV_512..)
	//limitation: some meshes cannot be merged at the moment (properties issue)
	for (var i = 0; i < meshNameArr.length; i++) {
		var meshArrToMerge = getMeshArrayByPrefix(meshNameArr[i]);
		if (meshArrToMerge.length > 1) {
			//manual exclusion of meshes 
			//error: Uncaught (in promise) Error: Cannot merge vertex data that do not have the same set of attributes
			try {
				//var newMeshMerged = BABYLON.Mesh.MergeMeshes(meshArrToMerge, true, true, null, false, true);
				//newMeshMerged.isPickable = true;
				setMasterChildMeshes(meshNameArr[i], meshArrToMerge);
			} catch (error) {
				console.log(error);
				//console.log('[warning] Could not merge meshes for ' + meshNameArr[i] + ' due to different attributes. Instead set as Master/Child meshes.');
			}
		}
	}
}

/**
  * @desc Set the first mesh in the array as the master, the rest as child meshes. To allow for moving/resizing meshes as a group (when merging meshes is not possible)
*/
function setMasterChildMeshes(meshID, meshArr) {
	//create an empty container Mesh as a parent for a mesh group. One advantage is that the Gizmo arrows are pointing in a fixed direction.
	var groupNode = new BABYLON.Mesh('group_' + meshID, scene);
	//set position based on first Mesh of array (caveat: could lead to misplaced group, but is roughly OK)
	groupNode.setAbsolutePosition(meshArr[0].getAbsolutePosition());
	//add all meshes in the array as children to the groupNode
	for (var j = 0; j < meshArr.length; j++) {
		groupNode.addChild(meshArr[j]);
	}
}

/**
  * @desc Apply standard settings for transparency of a set of meshes
  * @param meshes {arr} meshes
  * @todo check which app is loaded before applying custom transparencies, move to settings.js?
*/
function setObjectTransparencyProperties(meshes) {
	for (var i = 0; i < meshes.length; i++) {
		//change max number of lights for objects (normally 4).
		if (meshes[i].material) {
			meshes[i].material.maxSimultaneousLights = 6;
		}

		var infObj = getMeshInformationMetadataObject(getIDFromMeshName(meshes[i].name));

		if (infObj) {
			//if hypothesis -> use custom function for displaying mesh
			if (infObj.id.value.includes('HYP')) {
				setHypothesisTransparency(getIDFromMeshName(meshes[i].name));
			} else {
				//if hypothesis #1 or other -> show mesh by default
				setMeshTransparency(meshes[i], 1);
			}
		} else {
			setMeshTransparency(meshes[i], 1);
		}

		//PdG: show or hide ceiling (dep. on view)
		if (meshes[i].name == settings.CEILING_MESH_NAME && camera.currentSetting == 'orbit') {
			camera.hideCeiling(scene);
		} else if (meshes[i].name == settings.CEILING_MESH_NAME && camera.currentSetting != 'orbit') {
			camera.showCeiling(scene);
		}
	}
	//[FIXME]
	//manually change var.condensator, since only the glass should be transparent
	setMeshTransparencyByName('variabele-condensator-2_primitive1', 0.6);
	setMeshTransparencyByName('variabele-condensator-1_primitive1', 0.6);
}

/**
  * @desc Set mesh transparency based on its "clickable" value (functionality for multiple hypotheses)
  * @param meshes {Str} featureID
*/
function setHypothesisTransparency(featureID) {
	if (meshIsClickable(featureID)) {
		setMeshTransparencyByName(featureID, 1);
	} else {
		setMeshTransparencyByName(featureID, 0);
	}
}

/**
  * @desc Standard settings for transparency of a set of meshes (objects in database get other transp. than objects outside database)
  * @param meshes {arr} meshes
  * @param transpValueInfObj {0..1} Transparency value for objects in DB
  * @param transpValue {0..1} Transparency value for objects outside DB
  * @todo check which app is loaded before applying custom transparencies, move to settings.js?
*/
function setObjectTransparencyPropertiesBasedOnValue(meshes, transpValueInfObj, transpValue) {
	for (var i = 0; i < meshes.length; i++) {
		var infObj = getMeshInformationMetadataObject(getIDFromMeshName(meshes[i].name));

		if (infObj) {
			setMeshTransparency(meshes[i], transpValueInfObj);
		} else {
			setMeshTransparency(meshes[i], transpValue);
		}
	}
}

/**
  * @desc Change the transparency of a single mesh using its name
  * @param meshName {string}
  * @param transparencyValue {double} (0-1)
*/
function setMeshTransparencyByName(meshName, transparencyValue) {
	//pdg: getting mesh by name not working since INV_xxx is only a prefix
	//bg: getIDFromMeshName prevents _primitiveXX to be retrieved using this function

	//fix
	if (settings.PROCESS_MESH_IDS) {
		var meshesToHighlightArr = getMeshArrayByPrefix(meshName);
		if (meshesToHighlightArr.length > 0) {
			for (var i = 0; i < meshesToHighlightArr.length; i++) {
				setMeshTransparency(meshesToHighlightArr[i], transparencyValue);
			}
		}
	} else {
		var selectedMesh = scene.getMeshByName(meshName);
		if (selectedMesh) {
			setMeshTransparency(selectedMesh, transparencyValue);
		}
	}
}

/**
  * @desc Change the transparency of a single mesh using the object
  * @param meshName {string}
  * @param transparencyValue {double} (0-1)
*/
function setMeshTransparency(mesh, alpha) {
	if (mesh == null) {
		console.log('[warning] Could not set mesh transparancy, mesh is null');
	} else {
		if (mesh.getClassName() == 'InstancedMesh') {
			//changing the visibility of an instanced mesh results in 'TypeError: setting getter-only property "visibility"' (e.g., in app #6)
			console.log('[warning] Visibility of InstancedMesh cannot be altered');
		} else if (mesh.name == settings.SCENE_GROUND_PLANE_NAME) {
			mesh.visibility = settings.GROUND_PLANE_ALPHA;
			//console.log('[warning] Transparency of ground plane cannot be altered');
		} else {
			mesh.visibility = alpha;
		}
	}
}

/**
  * @desc Get different meshes that start with the prefix of a certain meshname
  * @param meshName {string} --> mesh ID
  * @param returnMeshArr {Arr}
*/
function getMeshArray(meshName) {
	var returnMeshArr = [];
	for (var i=0; i< scene.meshes.length; i++) {
		var currMeshArrItemNamePrefix = getIDFromMeshName(scene.meshes[i].name);
		var currMeshNamePrefix = getIDFromMeshName(meshName);
		if(currMeshArrItemNamePrefix == currMeshNamePrefix) {
			returnMeshArr.push(scene.meshes[i]);
		}
	}
	return returnMeshArr;
}

/**
  * @desc Get different meshes that start with the prefix of a certain meshname. Bugfix: use separation char to prevent matching OBJ-Voorhuys-1 <> OBJ-Voorhuys-13.
  * @param meshName {string} --> mesh ID
  * @param returnMeshArr {Arr}
*/
function getMeshArrayByPrefix(meshName) {
	var meshes = scene.meshes;
	var returnMeshArr = [];
	for (var i = 0; i < meshes.length; i++) {
		if (meshes[i].name.startsWith(meshName + '_')) {
			returnMeshArr.push(meshes[i]);
		}
	}
	return returnMeshArr;
}

/**
  * @desc Set clickable (pickable) property for a set of meshes
  * @param meshes {arr}
*/
function setDefaultObjectClickableProperties(meshes) {
	for (var i = 0; i < meshes.length; i++) {
		var infObj = getMeshInformationMetadataObject(getIDFromMeshName(meshes[i].name));
		if (infObj && infObj.clickable.value == true) {
			meshes[i].isPickable = true;
		} else if (meshes[i].name.startsWith('button3D') || meshes[i].name.startsWith('imageViewer3D')) {
			meshes[i].isPickable = true;
		} else {
			if (allowSelectingAllMeshesProperty) {
				meshes[i].isPickable = true;
			} else {
				meshes[i].isPickable = false;
			}
		}
	}
}

/**
  * @desc Get property if object should be clickable (from metadata)
  * @param selectedMeshName {str}
  * @return objectIsClickableProperty {bool}
  * @todo Improve exception system
*/
function meshIsClickable(selectedMeshName) {
	var infObj = getMeshInformationMetadataObject(getIDFromMeshName(selectedMeshName));
	if (infObj) {
		if (infObj.clickable.value == true) {
			return true;
		}
	}
	return false;
}

/**
  * @desc Highlight a set of objects (based on their clickable value)
  * @param meshes {arr}
  * @param highlightObjects {bool}
*/
function setObjectHighlighting(meshes, dynamicHighlightsProperty) {
	for (var i = 0; i < meshes.length; i++) {
		var infObj = getMeshInformationMetadataObject(getIDFromMeshName(meshes[i].name));

		//to display highlighting, the addHighlightToMesh dynamic highlighting should be disabled..
		meshes[i].actionManager = new BABYLON.ActionManager(scene);

		if (infObj) {
			if (dynamicHighlightsProperty == true) {
				addHoverHighlightToMesh(meshes[i]);
			} else {
				if (meshes[i].actionManager) {
					meshes[i].actionManager.dispose();
				}
			}
		}
	}
}

/**
  * @desc Check if an alternate reconstruction hypothesis based on meshName.
  * @param meshName {str}
*/
function alternativeMeshHypothesisExists(meshName) {
	//Current implementation: meshnames including HYP[n] have an alternative reconstruction hypothesis, e.g. HYP1, HYP2
	if (meshName.includes('HYP')) {
		return true;
	} else {
		return false;
	}
}

/**
  * @desc Check if meshName is an alternative hypothesis. Limitation: up to 2 hypotheses supported.
  * @param meshName {str}
*/
function isAlternateMeshHypothesis(meshName) {
	//meshName including "HYP2" is the alternate hypothesis (right now, support for up to 2 hypotheses)
	if (meshName.includes('HYP2')) {
		return true;
	} else {
		return false;
	}
}

/**
  * @desc Check if meshName is the main hypothesis for an object. Limitation: up to 2 hypotheses supported.
  * @param meshName {str}
*/
function isMainMeshHypothesis(meshName) {
	//meshName including "HYP1" is the main hypothesis (right now, support for up to 2 hypotheses)
	if (meshName.includes('HYP1')) {
		return true;
	} else {
		return false;
	}
}

/**
  * @desc Transform meshName to alternate hypothesis meshName
  * @param meshName {str}
  * @return meshName {str} Alternate hypothesis
*/
function getAlternateMeshHypothesis(meshName) {
	if (isMainMeshHypothesis(meshName)) {
		return meshName.replace('HYP1', 'HYP2');
	} else if (isAlternateMeshHypothesis(meshName)) {
		return meshName.replace('HYP2', 'HYP1');
	}
	return null;
}

/**
  * @desc Functionality to select a reconstruction hypothesis (max. 2 hypotheses possible)
  * @param meshIDToShow {str}
*/
function selectReconstructionHypothesis(meshIDToShow) {
	var meshIDToHide = getAlternateMeshHypothesis(meshIDToShow);
	//1. hide currently visible hypothesis
	//set metadata for visibility of mesh
	metadataObjectStorage.setMeshHypothesisVisibilityMetadata(meshIDToHide, false);
	//hide all meshes of hypothesis
	var meshArr1 = getMeshArrayByPrefix(meshIDToHide);
	for (var i = 0; i < meshArr1.length; i++) {
		setMeshTransparency(meshArr1[i], 0);
	}
	//2. show currently hidden alternate hypothesis
	//set metadata for visibility of mesh
	metadataObjectStorage.setMeshHypothesisVisibilityMetadata(meshIDToShow, true);
	//set default clickable properties for all meshes in the scene (updates which hypothesis is clickable)
	setDefaultObjectClickableProperties(scene.meshes);
	//show all meshes of alternate hypothesis
	var meshArr2 = getMeshArrayByPrefix(meshIDToShow);
	for (var i = 0; i < meshArr2.length; i++) {
		setMeshTransparency(meshArr2[i], 1);
	}
}

/**
  * @desc Over/Out highlighting functionality
  * @todo Possibility to improve efficiency.
  * @param currMesh {str}
*/
function addHoverHighlightToMesh(currMesh) {
	//convoluted way to add all meshes to highlight..
	var meshName = currMesh.name;

	//no lighting can directly be applied to instanced meshes
	//in the case it is an instanced mesh, get the name from the source (i.e. "real" mesh, so the highlighting is called upon the source)
	//thus highlighting the source and all instanced meshes.
	if (currMesh.getClassName() == 'InstancedMesh') {
		meshName = currMesh.sourceMesh.name;
	}

	//var meshesToAddArr = getIDFromMeshName(meshName);
	var meshesToAddArr = [];

	//first check all meshes that belong to the same object of a selected mesh
	if(settings.PROCESS_MESH_IDS) { //case of VI reconstructions / PdG
		meshesToAddArr = getMeshArrayByPrefix(getIDFromMeshName(meshName));
	} else {
		meshesToAddArr = getMeshArray(meshName);
	}

	//create an action manager for the current scene
	currMesh.actionManager = new BABYLON.ActionManager(scene);

	//register an action to highlight and de-highlight all meshes belonging to a selected object
	currMesh.actionManager.registerAction(new BABYLON.ExecuteCodeAction(
		{ trigger: BABYLON.ActionManager.OnPointerOverTrigger },
		function () {
			highlightLayer.isEnabled = true;
			for (var i = 0; i < meshesToAddArr.length; i++) {
				try {
					highlightLayer.addMesh(meshesToAddArr[i], settings.DEFAULT_HIGHLIGHT_COLOR);
				} catch(err) {
					console.log(err);
				}
			}
		}));

	currMesh.actionManager.registerAction(new BABYLON.ExecuteCodeAction(
		{ trigger: BABYLON.ActionManager.OnPointerOutTrigger },
		function () {
			for (var i = 0; i < meshesToAddArr.length; i++) {
				highlightLayer.removeMesh(meshesToAddArr[i]);
			}
			highlightLayer.isEnabled = false;
		}));
}

/**
  * @desc Remove all 2D labels from the scene [3d functionality: moved]
  * @param advancedTexture {BABYLON_GUI.AdvancedDynamicTexture}
  * @todo move 3D functionality to GUI3D class
*/
function removeLabelsFromMeshes(advancedTexture) {
	//remove 2D UI labels, linked to meshes, b/o global array
	while (twoDimensionalInterfaceElementsArr.length > 0) {
		advancedTexture.removeControl(twoDimensionalInterfaceElementsArr.pop());
	}
}

/**
  * @desc Remove '_primitiveX' from an object ID - this means that a mesh has multiple primitives (i.e. multiple objects under same parent in Blender, in order to match the name with the equivalent names in the contextual database
  * @param meshName {str}
*/
function getMeshNameWithoutPrimitives(meshName) {
	if (meshName.includes('primitive') && meshName.split('_').length > 0) {
		var meshNameWithoutPrimitives = meshName.split('_')[0];
		return meshNameWithoutPrimitives;
	}
	return meshName;
}

/**
  * @desc Get the color for a confidence index value
  * @param confIndex {int} 1-4
  * @return confIndexColor {arr} [r,g,b]
*/
function getConfidenceColor(confIndex) {
	//1. generic colors for confidence values (red-orange-yellow-green)
	//2. https://www.visualisingdata.com/2019/08/five-ways-to-design-for-red-green-colour-blindness/
	//see: http://google.github.io/palette.js/
	//see: https://davidmathlogic.com/colorblind/
	//https://gka.github.io/palettes/#/5|d|00ff00,96ffea,fcff6e|ffffe0,ff0000|1|1
	var confIndexCol;
	if (confIndex == 1) {
		confIndexCol = [0 / 255, 150 / 255, 0 / 255]; //#009E73
	} else if (confIndex == 2) {
		confIndexCol = [252 / 255, 255 / 255, 0 / 255]; //#FFFF92
	} else if (confIndex == 3) {
		confIndexCol = [255 / 255, 167 / 255, 123 / 255]; //#FFBCAF
	} else if (confIndex == 4) {
		confIndexCol = [255 / 255, 0 / 255, 0 / 255]; //#93003A
	} else {
		confIndexCol = [0.8, 0.8, 0.8];
	}
	return confIndexCol;
}

/**
  * @desc Clear the various selection filters for a set of meshes
  * @param meshes {arr}
*/
function clearSelectionFilter(meshes) {
	glowLayer.isEnabled = false;

	//turn off emissive color for objects without certainty values
	setEmissiveColorToAllMeshes(meshes, [0, 0, 0]);

	//reset transparency values
	setObjectTransparencyProperties(meshes);

	//reset to standard selection option
	setSelectionMode('highlight-mesh');
}

/**
  * @desc Delete a mesh from the scene.
 */
function deleteMesh(currentMesh) {
	if (currentMesh != null) {
		var meshGroup = getMeshGroupByMeshName(currentMesh.name);
		if (meshGroup != null) {
			//delete a group of meshes
			meshGroup.dispose();
		} else {
			//delete a single mesh
			currentMesh.dispose();
		}
		//deselect any currently selected meshes
		clearMeshSelections();
		//hide infobox alert if visible
		gui2D.hideInfoBoxAlert();
	}
}

/**
  * @desc Set a selection filter for a (set of) meshes
  * @param meshes {arr} 
  * @param meshName {str}
  * @param type {str} ('delete-mesh','hide-mesh','transparent-mesh-1', 'transparent-mesh-2', 'transparent-other-meshes', 'hide-other-meshes', 'wireframes', 'highlight-mesh', 'highlight-mesh-all','recolor-all-meshes')
  * @todo Fix scene parameter, more elegant way for selectionMode, create array with excluded items
*/
function setSelectionFilter(meshes, meshName, type) {
	//experimental function to set selection filters
	var currentMesh = scene.getMeshByName(meshName);

	//set global var with selection mode
	if (type != '') {
		setSelectionMode(type);
	}

	if (type == "delete-mesh") {
		//delete a certain mesh
		deleteMesh(currentMesh);

	} else if (type == "show-mesh") {
		//show a single mesh
		setMeshTransparency(currentMesh, 1);
	} else if (type == "hide-mesh") {
		//hide a single mesh
		setMeshTransparency(currentMesh, 0);
	} else if (type == "transparent-mesh-1") {
		//single mesh transparency, level one
		setMeshTransparency(currentMesh, 0.2);
	} else if (type == "transparent-mesh-2") {
		//single mesh transparency, level two
		setMeshTransparency(currentMesh, 0.5);
	} else if (type == "transparent-other-meshes") {
		//make other meshes semi transparent
		hideOtherMeshes(meshes, meshName, 0.2);
	} else if (type == "hide-other-meshes") {
		//hide other meshes
		hideOtherMeshes(meshes, meshName, 0);
	} else if (type == "wireframes") {
		//change all meshes to wireframes (slow)
		changeMeshesToWireframes(meshes);
	} else if (type == "activate-gizmo") {
		//show resize/move buttons (gizmos)
		activateGizmoManagerForMesh(currentMesh, false);
	} else if (type == "highlight-mesh") {
		//default highlighting
		highlightMesh(meshes, meshName, 0.45, true);
	} else if (type == "highlight-mesh-light") {
		//more transparent highlight
		highlightMesh(meshes, meshName, 0.2, true);
	} else if (type == "highlight-mesh-all") {
		//highlight all meshes which are selected by the user (debug)
		highlightMesh(meshes, meshName, 0.3, false);
	} else if (type == "recolor-all-meshes") {
		//random recolor of all meshes which use the same material
		recolorMesh(currentMesh);
	}
}

/**
  * @desc Make other meshes than the currently selected one transparent or invisble
  * @returns myMaterial {BABYLON.StandardMaterial}
  * @todo check if .setEnabled = false would improve performance
*/
function hideOtherMeshes(meshes, meshName, transparencyValue) {
	//make other meshes invisible
	for (var i = 0; i < meshes.length; i++) {
		//look for all other meshes with the same string prefix number (e.g. INV_517)
		var currMeshArrItemNamePrefix = meshes[i].name;
		var currMeshNamePrefix = meshName;
		if (currMeshArrItemNamePrefix != currMeshNamePrefix && meshes[i].name != 'billboard' && !meshes[i].name.startsWith('imageViewer3D')) {
			setMeshTransparency(meshes[i], transparencyValue);
		} else {
			setMeshTransparency(meshes[i], 1);
		}
	}
}

/**
  * @desc change all meshes to wireframes (slow, cannot be reversed)
  * @param meshes to make transparent
*/
function changeMeshesToWireframes(meshes) {
	for (var i = 0; i < meshes.length; i++) {
		if (meshes[i].material) {
			meshes[i].material = new BABYLON.StandardMaterial('wireframeMat', scene);
			meshes[i].material.wireframe = true;
		}
	}
	//change light settings to standard "white" setting, otherwise not visible
	//activateLightsSetting(0);
}

/**
  * @desc Recolor a single mesh with a random color
  * @param currentMesh {BABYLON.Mesh}
*/
function recolorMesh(currentMesh) {
	if (currentMesh != null) {
		setEmissiveColorToMesh(currentMesh, [Math.random(), Math.random(), Math.random()], true);
	}
}

function highlightMesh(meshes, meshName, intensity, highlightSingleMeshProperty) {
	//highlight a single mesh

	//do not highlight meshes in VR due to performance issues
	if (ENABLE_LOW_QUALITY_MODE) { return; }

	glowLayer.isEnabled = true;
	glowLayer.intensity = intensity;

	for (var i = 0; i < meshes.length; i++) {
		//remove existing selected meshes if single selection mode is active, or if the scene was selected before
		if (highlightSingleMeshProperty) {
			glowLayer.removeIncludedOnlyMesh(meshes[i]);
		}
		//check if other elements with the same id exist, add them if so
		var currMeshArrItemNamePrefix = getIDFromMeshName(meshes[i].name);
		var currMeshNamePrefix = getIDFromMeshName(meshName);

		if (currMeshArrItemNamePrefix == currMeshNamePrefix) {
			if (meshes[i].getClassName() == 'InstancedMesh') {
				//add src mesh (+src mesh instances) of selected mesh in case it is an instance
				glowLayer.addIncludedOnlyMesh(meshes[i].sourceMesh);
			} else {
				glowLayer.addIncludedOnlyMesh(meshes[i]);
			}
		}
	}
	glowLayer.customEmissiveColorSelector = function (mesh, subMesh, material, result) {
		result.set(settings.DEFAULT_SELECTION_COLOR_R, settings.DEFAULT_SELECTION_COLOR_G, settings.DEFAULT_SELECTION_COLOR_B, 0.4);
	}
}

/**
  * @desc Getter for active uncertainty filter
*/
function getActiveUncertaintyFilter() {
	return activeUncertaintyFilter;
}

/**
  * @desc Setter for active uncertainty filter
*/
function setActiveUncertaintyFilter(filter) {
	activeUncertaintyFilter = filter;
}

/**
  * @desc Toggler for uncertainty filter (used in VR)
*/
function toggleUncertaintyFilter() {
	if (getActiveUncertaintyFilter() == null) {
		setUncertaintyFilter('object');
	} else if (getActiveUncertaintyFilter() == 'object') {
		setUncertaintyFilter('location');
	} else if (getActiveUncertaintyFilter() == 'location') {
		setUncertaintyFilter(null);
	}
}

/**
  * @desc Toggler for uncertainty filter
*/
function toggleUncertaintyFilterByType(type) {
	var currUncFilter = getActiveUncertaintyFilter();
	//alternate type requested --> turn on alternate type
	if (currUncFilter != type) {
		setUncertaintyFilter(type);
		gui2D.showInfoBoxAlert(type, true);
		//same type requested --> turn off
	} else {
		setUncertaintyFilter(null);
		//resetActiveVisualFilters();
		gui2D.hideInfoBoxAlert();
	}
}

/**
  * @desc Show color coding based on object categories. Could be improved.
*/
function setObjectCategoryFilter() {
	var meshes = scene.meshes;
	var categoryArr = [];
	var objCategoryArr = [];

	//first, loop through meshes in scene to get categories
	for (var i = 0; i < meshes.length; i++) {
		var infObj = getMeshInformationMetadataObject(getIDFromMeshName(meshes[i].name));
		if (infObj) {
			glowLayer.addIncludedOnlyMesh(meshes[i]);
			//create arr with object categories, only add each one once (to later attach custom color)
			if (categoryArr.indexOf(infObj.category.value) == -1) {
				categoryArr.push(infObj.category.value);
			}
			objCategoryArr.push([meshes[i].name, categoryArr.indexOf(infObj.category.value)]);
		} else {
			//setEmissiveColorToMesh(meshes[i],[0.4,0.4,0.4]);
		}
	}
	//turn on glowLayer
	glowLayer.isEnabled = true;
	glowLayer.intensity = 0.2;
	//loop through meshes included in glowlayer - this happens continuously until filter deactivated!
	glowLayer.customEmissiveColorSelector = function (mesh, subMesh, material, result) {
		for (var i = 0; i < objCategoryArr.length; i++) {
			var confIndexMeshName = objCategoryArr[i][0];
			//still experimental : do not show glowlayer when occluded
			//mesh.occlusionType = BABYLON.AbstractMesh.OCCLUSION_TYPE_OPTIMISTIC;

			if (mesh.name == confIndexMeshName) { //&& !mesh.isOccluded) {
				var rgbArr = getCategoryColor(objCategoryArr[i][1]);
				var r = rgbArr[0];
				var g = rgbArr[1];
				var b = rgbArr[2];
				result.set(r, g, b, 1);
				break;
			} else {
				result.set(0, 0, 0, 0);
			}
		}
	}
}

/**
  * @desc Draft function for category colors
*/
function getCategoryColor(index) {
	if (index == 0) {
		return [1, 0, 0];
	} else if (index == 1) {
		return [0, 1, 0];
	} else if (index == 2) {
		return [0, 0, 1];
	} else if (index == 3) {
		return [1, 1, 0];
	} else if (index == 4) {
		return [1, 0, 1];
	} else if (index == 5) {
		return [0, 1, 1];
	} else if (index == 6) {
		return [0.5, 1, 1];
	} else {
		return [0, 0.5, 0.5];
	}
}

/**
  * @desc hide color coding filters
  * @param meshes {obj}
  * @todo (todo: rename)
*/
// 
function hideUncertaintyFilter(meshes) {
	glowLayer.isEnabled = false;
	for (var i = 0; i < meshes.length; i++) {
		//remove all existing meshes from glowlayer
		glowLayer.removeIncludedOnlyMesh(meshes[i]);
		var infObj = getMeshInformationMetadataObject(getIDFromMeshName(meshes[i].name));
		//also turn off emissive color for objects without certainty values
		if (!infObj) {
			//setEmissiveColorToMesh(meshes[i],[0,0,0]);
		}
	}
}

/**
  * @desc Dynamically show the uncertainty values of a set of meshes
  * @param meshes {obj}
  * @param type {str} (null, 'object', 'location') [type of uncertainty display]
  * @todo to check: only set emissive color once; Optimize displayal
*/
function setUncertaintyFilter(type) {
	//objects with uncertainty value: via glow/emissive color selector https://doc.babylonjs.com/how_to/glow_layer
	//objects without uncertainty value: add filter via adjusting their material (emissivecolor)

	var meshes = scene.meshes;
	setActiveUncertaintyFilter(type);

	if (type == "object" || type == "location") {
		var uncertaintyArr = [];

		for (var i = 0; i < meshes.length; i++) {
			var infObj = getMeshInformationMetadataObject(getIDFromMeshName(meshes[i].name));
			if (infObj) {
				glowLayer.addIncludedOnlyMesh(meshes[i]);
				//caution: string type
				var uncertaintyObj = [];
				uncertaintyObj[0] = meshes[i].name;
				uncertaintyObj[1] = infObj.confidenceIndexObject.value;
				uncertaintyObj[2] = infObj.confidenceIndexLocation.value;
				uncertaintyArr.push(uncertaintyObj);
			} else {
				//setEmissiveColorToMesh(meshes[i],[0.4,0.4,0.4]);
			}
		}

		//turn on glowLayer
		glowLayer.isEnabled = true;
		glowLayer.intensity = 0.3;

		//loop through meshes included in glowlayer
		glowLayer.customEmissiveColorSelector = function (mesh, subMesh, material, result) {
			for (var i = 0; i < uncertaintyArr.length; i++) {
				var confIndexMeshName = uncertaintyArr[i][0];
				var confIndexObj = uncertaintyArr[i][1];
				var confIndexLoc = uncertaintyArr[i][2];

				//still experimental : do not show glowlayer when occluded
				//mesh.occlusionType = BABYLON.AbstractMesh.OCCLUSION_TYPE_OPTIMISTIC;

				if (mesh.name == confIndexMeshName) { //&& !mesh.isOccluded) {
					var confIndexColorArr;
					if (type == 'object') {
						confIndexColorArr = getConfidenceColor(confIndexObj);
					} else if (type == 'location') {
						confIndexColorArr = getConfidenceColor(confIndexLoc);
					}
					var r = confIndexColorArr[0];
					var g = confIndexColorArr[1];
					var b = confIndexColorArr[2];

					result.set(r, g, b, 1);

					break;
				} else {
					result.set(0, 0, 0, 0);
				}
			}
		}
	} else {
		resetActiveVisualFilters();
	}
}

/**
  * @desc Change the emissive color for set of meshes
  * @param meshes {obj}
  * @param color {arr} [r,g,b]
*/
function setEmissiveColorToAllMeshes(meshes, color) {
	for (var i = 0; i < meshes.length; i++) {
		setEmissiveColorToMesh(meshes[i], color);
	}
}

/**
  * @desc Change the emissive color for a single mesh
  * @param mesh {BABYLON.Mesh}
  * @param color {arr} [r,g,b]
*/
function setEmissiveColorToMesh(mesh, color, replaceMaterialProperty) {
	var r = color[0];
	var g = color[1];
	var b = color[2];

	if (mesh.material == null) {
		//if necessary: assign material to every mesh
		//mesh.material = new BABYLON.StandardMaterial("customMaterial", scene);
	} else {
		//console.log('[log] trying to set emissive color');
		//create new material to make sure the material is created
		if (replaceMaterialProperty) {
			mesh.material = new BABYLON.StandardMaterial("customMaterial", scene);
		}
		mesh.material.emissiveColor = new BABYLON.Color3(r, g, b);
	}
}

/* *************** Interaction with Meshes ******************** */

/**
  * @desc Add interactivity to a mesh. Right now shows all possible actions in BJS.
  * @param mesh {BABYLON.Mesh}
*/
function addInteraction(mesh) {
	//interaction
	scene.onPrePointerObservable.add(function (pointerInfo, eventState) {
		console.log('%c PrePointerObservable: pointer pick: ' + pointerInfo.pickInfo.pickedMesh.name, 'background: red; color: white');
	}, BABYLON.PointerEventTypes.POINTERPICK, false);

	//click on an object
	scene.onPointerObservable.add(function (pointerInfo, eventState) {
		var selectedMeshName = pointerInfo.pickInfo.pickedMesh.name;
		selectMeshInScene(selectedMeshName);
		//debug msg
		console.log('%c PointerObservable: pointer pick: ' + selectedMeshName, 'background: blue; color: white');
	}, BABYLON.PointerEventTypes.POINTERPICK, false);

	var meshes = [mesh];
	for (var i = 0; i < meshes.length; i++) {
		let mesh = meshes[i];
		mesh.actionManager = new BABYLON.ActionManager(scene);
		mesh.actionManager.registerAction(new BABYLON.ExecuteCodeAction(BABYLON.ActionManager.OnPickTrigger, (function (mesh) {
			console.log("%c ActionManager: pick : " + mesh.name, 'background: green; color: white');
		}).bind(this, mesh)));
	}
}

/**
  * @desc Select a mesh in the scene
  * @param selectedMeshName {str}
  * @param currSelectionMode {str} Current selection mode [optional]
*/
function selectMeshInScene(selectedMeshName, currSelectionMode) {
	var meshes = scene.meshes;

	//deactivate gizmo if active
	deactivateGizmoManager();
	//deactivate guided tour alert if tour not active
	if (!guidedTourIsActive()) {
		gui2D.hideGuidedTourInfoAlert();
	}
	gui2D.hideInfoBoxAlert();

	if (currSelectionMode == null) {
		var globalSelectionMode = getSelectionMode();
		if (globalSelectionMode != null) {
			currSelectionMode = globalSelectionMode;
		} else {
			currSelectionMode = 'highlight-mesh';
		}
	}

	//set global var of which mesh is selected ()
	setSelectedMesh(scene.getMeshByName(selectedMeshName), true);

	//if an alternative reconstruction hypothesis exists, make sure it is visible (and the other one hidden)
	if (alternativeMeshHypothesisExists(selectedMeshName)) {
		selectReconstructionHypothesis(getIDFromMeshName(selectedMeshName));
	}

	//highlight a selected mesh
	if (selectedMeshName != null) {
		setSelectionFilter(meshes, selectedMeshName, currSelectionMode);
		//do not select a mesh if in a specific selection mode
		if (currSelectionMode == 'highlight-mesh' || currSelectionMode == 'transparent-other-meshes') {
			//reset previous transparencies (e.g. when activating x-ray mode for single object)
			//if(currSelectionMode=='highlight-mesh') { setObjectTransparencyProperties(meshes);}

			if (!gui2D.sideBarCanBeShown()) {
				//show meshes in 3d environment if the sidebar is hidden and cannot be shown (i.e. narrow window)
				showObjectInformation3D(selectedMeshName);
			} else {
				//if sidebar is hidden, but can be shown --> show it	
				if (gui2D.sideBarCanBeShown()) {
					gui2D.showSideBar();
				}
				//remove all 3d labels before showing the 2d ones
				removeLabelsFromMeshes(advancedTexture);
				//default case: show in a 2D panel if the sidebar is visible
				showObjectInformation2D(selectedMeshName);
			}
		}
	}
}

/**
  * @desc Show information about a selected object in 3D (e.g. VR) UI
  * @param meshes {arr}
  * @param selectedMeshName {str}
  * @todo Put back confidence index display
*/
function showObjectInformation3D(selectedMeshName) {
	var currentlySelectedMeshStr = String(selectedMeshName);
	//get csv content for current id
	var infObj = getMeshInformationMetadataObject(getIDFromMeshName(currentlySelectedMeshStr));

	var descrToDisplay = '';
	var titleToDisplay = '';
	var imageToDisplay = '';

	if (infObj) {
		//if sidebar is not visible, show short (vr-oriented) description
		var vrDesc = getMetadataValue(infObj, 'vrDescription');
		if (metadataValueExists(vrDesc)) {
			descrToDisplay = vrDesc;
		} else {
			descrToDisplay = getMetadataValue(infObj, 'description');
			if (descrToDisplay.length > 200) {
				//basic replace of html tags
				descrToDisplay = descrToDisplay.replace(/<[^>]+>/g, '');
				descrToDisplay = descrToDisplay.substring(0, 197) + '...';
			}
		}
		titleToDisplay = getMetadataValue(infObj, 'title');

		//switch to title from descr (shorter)
		//show an image associated to the object (or a placeholder image)
		if (metadataValueExists(infObj.image.url)) {
			imageToDisplay = settings.MEDIA_DIRECTORY + infObj.image.url;
		} else {
			//placeholder image
			imageToDisplay = 'data/images/dummy-object.png';
		}

		var ecarticoID = getMetadataValue(infObj, 'ecarticoID');
		var adamnetCategories = getMetadataValue(infObj, 'adamnetCategories');
		var aatURI = getMetadataValue(infObj, 'aatURI');

		//create 3D GUI
		gui3D.create3DGUI(scene, selectedMeshName, imageToDisplay, titleToDisplay, descrToDisplay, ecarticoID, adamnetCategories, aatURI);

	} else {
		//if no object info: remove labels
		removeLabelsFromMeshes(advancedTexture);
	}
}

/**
  * @desc Show information about a selected object in 2D UI (e.g. desktop)
  * @param meshes {arr}
  * @param selectedMeshName {str}
  * @todo Put back confidence index display
  * @todo Simplify, split up in functions
*/
function showObjectInformation2D(selectedMeshName) {
	var currentlySelectedObjectStr = String(selectedMeshName);

	//for PdG: get sequence number
	var currentlySelectedObjectID = getIDFromMeshName(currentlySelectedObjectStr);

	//get csv content for current id
	var infObj = getMeshInformationMetadataObject(getIDFromMeshName(currentlySelectedObjectID));

	gui2D.resetDynamicUIElements();

	if (infObj) {
		//assign title
		var titleLong = infObj.title.value;
		var titleShort;
		if (titleLong.length > 20) {
			titleShort = titleLong.substring(0, 18) + '..';
		}
		//abbreviate long title where necessary
		if (titleLong.length > 30) {
			titleLong = titleLong.substring(0, 28) + '..';
		}
		//create two versions of the title: for a small viewport and for a large one
		var titleHTMLText = '<span class="d-md-block d-lg-none card-title-width">' + titleShort + '</span><span class="d-md-none d-lg-block card-title-width">' + titleLong + '</span>';
		textInfoCardTitle.innerHTML = titleHTMLText;// + ' [' + currentlySelectedObjectStr + ']'; //volgnummer (--> obj id)
		gui2D.showUIElement(elementInfoCard);
		textInfoCardDescription.innerHTML = createTextLinks(infObj.description.value);

		showAnnotationAlertForID(currentlySelectedObjectID);
		//set inputfield for annotation to current value
		var metadataObj = new AnnotationDataObject(dataStorage, currentlySelectedObjectID);
		inputFieldAnnotationText.value = metadataObj.userAnnotation_text;

		//set object category link
		if (metadataValueExists(infObj.category.value)) {
			var categoryInfoLink = '';
			if (metadataValueExists(infObj.category.url)) {
				categoryInfoLink = '<a href="' + infObj.category.url + '" target="_blank">' + infObj.category.value + '</a>';
			} else {
				categoryInfoLink = infObj.category.value;
			}
			textInfoCardCategory.innerHTML = '<b>' + infObj.category.label + '</b>: ' + categoryInfoLink + ' (<a href="#" id="searchCat-' + currentlySelectedObjectID + '">filter</a>)'; //kamer
			document.getElementById('searchCat-' + currentlySelectedObjectID).addEventListener('click', function (evt) { searchObjectCategory(infObj.category.value); }, false);
		}

		//set creator information
		if (metadataValueExists(infObj.modelCreatorName.value)) {
			text3DModelInformation.innerHTML = '<b>Creator</b><br>'
			text3DModelInformation.innerHTML += 'Name: ' + infObj.modelCreatorName.value + '<br>';
			text3DModelInformation.innerHTML += 'Organisation: ' + infObj.modelCreatorOrganisation.value + '<br>';
		} else {
			text3DModelInformation.innerHTML = 'No information available.';
		}

		//Display linked data if not disabled
		if (settings.ACTIVATE_LINKED_DATA == true) {
			showLinkedData(infObj.ecarticoID.value, infObj.category.url, infObj.adamnetCategories.value, infObj.rijksmuseumID.value);
		} else {
			//hide LD accordeon element
			gui2D.hideUIElement(inspectorDataExplorationTab);
		}

		//show similar objects
		if (metadataValueExists(infObj.similarObjects.value)) {
			//20200910: todo: check if there are issues here (getMeshID?)
			var simObjID = infObj.similarObjects.value;
			//get related object information from arr 
			var infObjRelObj = getMeshInformationMetadataObject(simObjID);
			//if available info, show hyperlink to related obj
			if (infObjRelObj) {
				textInfoCardSimilarObjects.innerHTML = '<b>' + infObj.similarObjects.label + '</b>: <a id="ro-' + simObjID + '" href="#">' + infObjRelObj.title.value + '</a>'; //reconstructed as
				//add event listener to make related object selectable
				document.getElementById('ro-' + simObjID).addEventListener('click', function (evt) { selectMeshInScene(simObjID); }, false);
				gui2D.showUIElement(textInfoCardSimilarObjects);
			}
		}

		//show link to alternative mesh
		if (alternativeMeshHypothesisExists(selectedMeshName)) {
			//show alternative hypothesis option
			gui2D.showUIElement(textInfoCardAlternateHypothesis);
			//get alternate hypothesis mesh name
			var altHypothesisMeshName = getAlternateMeshHypothesis(selectedMeshName);
			//get information about alternate hypothesis
			var altHypInfObj = getMeshInformationMetadataObject(getIDFromMeshName(altHypothesisMeshName));
			var altHypTitle = '';
			if (altHypInfObj.title.value != null) {
				altHypTitle = altHypInfObj.title.value;
			}
			//create link to alternate hypothesis
			textInfoCardAlternateHypothesis.innerHTML = '<b>Alternate hypothesis</b>: <a id="id-altHyp" href="#">' + altHypTitle + '</a>';
			//add event listener to make related object selectable
			document.getElementById('id-altHyp').addEventListener('click', function (evt) { selectMeshInScene(altHypothesisMeshName); }, false);
		}

		if (settings.SHOW_CONFIDENCE_VALUES) {
			var confIndexObj = (1.25 - (infObj.confidenceIndexObject.value / 4)) * 100;
			var confIndexLoc = (1.25 - (infObj.confidenceIndexLocation.value / 4)) * 100;
			//[fixme]
			var confIndexObjOrig = infObj.confidenceIndexObject.value;
			var confIndexLocOrig = infObj.confidenceIndexLocation.value;
			if (confIndexObj < 0 || confIndexObj > 100) {
				//invalid value
				scaleInfoConfidenceObject.className = 'progress-bar progress-bar-striped';
				scaleInfoConfidenceObject.style = 'width:0%';
			} else {
				var confIndexColorArr = getConfidenceColor(confIndexObjOrig);
				//conv to 0-255 for rgb in css
				var r = confIndexColorArr[0] * 255;
				var g = confIndexColorArr[1] * 255;
				var b = confIndexColorArr[2] * 255;
				scaleInfoConfidenceObject.className = 'progress-bar';
				//scaleInfoConfidenceObject.style = 'width: ' + confIndexObj + '%; background-color: red;';
				scaleInfoConfidenceObject.style = 'width: 100%; background-color: rgb(' + r + ',' + g + ',' + b + ')';
				spanInfoConfidenceObjectNumberDisplay.innerHTML = confIndexObjOrig;
			}

			if (confIndexLoc < 0 || confIndexLoc > 100) {
				//invalid value
				scaleInfoConfidenceLocation.className = 'progress-bar progress-bar-striped';
				scaleInfoConfidenceLocation.style = 'width:0%';
			} else {
				var confIndexColorArr = getConfidenceColor(confIndexLocOrig);
				//conv to 0-255 for rgb in css
				var r = confIndexColorArr[0] * 255;
				var g = confIndexColorArr[1] * 255;
				var b = confIndexColorArr[2] * 255;
				scaleInfoConfidenceLocation.className = 'progress-bar';
				//scaleInfoConfidenceLocation.style = 'width: ' + confIndexLoc + '%';
				scaleInfoConfidenceLocation.style = 'width: 100%; background-color: rgb(' + r + ',' + g + ',' + b + ')';
				spanInfoConfidenceLocationNumberDisplay.innerHTML = confIndexLocOrig;
			}
		} else {
			//hide confidence values in item display
			gui2D.hideUIElement(scaleInfoConfidence);
		}

		//show an image associated to the object (or a placeholder image)
		if (metadataValueExists(infObj.image.url)) {
			gui2D.showUIElement(imageInfoCardImage);
			imageInfoCardImage.src = settings.MEDIA_DIRECTORY + infObj.image.url;
			imageInfoCardImageCaption.innerHTML = infObj.image.label;
		} else {
			//placeholder image for image
			//imageInfoCardImage.src = 'data/images/dummy-object.png';
			gui2D.hideUIElement(imageInfoCardImage);
			imageInfoCardImageCaption.innerHTML = 'No image available';
		}

		//reinitialize bootstrap tooltips to add confidence index explanation in "title" tag
		gui2D.updateBootstrapTooltips();
		//open the tab itself
		gui2D.openObjectTab();
	} else {
		clearMeshSelections();
	}
}

/**
  * @desc Deselect all items in the scene (2D and 3D)
  * @param openHomeTab {bool} Home tab will be opened when the inspector is closed (default: true)
*/
function clearMeshSelections(openHomeTab) {
	if (openHomeTab == true || openHomeTab == null) {
		gui2D.selectTab(tabHome, tabHomeContent);
	}

	//reset the selected mesh
	setSelectedMesh(null, true);

	//hide 2D tabs
	gui2D.deselectTab(tabObject, tabObjectContent);
	gui2D.deselectTab(tabObjectList, tabObjectListContent);
	gui2D.hideTab(tabObject);

	//hide 3D ui elements
	removeLabelsFromMeshes(advancedTexture);

	//remove gizmos for resizing objects
	deactivateGizmoManager();

	//reset highlightd object (if not in debug viewing modes)
	if (getSelectionMode() == 'highlight-mesh') {
		clearSelectionFilter(scene.meshes);
	}
}

/* **************** LOD functionality *********************** */

async function showLinkedData(ecarticoID, aatURI, adamnetCategories, rijksmuseumURI) {
	if (metadataValueExists(ecarticoID)) {
		var sparqlDataEcartico = new SPARQLDataEcartico();
		var ecarticoResultObj = await sparqlDataEcartico.getEcarticoData(ecarticoID).catch(error => console.error(error));
		if (ecarticoResultObj != null) {
			//under details tab
			gui2D.showEcarticoCreatorInformation(ecarticoResultObj);
			//under linked data tab
			gui2D.showEcarticoExternalRelationsInformation(ecarticoResultObj);
		}
		await showWorksBySameCreator(ecarticoID);
	}
	if (metadataValueExists(rijksmuseumURI)) {
		var sparqlDataGoldenAgents = new SPARQLDataGoldenAgents();
		var rijksmuseumResultObj = await sparqlDataGoldenAgents.getRijksmuseumDataForURI(rijksmuseumURI).catch(error => console.error(error));
		if (rijksmuseumResultObj != null) {
			//under details tab
			gui2D.showRijksmuseumOriginalObjectInformation(rijksmuseumResultObj);
		}
	}

	//show similar works if (at least) one of the three variables is not null
	if (metadataValueExists(adamnetCategories) || metadataValueExists(aatURI) || metadataValueExists(ecarticoID)) {
		await showSimilarWorks(adamnetCategories, aatURI, ecarticoID);
	}

	//reinitialize bootstrap tooltips and jquery clickable images to add newly created ones
	gui2D.updateBootstrapTooltips();
	gui2D.updateJQueryClickableImages();
}


async function showSimilarWorks(adamnetCategories, aatURI, ecarticoID) {
	var sparqlDataWikidata = new SPARQLDataWikidata();
	//do sparql queries for similar works
	var adamnetResultObjRelWorks = await (getAdamnetRelatedWorks(adamnetCategories, aatURI, ecarticoID));
	var wikidataResultObjRelWorks = await sparqlDataWikidata.getWikidataDataRelatedWorksAAT(aatURI).catch(error => console.error(error));
	//show total # results in header badge
	if (metadataValueExists(adamnetResultObjRelWorks) && metadataValueExists(wikidataResultObjRelWorks)) {
		badgeHeadingLODThree.innerHTML = wikidataResultObjRelWorks.numberOfResults + adamnetResultObjRelWorks.numberOfResults;
		if (adamnetResultObjRelWorks.numberOfResults > 0) {
			gui2D.showLinkedDataWorks(textWorksOfSameType, adamnetResultObjRelWorks, false);
		}
		if (wikidataResultObjRelWorks.numberOfResults > 0) {
			gui2D.showLinkedDataWorks(textWorksOfSameType, wikidataResultObjRelWorks, true);
		}
	}
}

async function showWorksBySameCreator(ecarticoID) {
	var sparqlDataWikidata = new SPARQLDataWikidata();
	var sparqlDataAdamnet = new SPARQLDataAdamnet();
	//works by same creator, b/o Ecartico ID
	//wikidata (portrait of, works by creator)
	var wikidataArtworksResultObj = await sparqlDataWikidata.getWikidataData(ecarticoID).catch(error => console.error(error));
	var adamnetResultObj = await sparqlDataAdamnet.getAdamnetData(ecarticoID).catch(error => console.error(error));
	//show total # results in header badge
	badgeHeadingLODTwo.innerHTML = adamnetResultObj.numberOfResults + wikidataArtworksResultObj.numberOfResults;
	if (adamnetResultObj.numberOfResults > 0) {
		gui2D.showLinkedDataWorks(textWorksByCreator, adamnetResultObj, false);
	}
	if (wikidataArtworksResultObj.numberOfResults > 0) {
		gui2D.showLinkedDataWorks(textWorksByCreator, wikidataArtworksResultObj, true);
	}
}

async function getAdamnetRelatedWorks(adamnetCategories, aatURI, ecarticoID) {
	var sparqlDataAdamnet = new SPARQLDataAdamnet();
	var adamnetResultObjRelWorks;
	if (metadataValueExists(adamnetCategories)) {
		adamnetResultObjRelWorks = await sparqlDataAdamnet.getAdamnetDataRelatedWorksAdamnetCategories(adamnetCategories).catch(error => console.error(error));
	} else if (metadataValueExists(aatURI)) {
		adamnetResultObjRelWorks = await sparqlDataAdamnet.getAdamnetDataRelatedWorksAAT(aatURI).catch(error => console.error(error));
	} else if (metadataValueExists(ecarticoID)) {
		adamnetResultObjRelWorks = await sparqlDataAdamnet.getAdamnetDataRelatedWorks(ecarticoID).catch(error => console.error(error));
	}
	return adamnetResultObjRelWorks;
}

/* ************** Various helper functions ********************** */

function metadataValueExists(val) {
	if(val != null & val != '') {
		return true;
	} else {
		return false;
	}
}

/**
  * @desc Get the ID of certain mesh (PdG: OBJ-XXX_Descr.. --> OBJ-XXX, RadioXX_primitive1 --> RadioXX, RadioXX --> RadioXX)
  * @param meshName {Str} name of a mesh in the scene
  * @todo What if both pdg & primitive? Revise.
*/
function getIDFromMeshName(meshName) {
	if (meshName == null) {
		return null;
	}
	if (meshName.split('_')[1]) {
		//PdG inventory case
		if (settings.PROCESS_MESH_IDS) {
			//transform OBJ-XXX_Descr
			return meshName.split('_')[0];
		} else if (settings.PROCESS_PRIMITIVES) {
			return getMeshNameWithoutPrimitives(meshName);
		}
	}
	return meshName;
}

/**
 * @desc Helper function (get a metadata value)
*/
function getMetadataValue(obj, prop) {
	if (obj) {
		if (obj[prop]) {
			if (obj[prop].value) {
				return obj[prop].value;
			}
		}
	}
	return null;
}

/**
  * @desc (Helper function) Replace a texture in a GLTF/GLB model
  * @param materialToChange {Material}
  * @param textureURL {str}
*/
function replaceGLBTexture(materialToChange, textureURL) {
	//need to be loaded using inverted Y texture axis, https://www.html5gamedevs.com/topic/35613-live-pbr-texturing-on-gltf-model/
	materialToChange.albedoTexture = new BABYLON.Texture(textureURL, scene, true, false);
}

function setSelectedMesh(selMesh, selectSingleMeshBool) {
	//add only one mesh to the selection
	if (selectSingleMeshBool) {
		selectedMeshesInSceneArray = [];
		selectedMeshesInSceneArray.push(selMesh);
	} else {
		//add multiple meshes to selection
		selectedMeshesInSceneArray.push(selMesh);
	}
}

/**
  * @desc Return mesh ID of currently selected mesh in scene
*/
function getSelectedMeshID() {
	var selectedMeshesArr = getSelectedMeshesInScene();
	if (selectedMeshesArr.length != 0) {
		if (selectedMeshesArr[0] != null) {
			return getIDFromMeshName(selectedMeshesArr[0].name);
		}
	}
	return null;
}

/**
  * @desc Return Mesh Obj of currently selected mesh in scene
*/
function getSelectedMesh() {
	var selectedMeshesArr = getSelectedMeshesInScene();
	//only return single selected mesh if only 1 has been selected (i.e. included in the glowlayer)
	if (selectedMeshesArr && selectedMeshesArr.length == 1) {
		return selectedMeshesArr[0];
	} else if (selectedMeshesArr && selectedMeshesArr.length > 1) {
		console.log('[warning] selecting first mesh of longer array');
		return selectedMeshesArr[0];
	} else {
		return null;
	}
}

/**
  * @desc Get currently selected meshes in the scene, now based on which is included in the glowlayer
  * @todo Limitation: cannot work in "Analysis Layer" mode, since those layers also use glowLayer
  * @todo Rework?
*/
function getSelectedMeshesInScene() {
	return selectedMeshesInSceneArray;
}

/**
  * @desc (helper) Extract links from a text and convert them to a [link] href
  * @param text {Str}
*/
function createTextLinks(text) {
	text = text.replace(/\]/g, ' ');
	text = text.replace(/\[/g, ' ');

	return (text || "").replace(
		/([^\S]|^)(((https?\:\/\/)|(www\.))(\S+))/ig,
		function (match, space, url) {
			var hyperlink = url;
			return space + '[<a href="' + hyperlink + '" target="_blank">' + 'link' + '</a>]';
		})
}

/* ************** Search / Filtering functionality ***************** */

/**
  * @desc Very rudimentary search/filtering functionality, search in <li> list of subjects
*/
function filterObjects() {
	//when search starts, make sure that the reset view button is hidden
	gui2D.hideResetViewAlert();

	var meshes = scene.meshes;

	var ul, li, a, i, txtValue;
	var filter = gui2D.getCurrentSearchFilter().toUpperCase();

	//clear previous selections
	if (filter != '') {
		clearSelectionFilter(meshes);
		setObjectTransparencyPropertiesBasedOnValue(meshes, 0.1, 0.05);
	} else {
		setObjectTransparencyProperties(meshes);
	}

	var ul = document.getElementById('ulObjectList');
	var li = ul.getElementsByTagName('li');

	// Loop through all list items, and hide those who don't match the search query
	for (i = 0; i < li.length; i++) {
		a = li[i].getElementsByTagName("a")[0];
		var featureID = li[i].getElementsByTagName("a")[0].id;

		txtValue = a.textContent || a.innerText;
		if (txtValue.toUpperCase().indexOf(filter) > -1) {
			li[i].style.display = "";
			setMeshTransparencyByName(featureID, 1);
			//highlight features here ..//

			//quick hack//
			if (featureID.includes('variabele-condensator')) {
				var newFID = featureID + '_primitive0';
				setMeshTransparencyByName(newFID, 1);
				newFID = featureID + '_primitive1';
				setMeshTransparencyByName(newFID, 0.6);
			} else if (alternativeMeshHypothesisExists(featureID)) {
				setHypothesisTransparency(featureID);
			}
		} else {
			li[i].style.display = "none";
			//setMeshTransparencyByName(featureID,0.2);
			//hide features here ../
		}
	}

	//keep on hiding ceiling in orbit view
	if (camera.currentSetting == 'orbit') {
		setMeshTransparencyByName(settings.CEILING_MESH_NAME, 0);
	}

	//show (or hide) remove filters button
	if (gui2D.getCurrentSearchFilter() != '') {
		gui2D.showFilterAlert();
	} else {
		gui2D.hideFilterAlert();
	}
}

/**
  * @desc Filter objects for a certain category
  * @param filter {Str}
*/
function searchObjectCategory(filter) {
	if (filter != null) {
		gui2D.setCurrentSearchFilter(filter);
		filterObjects();
		//setObjectTransparencyProperties(scene.meshes);
	}
	gui2D.openObjectListTab();
}

/* ****** Environment helpers ****** */

/**
  * @desc Create a ground plane (for gravity and for VR teleportation). Position is set in settings.js
*/
function createGroundPlane() {
	//basic ground plane
	var ground = BABYLON.Mesh.CreatePlane(settings.SCENE_GROUND_PLANE_NAME, 100.0, scene);
	var myMaterial = new BABYLON.StandardMaterial("myMaterial", scene);
	myMaterial.diffuseColor = new BABYLON.Color3(0, 0, 0);
	myMaterial.specularColor = new BABYLON.Color3(0, 0, 0);
	myMaterial.emissiveColor = new BABYLON.Color3(0, 0, 0);
	myMaterial.ambientColor = new BABYLON.Color3(0, 0, 0);
	ground.material = myMaterial;
	ground.position = settings.GROUND_PLANE_DEFAULT_POSITION;
	ground.rotation = new BABYLON.Vector3(Math.PI / 2, 0, 0);
	ground.isPickable = false;
}

/* ****** Getters / setters ***** */

function getMeshInformationMetadataObject(meshID) {
	return metadataObjectStorage.getMeshDataObject(meshID);
}

/**
  * @desc Set selection mode global variable
  * @param str newVal (...)
*/
function setSelectionMode(newVal) {
	selectionMode = newVal;
}

/**
  * @desc Get selection mode global variable
  * @return selectionMode {str}
*/
function getSelectionMode() {
	return selectionMode;
}

/* ****** Rendering / Optimizations ***** */

/**
  * @desc Main 3D scene render loop of babylonJS
  * @param renderFunction {func}
  * @todo Put back confidence index display
*/
function runRenderLoop() {
	var interval = 1000 / desiredFps;
	var lastFrameTime = performance.now();
	var numFramesAvgFramerate = 0;
	var totalAvgFramerate = 0;
	engine.runRenderLoop(function () {
		if (scene) {
			//if in low quality mode: limit framerate to [desiredFPS] frames per second; do not activate low framerate setting in XR/VR mode (to e.g. prevent motion sickness)
			if (lowQualityMode && !camera.xrIsActive) {
				var currentFrameTime = performance.now();
				var deltaFrameTime = currentFrameTime - lastFrameTime;
				//render frame only if > interval
				if (deltaFrameTime > interval) {
					scene.render();
					lastFrameTime = currentFrameTime;
					//show fps statistics - average over [desiredFps] # of frames
					if (showFPSCounter) {
						gui2D.showFPSLabel();
						numFramesAvgFramerate++;
						var fpsCnt = Math.round(1000 / deltaFrameTime);
						totalAvgFramerate += fpsCnt;
						if (numFramesAvgFramerate == desiredFps) {
							var fpsAvg = Math.round(totalAvgFramerate / desiredFps);
							gui2D.setFPSLabel(fpsAvg + ' / ' + desiredFps + ' fps');
							numFramesAvgFramerate = 0;
							totalAvgFramerate = 0;
						}
					} else {
						gui2D.hideFPSLabel();
						gui2D.resetFPSLabel();
					}
				}
			} else {
				scene.render();
				//show fps statistics
				var fpsLabel = document.getElementById("spanFPSLabel");
				if (showFPSCounter) {
					gui2D.showFPSLabel();
					gui2D.setFPSLabel(Math.round(engine.getFps().toFixed()) + " / 60 fps");
				} else {
					gui2D.hideFPSLabel();
					gui2D.resetFPSLabel();
				}
			}
		}
	});
}

/**
  * @desc "Gets the current hardware scaling level. By default the hardware scaling level is computed from the window device ratio. if level = 1 then the engine will render at the exact resolution of the renderCanvas. If level = 0.5 then the engine will render at twice the size of the renderCanvas."
*/
function toggleRenderQuality() {
	var hardwareScalingLevel = engine.getHardwareScalingLevel();
	if (hardwareScalingLevel == HARDWARE_SCALING_LEVEL_HIGH) {
		setRenderQuality('low');
	} else if (hardwareScalingLevel == HARDWARE_SCALING_LEVEL_LOW) {
		setRenderQuality('high');
	}
}

/**
  * @desc "Gets the current hardware scaling level. By default the hardware scaling level is computed from the window device ratio. if level = 1 then the engine will render at the exact resolution of the renderCanvas. If level = 0.5 then the engine will render at twice the size of the canvas."
*/
function setRenderQuality(setting) {
	if (setting == 'high') {
		engine.setHardwareScalingLevel(HARDWARE_SCALING_LEVEL_HIGH);
	} else if (setting == 'low') {
		engine.setHardwareScalingLevel(HARDWARE_SCALING_LEVEL_LOW);
	}
}

/**
  * @desc Optimize the scene to improve speed (currently in "debug" menu)
*/
function startSceneOptimizer() {
	//optimize for 60fps (1st param)
	var result = new BABYLON.SceneOptimizerOptions(60, 500);

	var priority = 0;
	result.optimizations.push(new BABYLON.ShadowsOptimization(priority));
	result.optimizations.push(new BABYLON.LensFlaresOptimization(priority));

	// Next priority
	priority++;
	/* 
	//postprocess optimizations seems to crash VR mode, disable for now
	result.optimizations.push(new BABYLON.PostProcessesOptimization(priority)); 
	*/
	result.optimizations.push(new BABYLON.ParticlesOptimization(priority));

	// Next priority
	priority++;
	result.optimizations.push(new BABYLON.TextureOptimization(priority, 256));

	// Next priority
	priority++;
	result.optimizations.push(new BABYLON.RenderTargetsOptimization(priority));

	// Next priority
	priority++;
	result.optimizations.push(new BABYLON.MergeMeshesOptimization(priority));

	// Next priority
	priority++;
	result.optimizations.push(new BABYLON.HardwareScalingOptimization(priority, 4));

	// Optimizer
	var optimizer = new BABYLON.SceneOptimizer(scene, result);

	optimizer.start();
}

/**
  * @desc Resize the 3d application (eventlistener -> "resize")
*/
function resizeApp() {
	engine.resize();
}

//debug
console.log(engine.webGLVersion);

//event listener for resizing browser window -> adjust 3D scene dimensions
window.addEventListener("resize", resizeApp);

/* ** Main function ** */
startApplication();