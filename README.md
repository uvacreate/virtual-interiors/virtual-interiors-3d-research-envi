# Virtual Interiors 3D Research Environment Demonstrator

Virtual Interiors 3D research environment demonstrator. Needs `nodejs` (tested with v12.18 LTS), `npm` (tested with v6.14.4).

Creator of this demonstrator (2019-2021): Hugo Huurdeman (https://orcid.org/0000-0002-3027-9597).

The demonstrator will be refactored and modularized by Saan Rashid (2021-2022). Read more about the demonstrator and its creation process in this blogpost: https://www.virtualinteriorsproject.nl/2021/08/04/towards-a-3d-research-environment/

## 1. General information

### 1.1 Background

Within [Virtual Interiors](https://www.virtualinteriorsproject.nl), both 2D and 3D interfaces to big historical data are created and evaluated. 

Hugo explored the design and development of 3D research environment demonstrator in a "co-design" setting with Chiara Piccoli ([postdoc in Virtual Interiors](https://www.virtualinteriorsproject.nl/visualising-amsterdam-interiors/)), and in an embedded research setting at the Netherlands Institute for Sound & Vision as well as Brill Publishers.

### 1.2 Link to prototype

*Sample application*
- A application is loaded as the standard application (accessible via: https://3d-demo.virtualinteriorsproject.nl or https://3d-demo.virtualinteriorsproject.nl/index.html?app=sample), located in the `data/apps/sample` directory in this repository.

Within the Virtual Interiors project, applications have been created with the following data:

*Virtual Interiors 3D reconstructions:* 
- (1) Herengracht 573, Amsterdam (Pieter de Graeff & Jacoba Bicker): Voorhuis (accessible via: https://3d-demo.virtualinteriorsproject.nl/index.html?app=pdg-entrance-hall)
  - 3D models, historical research, data: Chiara Piccoli (https://orcid.org/0000-0001-9854-4273)
  - Screencast: https://dx.doi.org/10.21942/uva.14424218
- (2) Herengracht 573, Amsterdam (Pieter de Graeff & Jacoba Bicker): Boekenkamer (will be made accessible at a later moment)
  - 3D models, historical research, data: Chiara Piccoli (https://orcid.org/0000-0001-9854-4273)
- (3) Herengracht 573, Amsterdam (Pieter de Graeff & Jacoba Bicker): grayscale model of whole house (will be made accessible at a later moment)
  - 3D models, historical research, data: Chiara Piccoli (https://orcid.org/0000-0001-9854-4273)

*Embedded research Beeld en Geluid (https://www.beeldengeluid.nl/en/knowledge/blog/visualizing-radio-past-using-technology-future):*
- (4) Beeld en Geluid interactive radio demo (accessible via: https://3d-demo.virtualinteriorsproject.nl/index.html?app=bg-interactive-radio or version June, 2020 via https://proto.virtualinteriorsproject.nl/3d/experimental-radio-prototype/)
  - 3D model: radio: Netherlands Institute for Sound and Vision. License: CC-BY-SA. Media items (photos/videos): Netherlands Institute for Sound and Vision. License: CC-BY-SA. Application data by Hugo Huurdeman, Jesse de Vos in consultation with historical radio experts.
  - Screencasts: https://www.youtube.com/watch?v=IrTmf3Ld5Hc, https://www.youtube.com/watch?v=OCq1YwLoT50
- (5) Beeld en Geluid radio in a sample room (accessible via: virtualinteriorsproject.nl/3d-demo/index.html?app=bg-radio-in-room)
  - 3D models: radio : Netherlands Institute for Sound and Vision. License: CC-BY-SA; grayscale room adapted from https://sketchfab.com/3d-models/low-poly-house-9f32446369454122a35a04bdc0e359b4, Pablo Chaves Pedrero. License: CC-BY. Media items (photos/videos): Netherlands Institute for Sound and Vision. License: CC-BY-SA. Application data by Hugo Huurdeman, Jesse de Vos in consultation with historical radio experts.

*Embedded research Brill Publishers:*
- (6) Dynamic Drawings 'Ramelli Mill' experimental demo application (accessible via: https://3d-demo.virtualinteriorsproject.nl/index.html?app=dd-ramelli-mill)
  - 3D models: adapted from Dynamic Drawings project, https://doi.org/10.17026%2Fdans-zzq-ymge (CC-0). 

### 1.3 2D and 3D GUI

The app provides two types of GUIs: the standard 2D GUI (see `data/js/gui-2d.js`) and a 3D GUI (see `data/js/gui-3d.js`).

The 2D GUI is aimed to be used via larger screens (e.g. desktop, tablet) and automatically displays a sidebar on the right-hand side of the screen. It contains all reconstruction details and features. (Specifically, upon Bootstrap breakpoint `md` or larger, see: https://getbootstrap.com/docs/4.6/layout/).

The 3D GUI is aimed at smaller screens (e.g. mobile phones) as well as VR, and does not display the sidebar. It consists of a simplified UI, projected within the virtual space of the included 3D model, and is displayed upon selecting an object in the scene.

Both 2D and 3D GUI allow access to a 2D button panel displayed in the top-left corner with basic settings for the scene.

### 1.4 General camera options (selectable in application using "Camera" menu)

- *orbit:* external third-person view
- *first-person:* first-person perspective, ability to walk around with arrow keys
- *vr-first-person:* view for Virtual Reality headset (tested using Oculus Quest 1). Enter XR button appears on compatible devices (on MacOS: "Enter XR" button only appears in Firefox, not Chrome). Local testing can be done using "WebXR API Emulator" Firefox plugin (https://addons.mozilla.org/en-US/firefox/addon/webxr-api-emulator/).
- *phone-first-person:* view for mobile devices (phone, tablet), which uses the motion sensors to rotate the perspective. Might ask for permission to access motion sensor data.
- *cardboard-vr-first-person:* view for cardboard VR devices. Might ask for permission to access motion sensor data.

## 2. Customizing the application

### 2.1 URL parameters

Using URL parameters, i.e. parameters in the URL string, it is possible to adapt certain application settings

- e.g. `https://3d-demo.virtualinteriorsproject.nl/index.html ?` __`app=sample&fps=30`__ loads the application, sets the *application* name to `sample`, and the *desired framerate* to `30`.

URL parameters are separated with a `?` from the regular point of the URL; multiple parameters are separated using an `&`.

The following parameters are available in the Virtual Interiors 2D viewer:

- App selection: `https://3d-demo.virtualinteriorsproject.nl/index.html?app=sample` [Textual string]. Select an application, the text should match the name of an application in the folder `data/apps`. 
- Low quality mode: `https://3d-demo.virtualinteriorsproject.nl/index.html?lq=true` [true|false]. Low quality mode uses a low resolution and target framerate. This can be useful when opening the application on e.g. a mobile device.
- Framerate selection: `https://3d-demo.virtualinteriorsproject.nl/index.html?fps=30` add a number for specifying the framerate the 3D scene should render at, between 1 and 60 [1..60]
- Camera selection: `https://3d-demo.virtualinteriorsproject.nl/index.html?cam=vr-first-person`. Set a standard camera which is active upon loading the application. For instance useful for opening the application in a VR headset. [orbit, first-person, vr-first-person, phone-first-person, cardboard-vr-first-person], see also below.
- Display [BabylonJS inspector](https://doc.babylonjs.com/toolsAndResources/tools/inspector), via two sidebars: `https://3d-demo.virtualinteriorsproject.nl/index.html?debugmode=true` [true|false]

## 3. Local use and development

### 3.1 Installation

Needs `nodejs` (tested with v12.18 LTS), `npm` (tested with v6.14.4).

1. Clone the repository to a local folder
2. Using the command line, go to the directory of the cloned repository and run `npm install` to install the necessary modules (`npm update` to update to current version).
3. `npm start`: start the application (served at `localhost:3000`, which can be opened in a browser, preferably Chrome).

- Create a standalone build of the demonstrator (for deployment on a web server): `npm run build`

(shortcut shell scripts for building and running the application: via `build.sh`, `run.sh` in the `scripts` folder) 

- Switch between applications using `localhost:3000/index.html?app=[app-name]`, e.g. `localhost:3000/index.html?app=sample` for the sample application (see [URL parameters](#21-url-parameters)).

### 3.2 Creating new applications to be viewed via the demonstrator

Applications can be added to the 3D research environment demonstrator. First, the application has to be cloned to the local computer (see *Installation* above). 

An application should be placed in the `data/apps` directory of the demonstrator, and it should be able to load the same files as the sample application (i.e. an app settings file, a 3D model and a CSV data file). To load a custom application, use the URL parameters as described [above](#21-url-parameters).

More specifically, the sample application in this repository contains:
- a basic settings file in Javascript format (`app-settings.js`)
- a basic 3D model in [glTF](https://www.khronos.org/gltf/) format in the `glb` directory (`glb/sample.glb`)
- a comma-separated data file with information about the objects (meshes) in the 3D scene as a [CSV](https://en.wikipedia.org/wiki/Comma-separated_values) format in the `csv` directory (`csv/sample-data.csv`)
- some sample images in the `media` directory (`media/geometry_sample_1.png`, `media/geometry_sample_2.png`)

After providing these files, the application can be loaded locally by starting or building the application (see above). 

It is also possible to specify custom links (relative or absolute) to an application's assets in a diffent location, for instance another folder or another web URL. This can be done by adapting the `this.MEDIA_DIRECTORY`, `this.MESH_DIRECTORY`, and `this.CSV_DIRECTORY` in the `app-settings.js` file of an application.

### 3.3 Demonstrator structure

The demonstrator consists of the following structure. See https://3d-demo.virtualinteriorsproject.nl/jsdoc for details about each application file (Javascript) and the functions they contain.

#### Demonstrator sources
- `/` : main demonstrator files (`index.html`, `main.js`), scripts to update BabylonJS version (default, stable, preview releases), script to update JSDoc documentation.
  - `data` : all data files related to the demonstrator
    - `data/apps` : files related to the different applications which can be shown via the demonstrator. App name corresponds to name of folder the application is located in (e.g. `data/apps/sample`). Adapt standard settings via `data/apps/default-app-settings.js`.
      - `data/apps/#/app-settings.js` : settings file for each app (uses Javascript syntax). Provided settings overwrite settings in `default-app-settings.js`.
        - `data/apps/#/csv/` : CSV file with application content (the content of each column should be specified in `app-settings.js`). Using the provided IDs in the CSV, it is possible to display metadata about meshes/objects within the 3D scene. (For instance, in the sample, the name of the mesh `Cone` in the 3D model is specified, and other columns provide additional information about this mesh, for instance a title and description to display in the application). There are two options for specifying the mesh names in the CSV: specifying the exact mesh name (e.g. `Cone`) [default] or a mesh name prefix (e.g. `ID01` for a mesh named `ID01_Cone`). The latter setting automatically groups meshes (e.g. representing one object) in the user interface, without the need to specify each mesh separately. See documentation of variable `PROCESS_MESH_IDS` in `app-default-settings.js` for more details.
        - `data/apps/#/glb/` : GLTF or GLB 3D models used by app. As indicated above, the names of meshes in the 3D scene should correspond with the IDs specified in the CSV.
        - `data/apps/#/media/` : media files (e.g. images, audio) used by an app
    - `data/css` : CSS files (main styling for the demonstrator, Fontawesome files for icons)
    - `data/images` : general demonstrator icons
    - `data/js` : 16 Javascript classes containing various demonstrator functionality. See https://3d-demo.virtualinteriorsproject.nl/jsdoc for all details.
    - `data/lib` : draco decoder library (currently loaded from the web, setting has to be adapted for current BabylonJS version)
    - `data/sparql-queries` : as a reference, SPARQL queries used by the demonstrator with some inline documentation (currently queries are included as variables in `data/js/sparql-data-`[..]`.js` classes, e.g. `data/js/sparql-data-adamnet.js`)
    - `textures` : textures and HDR environment maps used by demonstrator
  - `jsdoc` : documentation generated by JSDoc

#### After running `npm install`
  - `node modules` : used modules by the demonstrator

#### After running `npm run build`
  - `build` : the built demonstrator

### 3.4 Main dependencies

- Bootstrap (v4.6), https://getbootstrap.com/docs/4.6/
- BabylonJS (v5.0.0-alpha.8), https://github.com/BabylonJS/Babylon.js/releases/tag/5.0.0-alpha.8

See `package.json` for all dependencies.

### 3.5 Setting a default dataset

To use a default application within the demonstrator, update the variable `DEFAULT_APP_NAME` in `main.js` to the name of your application, corresponding to the name of the folder within `data/apps/` it has been placed in. By default, the sample dataset is loaded within the application.

### 3.6 Adapting default settings

Adapt the default settings by editing the Javascript file `data/apps/default-app-settings.js`.

### 3.7 Source code documentation

Source code documentation is generated automatically via JSDoc, accessible via: https://2d-demo.virtualinteriorsproject.nl/jsdoc/.

### 3.8 Attributions

- This application uses Fontawesome icons (https://www.fontawesome.com)

## 4. Known issues:

- VR mode: In BabylonJS v5.0.0-alpha version >12, there is a bug (or breaking change) with data included in 3D meshes, which causes an error when selecting the image button in the VR button panel. In BabylonJS v5.0.0-alpha version >12 and <22, the enter XR button does not appear. Therefore v.5.0.0-alpha.8 is now used as the default version. 
- Normal mode: Ambient occlusion (an optional image enhancement feature) works correctly in Chrome, but not in Firefox (using BabylonJS v5.0.0-alpha.8).
- The position of 3D information panels and 3D button panels (for VR/mobile display) in the `app-settings.js` do not fully correspond due to issues/bugs in panel rotation in current BabylonJS version.
- The Linked Data functionality has only been adapted to the Virtual Interiors reconstructions (Herengracht 573).

### Further notes:

- When running the application online, an *https://* connection is necessary to use device sensors (e.g. motion sensor on an iPhone)

## 5. License

This project is licensed under the GPL-v3 license. The full license can be found in [LICENSE](LICENSE).

## 6. Further information and resources

### Demos: 

- Latest web application for this 3D viewer is generated accessible via: https://3d-demo.virtualinteriorsproject.nl
- The Beeld en Geluid historical radio app (older version, June, 2020) is available via this public link: https://proto.virtualinteriorsproject.nl/3d/experimental-radio-prototype/.

### Publications

[1] Huurdeman, H.C. & Piccoli, C. (2021). 3D Reconstructions as Research Hubs: Geospatial Interfaces for Real-Time Data Exploration of Seventeenth-Century Amsterdam Domestic Interiors. Open Archaeology journal. https://doi.org/10.1515/opar-2020-0142

[2] Piccoli, C. (2021). Home-making in 17th century Amsterdam: A 3D reconstruction to investigate visual cues in the entrance hall of Pieter de Graeff (1638-1707). In G. Landeschi & E. Betts (Eds.), Capturing the senses: digital methods for sensory archaeologies. New York: Springer.

[3] Huurdeman, H.C., Piccoli C., van Wissen, L. (2021). Linked Data in a 3D context: Experiential Interfaces for Exploring the Interconnections of 17th Century Historical Data. DH Benelux 2021. https://doi.org/10.5281/zenodo.4889856

[4] Huurdeman, H.C., & Piccoli, C. (2020). “More than just a Picture” -- The Importance of Context in Search User Interfaces for Three-Dimensional Content. Proceedings of the 2020 Conference on Human Information Interaction and Retrieval, 338–342. https://doi.org/10.1145/3343413.3377994

### Blogposts

- Huurdeman, H.C. (2021). Analyze & Experience: Towards a Research Environment for 3D Reconstructions, https://www.virtualinteriorsproject.nl/2021/08/04/towards-a-3d-research-environment/ 
- Huurdeman, H.C., De Vos, J. (2020). Visualizing a Radio of the Past using Technology of the Future, https://www.beeldengeluid.nl/en/knowledge/blog/visualizing-radio-past-using-technology-future
- Piccoli, C. (2020). Private libraries as a microcosmos of knowledge: Travelling through time and space with Pieter de Graeff’s book collection. https://www.virtualinteriorsproject.nl/2020/07/02/private-libraries-as-microcosmos-of-knowledge-travelling-through-time-and-space-with-pieter-de-graeffs-book-collection/
