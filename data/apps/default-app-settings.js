import GuidedTour from "../js/guided-tour";

class DefaultAppSettings {
    constructor() {
        //various internal variables
        this.hdrTexture;
        this.EnvironmentHelper;
        this.lighting;

        /* Default values for settings. All these values can be overridden in child classes */

        /* App location settings */
        //other directories derived from app-number
        this.APP_DIRECTORY = '';
        
        /* Camera settings */
        //PC/Mobile
        //First person perspective
        this.FIRST_PERSON_DEFAULT_POSITION = new BABYLON.Vector3(0, 0, 0);
        this.FIRST_PERSON_DEFAULT_TARGET = new BABYLON.Vector3(0, 0, 0);
        this.FIRST_PERSON_DEFAULT_ROTATION = new BABYLON.Vector3(0, 0, 0);
        //Orbit perspective
        this.ORBIT_DEFAULT_POSITION = new BABYLON.Vector3(3, 10, 10.);
        this.ORBIT_DEFAULT_TARGET = new BABYLON.Vector3(2, 6, 5);
        //Defines the camera rotation along the longitudinal axis 
        this.ORBIT_DEFAULT_ALPHA = 0;
        //Defines the camera rotation along the latitudinal axis 
        this.ORBIT_DEFAULT_BETA = 0;
        //Defines the camera distance from its target
        this.ORBIT_DEFAULT_RADIUS = 10;

        //Virtual reality perspective
        this.FIRST_PERSON_VR_DEFAULT_POSITION = new BABYLON.Vector3(0, 0, 0);
        this.FIRST_PERSON_VR_DEFAULT_TARGET = new BABYLON.Vector3(0, 0, 0);
        //ellipsoid around the camera, position when moving around
        this.CAMERA_DEFAULT_ELLIPSOID = new BABYLON.Vector3(0, 1, 0.1);
        this.DEFAULT_CAMERA = 'first-person';

        /* Basic file information */

        //3d scene to import
        this.MESH_DIRECTORY = this.getMeshDirectory();
        //GLTF/GLB filename
        this.MESH_NAME = '';
        //Contextual data CSV to import
        this.CONTEXT_DATA_URL = this.getCSVDirectory() + '',

        //removing "_primitiveX" from mesh names (to get their actual name)
        ////Set to true for BG radio demo, false for Virtual Interiors reconstructions
        this.PROCESS_PRIMITIVES = false;

        //Process mesh IDs (e.g. ID01_xxx). The part after (and including) the underscore is ignored, which means that all meshes
        //with the same prefix ID will be grouped (shown together) when selecting an object.
        //For instance, if a 3D model contains the meshes 'ID01_table-top', 'ID01_table-base', 'ID01_table-leg',
        //and the CSV uses 'ID01' in the ID column, all meshes of the table will be grouped upon selecting them in the UI.
        ////Set to true for Virtual Interiors reconstructions, false for the other demos so far
        this.PROCESS_MESH_IDS = false;

        //Activate the linked data functionality (currently only supported for Virtual Interiors 3D reconstructions)
        ////Set to true for Virtual Interiors reconstructions, false in all other cases
        this.ACTIVATE_LINKED_DATA = false;

        /** Basic mesh info **/

        //name, position and opacity of ground plane in scene, needed for gravity and collision detection
        //In first-person view, should be positioned -under- position of camera if gravity is activated (otherwise camera will "fall through")
        this.SCENE_GROUND_PLANE_NAME = 'ground';
        this.GROUND_PLANE_DEFAULT_POSITION = new BABYLON.Vector3(0, 0, 0);
        this.GROUND_PLANE_ALPHA = 0;
        //optional name (string) of ceiling mesh in scene, to hide it when showing orbit view
        this.CEILING_MESH_NAME = '';

        /** UI settings **/

        //show confidence values in item view (for pdg: true)
        this.SHOW_CONFIDENCE_VALUES = true;
        //highlighting colors
        this.DEFAULT_HIGHLIGHT_COLOR = new BABYLON.Color3(0.489, 0.740, 1); //blue
        this.DEFAULT_SELECTION_COLOR_R = 0.489;
        this.DEFAULT_SELECTION_COLOR_G = 0.740;
        this.DEFAULT_SELECTION_COLOR_B = 1;
        
        //button labels
        //main menu
        this.BUTTON_GENERAL = 'General';
        this.BUTTON_LIST = 'Object list';
        this.BUTTON_INSPECTOR = 'Inspector';
        //accordeon items (under details)
        this.BUTTON_RECONSTRUCTION = 'Details';
        this.BUTTON_COMPARISONS = 'Images';
        this.BUTTON_3D_MODEL = '3D model';

        //3D infopanel (only visible in VR, mobile, small viewport)
        //position the information based on predefined position/rotation
        this.INFOPANEL_3D_POSITION = new BABYLON.Vector3(1.39, 4.65, 4.41);
        this.INFOPANEL_3D_ROTATION = new BABYLON.Vector3(0, 3.12, 0);
        //margin between title and infopanel
        this.INFOPANEL_3D_MARGIN = 0.85;
        //width of infopanel (title and information)
        this.INFOPLANE_3D_WIDTH = 1;
        //font size infopanel
        this.INFOPLANE_3D_FONT_SIZE = 86; //pt
        //height of information plane
        this.INFOPLANE_3D_HEIGHT = 1.32;
        //height of title plane
        this.TITLEPLANE_3D_HEIGHT = 0.21;
        //3D button panel
        this.BUTTON_PANEL_3D_POSITION = new BABYLON.Vector3(1.39, 3.65, 4.41);
        this.BUTTON_PANEL_3D_ROTATION = new BABYLON.Vector3(0, 3.12, 0);

        /* default content settings */
        //Default information displayed in application
        this.DEFAULT_TAB_GENERAL_TITLE = 'Title of the application';
        this.DEFAULT_TAB_GENERAL_DESCRIPTION = 'General description can be placed here';
        this.DEFAULT_TAB_GENERAL_ABOUT = 'About information can be displayed here.';
        this.DEFAULT_TAB_FURTHER_INFORMATION = 'Further information can be displayed here';
        //add a maximum of 6 carousel items to the sidebar (leave at null to not display)
        this.DEFAULT_TAB_GENERAL_IMAGE = this.getMediaDirectory() + 'dummy-object.png';
        this.DEFAULT_TAB_GENERAL_IMAGE_CAPTION = 'Caption 1';
        this.DEFAULT_TAB_GENERAL_IMAGE_2;
        this.DEFAULT_TAB_GENERAL_IMAGE_2_CAPTION;
        this.DEFAULT_TAB_GENERAL_IMAGE_3;
        this.DEFAULT_TAB_GENERAL_IMAGE_3_CAPTION;
        this.DEFAULT_TAB_GENERAL_IMAGE_4;
        this.DEFAULT_TAB_GENERAL_IMAGE_4_CAPTION;
        this.DEFAULT_TAB_GENERAL_IMAGE_5;
        this.DEFAULT_TAB_GENERAL_IMAGE_5_CAPTION;
        this.DEFAULT_TAB_GENERAL_IMAGE_6;
        this.DEFAULT_TAB_GENERAL_IMAGE_6_CAPTION;
  
        /* default CSV data column settings, each number represents a column (starting from 0). -1 indicates the column is not in use. */
        this.COLUMN_ID_ID = -1;
        this.COLUMN_ID_TITLE = -1;
        this.COLUMN_ID_DESCRIPTION = -1;
        this.COLUMN_ID_IMAGE = -1;
        this.COLUMN_ID_IMAGE_CAPTION = -1;
        this.COLUMN_ID_CATEGORY = -1;
        this.COLUMN_ID_CATEGORY_URL = -1;
        this.COLUMN_ID_SIMILAR_OBJECTS = -1;
        this.COLUMN_ID_CONF_INDEX_OBJECT = -1;
        this.COLUMN_ID_CONF_INDEX_LOCATION = -1;
        this.COLUMN_ID_LOCATION = -1;
        this.COLUMN_ID_CLICKABLE = -1;
        this.COLUMN_ID_VR_DESCRIPTION = -1;
        this.COLUMN_ID_ECARTICO_ID = -1;
        this.COLUMN_ID_RIJKSMUSEUM_ID = -1;
        this.COLUMN_ID_ADAMNET_CATEGORIES = -1;
        this.COLUMN_ID_MODEL_CREATOR_NAME = -1;
        this.COLUMN_ID_MODEL_CREATOR_ORGANISATION = -1;
    }

    /**
      * @desc Getter for getting the CSV column index number for a certain property, called from data/js/metadata-object-storage.js
      * @param columnName {str} Column name
      * @returns CSV column index number {int}, or -1 if not available
    */
    getColumnIndexNumberContextualData(columnName) {
        //function returns column number (in the CSV with the data for the current app) for a certain property
        //if features are not included, return -1
        switch (columnName) {
            case 'id':
                //id of the object
                return this.COLUMN_ID_ID;
            case 'title':
                //title of the object (plain text)
                return this.COLUMN_ID_TITLE;
            case 'description':
                //description of the object
                return this.COLUMN_ID_DESCRIPTION;
            case 'image':
                //thumbnail image for the object
                return this.COLUMN_ID_IMAGE;
            case 'imageCaption':
                //caption for thumbnail
                return this.COLUMN_ID_IMAGE_CAPTION;
            case 'category':
                //category (textual) for the object
                return this.COLUMN_ID_CATEGORY;
            case 'categoryUrl':
                //category URL (e.g. AAT) for the object
                return this.COLUMN_ID_CATEGORY_URL;
            case 'similarObjects':
                //similar or connected objects
                return this.COLUMN_ID_SIMILAR_OBJECTS;
            case 'confIndexObject':
                //confidence value for the object itself
                return this.COLUMN_ID_CONF_INDEX_OBJECT;
            case 'confIndexLocation':
                //confidence value for the location of the object
                return this.COLUMN_ID_CONF_INDEX_LOCATION;
            case 'location':
                //where the object is located (if available)
                return this.COLUMN_ID_LOCATION; //?
            case 'clickable':
                //the clickable value is 'TRUE' if an object should be pickable
                //set to -1 if not in use (then clickable always defaults to true)
                return this.COLUMN_ID_CLICKABLE;
            case 'vrDescription':
                //The description of the object which is shown in VR
                return this.COLUMN_ID_VR_DESCRIPTION;
            case 'ecarticoID':
                //The ecarticoID for a certain object's creator
                return this.COLUMN_ID_ECARTICO_ID;
            case 'rijksmuseumID':
                //The ecarticoID for a certain object's creator
                return this.COLUMN_ID_RIJKSMUSEUM_ID;
            case 'adamnetCategories':
                //Specific categories for Adamnet
                return this.COLUMN_ID_ADAMNET_CATEGORIES;
            case 'modelCreatorName':
                //creator of the 3D model
                return this.COLUMN_ID_MODEL_CREATOR_NAME;
            case 'modelCreatorOrganisation':
                //organisation of the model creator
                return this.COLUMN_ID_MODEL_CREATOR_ORGANISATION;
            default: -1
        }
    }

   /**
    * @desc Getter for app directory
    * @returns APP_DIRECTORY {str}
    */
    getAppDirectory() {
        return this.APP_DIRECTORY;
    }

    /**
      * @desc Getter for media directory (images, videos, etc.)
      * @returns MEDIA_DIRECTORY {str}
    */
    getMediaDirectory() {
        return this.APP_DIRECTORY + 'media/';
    }

    /**
      * @desc Getter for mesh directory (e.g. containing .GLB models)
      * @returns MESH_DIRECTORY {str}
    */
    getMeshDirectory() {
        return this.APP_DIRECTORY + 'glb/';
    }

    /**
    * @desc Getter for mesh directory (e.g. containing .GLB models)
    * @returns MESH_DIRECTORY {str}
    */
    getCSVDirectory() {
        return this.APP_DIRECTORY + 'csv/';
    }

    /**
    * @desc Getter which returns array of GuidedTourDataObjects (array of positions/rotations and display settings). Override and add camera positions to provide a guided tour for an application.
    * @param scene {BabylonJS.scene}
    * @returns guidedTourArray {arr} Array with guidedTourDataObjects
    */
    getGuidedTourArray(scene) {
        var guidedTour = new GuidedTour();
        //return empty guided tour array
        return guidedTour.guidedTourArray;
    }

    /**
    * @desc Apply a default lighting preset
    * @param scene {BabylonJS.scene}
    * @param presetNum {int} Preset to activate in the specified scene (0=default, 1, 2, 3)
    */
    applyLightingPreset(scene, lighting, presetNum, vrModeActivated) {
        //the application currently expects a default light, and three other lights
        switch (presetNum) {
            case 1: //"museum lighting"
                //disable default HDR lighting
                lighting.disableHDRTexture(scene);
                lighting.resetShadowGenerator();

                //set scene bg color
                scene.clearColor = new BABYLON.Color3(0.9, 0.9, 0.9);

                //basic "fill" light
                var light0 = new BABYLON.HemisphericLight();
                light0.intensity = 1;

                break;

            case 2:
                //disable default HDR lighting
                lighting.disableHDRTexture(scene);
                lighting.resetShadowGenerator();

                //set scene bg color
                scene.clearColor = new BABYLON.Color3(0, 0, 0);

                //basic "fill" light
                var light1 = new BABYLON.HemisphericLight();
                light1.intensity = 3;
                light1.diffuse = new BABYLON.Color3(1, 0.57, 0.38);
                light1.specular = new BABYLON.Color3(1, 0.57, 0.38);

                break;

            case 3:
                //disable hdr lighting from default setting
                lighting.disableHDRTexture(scene);
                lighting.resetShadowGenerator();

                scene.clearColor = new BABYLON.Color3(1, 1, 1);

                //basic "fill" light
                var light = new BABYLON.HemisphericLight();
                light.intensity = 0.1;
                light.diffuse = new BABYLON.Color3(1, 0.57, 0.38);
                light.specular = new BABYLON.Color3(1, 0.57, 0.38)

                //light simulating sunlight coming in through the window
                var light3 = new BABYLON.SpotLight("spotLight", new BABYLON.Vector3(3.27, 14.5, 24), new BABYLON.Vector3(0.10, -3.16, 0.00), 2.565, 0, scene);
                light3.diffuse = new BABYLON.Color3(1, 0.57, 0.38);
                light3.specular = new BABYLON.Color3(1, 0.57, 0.38);
                light3.intensity = 40000;

                break;

            default: //default, "neutral light" setting    
                lighting.disableHDRTexture(scene);
                lighting.resetShadowGenerator();

                //for some reason, previously active lights remain visible in the scene, when creating a defaultEnvironment, 
                //*unless* we create a standard light first
                if (!vrModeActivated) {
                    var light = new BABYLON.HemisphericLight();
                    light.intensity = 0;
                }

                //set default bg color
                scene.clearColor = new BABYLON.Color3(0.9, 0.9, 0.9);

                //create the default environment (similar to sandbox)
                //using the default helper, various settings (e.g. PBR lighting) improve the visual display as opposed to only creating a default light
                var defaultEnvironmentHelper = scene.createDefaultEnvironment({
                    skyboxSize: 60,
                    createSkybox: false,
                    //load environmentTexture from local source instead of (default) assets.babylonjs.com server 
                    environmentTexture: 'data/textures/environmentSpecular.env',
                    //load empty/transparent ground png, since it has to be disposed below anyway
                    groundTexture: 'data/textures/backgroundGroundEmpty.png'
                });

                defaultEnvironmentHelper.ground.dispose();

                scene.imageProcessingConfiguration.toneMappingEnabled = true;
                scene.imageProcessingConfiguration.toneMappingType = BABYLON.TonemappingOperator.Photographic;
                defaultEnvironmentHelper.setMainColor(new BABYLON.Color3(1, 1, 1));

                break;
        }
    }
}

export default DefaultAppSettings;