import GuidedTour from "../../js/guided-tour.js";
import DefaultAppSettings from "../default-app-settings.js"

//Custom app settings. Documentation of used variables, see: default-app-settings.js
class AppSettings extends DefaultAppSettings {
    constructor(APP_DIRECTORY) {
        super();

        //Default values of these variables: ../default-app-settings.js
        /* App location info */
        this.APP_DIRECTORY = APP_DIRECTORY;
        this.MEDIA_DIRECTORY = this.getMediaDirectory();
        this.MESH_DIRECTORY = this.getMeshDirectory();
        this.CSV_DIRECTORY = this.getCSVDirectory();

        /* Camera settings */
        this.FIRST_PERSON_DEFAULT_POSITION = new BABYLON.Vector3(3, 0, 0);
        this.FIRST_PERSON_DEFAULT_TARGET = new BABYLON.Vector3(0, 0, 0);
        this.FIRST_PERSON_DEFAULT_ROTATION = new BABYLON.Vector3(-0.04, -2.26, 0.00);
        this.ORBIT_DEFAULT_POSITION = new BABYLON.Vector3(6.31, 3.02, -3.92);
        this.ORBIT_DEFAULT_TARGET = new BABYLON.Vector3(0, 0, 0);
        this.FIRST_PERSON_VR_DEFAULT_POSITION = new BABYLON.Vector3(-4.50, 0.81, 0.23);
        this.FIRST_PERSON_VR_DEFAULT_TARGET = new BABYLON.Vector3(9.54, 2.17, -1.99);
        //the ellipsoid determines the size of the "player" when navigating in first-person perspective (using arrow keys)
        this.CAMERA_DEFAULT_ELLIPSOID = new BABYLON.Vector3(0.1, 1, 0.1);
        //default camera (e.g., 'first-person', 'orbit')
        this.DEFAULT_CAMERA = 'orbit';

        /* Basic file information */
        this.MESH_DIRECTORY = this.MESH_DIRECTORY;
        this.MESH_NAME = 'sample.glb',
        this.CONTEXT_DATA_URL = this.CSV_DIRECTORY + 'sample-data.csv';
        //specific setting for BG radio
        this.PROCESS_PRIMITIVES = false;
        //process identifiers - extract prefix for mesh name and use it as an identifier
        this.PROCESS_PDG_IDS = false;
        //disable linked data for BG radio
        this.ACTIVATE_LINKED_DATA = false;

        /* Basic mesh info */
        //ground plane, which is used as a basis for gravity when navigating in first-person perspective. Make sure it is _below_ the camera position, otherwise you may "fall through"
        this.GROUND_PLANE_DEFAULT_POSITION = new BABYLON.Vector3(0, -1.2, 0);
        //set transparency of ground plane (0=fully transparent, 1=fully visible)
        this.GROUND_PLANE_ALPHA = 0.8;

        /* UI settings */
        //show confidence values for objects and object locations in item view (for Virtual Interiors 3D reconstructions: true). Needs corresponding data columns (COLUMN_ID_CONF_INDEX_OBJECT, COLUMN_ID_CONF_INDEX_LOCATION).
        this.SHOW_CONFIDENCE_VALUES = false;

        //position 3D panel (title+info plane) based on predefined position/rotation (visible when on mobile, VR platforms, or small viewport)
        this.INFOPANEL_3D_POSITION = new BABYLON.Vector3(0, 2.2, 0);
        this.INFOPANEL_3D_ROTATION = new BABYLON.Vector3(0, -1.6, 0);
        //margin between title and infopanel
        this.INFOPANEL_3D_MARGIN = 0.65;
        //plane inside panel container
        this.INFOPLANE_3D_WIDTH = 1.3;
        this.INFOPLANE_3D_HEIGHT = 1;
        this.INFOPLANE_3D_FONT_SIZE = 130; //pt
        //3D buttons
        this.BUTTON_PANEL_3D_POSITION = new BABYLON.Vector3(1.55, 1.5, 0);
        this.BUTTON_PANEL_3D_ROTATION = new BABYLON.Vector3(0, -1.6, 0);

        /* Default content settings */
        //basic info
        this.DEFAULT_TAB_GENERAL_TITLE = 'Sample application';
        this.DEFAULT_TAB_GENERAL_DESCRIPTION = 'A description of the application can be displayed here. Which may contain <a href="https://www.virtualinteriorsproject.nl" target="_blank">hyperlinks</a>.';
        this.DEFAULT_TAB_GENERAL_ABOUT = 'This represents a sample application, which can be used as a basis for creating a new application.';
        //up to 6 carousel items
        this.DEFAULT_TAB_GENERAL_IMAGE = this.MEDIA_DIRECTORY + 'geometry_sample_1.png';
        this.DEFAULT_TAB_GENERAL_IMAGE_CAPTION = 'Sample image: Hirschvogel, Geometria (1543). CC-0.';
        this.DEFAULT_TAB_GENERAL_IMAGE_2 = this.MEDIA_DIRECTORY + 'geometry_sample_2.png';
        this.DEFAULT_TAB_GENERAL_IMAGE_2_CAPTION = 'Sample image: Hirschvogel, Geometria (1543). CC-0.';

        /* default data column settings (-1 indicates not available) */
        //each number indicates a column in the CSV data (starting from 0, which is the first column in the data)
        this.COLUMN_ID_ID = 0;
        this.COLUMN_ID_TITLE = 1;
        this.COLUMN_ID_DESCRIPTION = 2;
        this.COLUMN_ID_IMAGE = 4;
        this.COLUMN_ID_IMAGE_CAPTION = 5;
        this.COLUMN_ID_CATEGORY = 3;
        this.COLUMN_ID_CATEGORY_URL = -1;
        this.COLUMN_ID_SIMILAR_OBJECTS = -1;
        this.COLUMN_ID_CONF_INDEX_OBJECT = -1;
        this.COLUMN_ID_CONF_INDEX_LOCATION = -1;
        this.COLUMN_ID_LOCATION = -1;
        this.COLUMN_ID_CLICKABLE = -1;
        this.COLUMN_ID_VR_DESCRIPTION = -1;
        this.COLUMN_ID_ECARTICO_ID = -1;
        this.COLUMN_ID_RIJKSMUSEUM_ID = -1;
        this.COLUMN_ID_ADAMNET_CATEGORIES = -1;
        this.COLUMN_ID_MODEL_CREATOR_NAME = -1;
        this.COLUMN_ID_MODEL_CREATOR_ORGANISATION = -1;
    }

    /**
    * @desc Getter which returns array of GuidedTourDataObjects (array of positions/rotations and display settings) (override default)
    * @param scene {BabylonJS.scene}
    * @returns guidedTourArray {arr} Array with guidedTourDataObjects
    */
    getGuidedTourArray(scene) {
        var guidedTour = new GuidedTour();

        //1 right-hand side view
        guidedTour.addGuidedTourPosition(new BABYLON.Vector3(5.36, 0.81, 2.80), new BABYLON.Vector3(0.13, -2.07, 0.00), 'Guided tour sample text 1');

        //2 birds-eye view
        guidedTour.addGuidedTourPosition(new BABYLON.Vector3(4.54, 4.06, -3.50), new BABYLON.Vector3(0.76, -0.97, 0.00), 'Guided tour sample text 2');

        return guidedTour.guidedTourArray;
    }

    /**
    * @desc Apply a default lighting preset (override default)
    * @param scene {BabylonJS.scene}
    * @param presetNum {int} Preset to activate in the specified scene (0=default, 1, 2, 3)
    */
    applyLightingPreset(scene, lighting, presetNum, vrModeActivated) {
        //the application currently expects a default light, and three other lights
        switch(presetNum) {
            case 1:
                //set scene bg color
                scene.clearColor = new BABYLON.Color3(255,255,255);
                scene.createDefaultLight();
                var light3 = new BABYLON.SpotLight("spotLight", new BABYLON.Vector3(0.11, 0.72, 2.31), new BABYLON.Vector3(-0.1, -0.2, -0.44), Math.PI / 2, 10, scene);
                light3.diffuse = new BABYLON.Color3(1, 1, 1);
                light3.specular = new BABYLON.Color3(1, 1, 1);
                light3.intensity = 8;
                break;
            case 2:
                var light = new BABYLON.HemisphericLight();
                light.intensity = 0.3;
                //set scene bg color
                scene.clearColor = new BABYLON.Color3(0.5,0.5,0.5);
                //Add additional point light (for seeing differences in different areas of the scene)
                var pointLight2 = new BABYLON.PointLight("pointLight", new BABYLON.Vector3(-3.5, 8, 5.16), scene);
                pointLight2.target = new BABYLON.Vector3(0,0,0);
                pointLight2.intensity = 300;
                lighting.addShadowsToMeshes(scene, pointLight2, 'PCF');                
                break;
            case 3:
                //dark, colored lights
                var light = new BABYLON.HemisphericLight();
                light.intensity = 0.3;
                //set scene bg color
                scene.clearColor = new BABYLON.Color3(0,0,0);
                var light1 = new BABYLON.SpotLight("spotLight", new BABYLON.Vector3(-1, 5, -1), new BABYLON.Vector3(0, -1, 0), Math.PI / 2, 10, scene);
                light1.diffuse = new BABYLON.Color3(1, 0, 0);
                light1.specular = new BABYLON.Color3(0, 1, 0);
                light1.intensity = 50;
                lighting.addShadowsToMeshes(scene, light1, 'PCF');
                var light2 = new BABYLON.SpotLight("spotLight", new BABYLON.Vector3(1, 5, 1), new BABYLON.Vector3(0, -1, 0), Math.PI / 2, 10, scene);
                light2.diffuse = new BABYLON.Color3(0, 1, 0);
                light2.specular = new BABYLON.Color3(0, 1, 0);
                light2.intensity = 50;
                lighting.addShadowsToMeshes(scene, light2, 'PCF');
                var light3 = new BABYLON.SpotLight("spotLight", new BABYLON.Vector3(0.11, 0.72, 2.31), new BABYLON.Vector3(-0.1, -0.2, -0.44), Math.PI / 2, 10, scene);
                light3.diffuse = new BABYLON.Color3(1, 1, 1);
                light3.specular = new BABYLON.Color3(1, 1, 1);
                light3.intensity = 10;
                lighting.addShadowsToMeshes(scene, light3, 'PCF');
                break;
            default:
                //set scene bg color
                scene.clearColor = new BABYLON.Color3(0.964,0.79,0.796);
                var light0 = new BABYLON.HemisphericLight();
                light0.intensity = 1;
                light0.diffuse = new BABYLON.Color3(1, 0.927, 0.871);
                light0.specular = new BABYLON.Color3(1, 0.927, 0.871);
        }
    }
}

export default AppSettings;