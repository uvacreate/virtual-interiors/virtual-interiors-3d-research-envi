/**
 * @classdesc MetadataObjectStorage saves objects containing information about meshes (specific column settings for each app are in data/apps/#/settings.js)
 */
class MetadataObjectStorage {
    constructor(settings) {
        //create empty array to store metadata objects in
        this.metadataObjectArray = [];
        //get column settings
        this.settings = settings;
    }

    /**
     * @desc get a metadata information object (from internal data store) for a mesh name
     * @param objID [name of mesh] {str}
     * @todo use metadata format as default (needs changing references in many other functions)
    */  
    getMeshDataObject(meshObjID) {
        for(var i=0; i<this.metadataObjectArray.length; i++) {
            if(this.metadataObjectArray[i].id.value == meshObjID) {
                return this.metadataObjectArray[i];
            }
        }
        //no matches found, return null
        //console.log('[warning] Object ID: ' + objID + ' not found in metadata array');
        return null;
    }

    /**
     * @desc Create a metadata information object (from object information CSV) for a mesh name
    */   
    saveMeshInformationMetadata(contextualDataArray) {
        //default column values defined in main settings
        //array column indices for contextual data CSV
        var index_id = this.settings.getColumnIndexNumberContextualData('id');
        var index_title = this.settings.getColumnIndexNumberContextualData('title');
        var index_description = this.settings.getColumnIndexNumberContextualData('description');
        var index_vrDescription = this.settings.getColumnIndexNumberContextualData('vrDescription');
        var index_image = this.settings.getColumnIndexNumberContextualData('image');
        var index_imageCaption = this.settings.getColumnIndexNumberContextualData('imageCaption');
        var index_category = this.settings.getColumnIndexNumberContextualData('category');
        var index_categoryUrl = this.settings.getColumnIndexNumberContextualData('categoryUrl');
        var index_similarObjects = this.settings.getColumnIndexNumberContextualData('similarObjects');
        var index_confIndexObject = this.settings.getColumnIndexNumberContextualData('confIndexObject');
        var index_confIndexLocation = this.settings.getColumnIndexNumberContextualData('confIndexLocation');
        var index_location = this.settings.getColumnIndexNumberContextualData('location');
        var index_clickable = this.settings.getColumnIndexNumberContextualData('clickable');
        var index_ecarticoID = this.settings.getColumnIndexNumberContextualData('ecarticoID');
        var index_rijksmuseumID = this.settings.getColumnIndexNumberContextualData('rijksmuseumID');
        var index_adamnetCategories = this.settings.getColumnIndexNumberContextualData('adamnetCategories');
        var index_modelCreatorName = this.settings.getColumnIndexNumberContextualData('modelCreatorName');
        var index_modelCreatorOrganisation = this.settings.getColumnIndexNumberContextualData('modelCreatorOrganisation');

        //skip first row since that contains the column headers
        for(var i=1; i<contextualDataArray.length; i++) {
            if(contextualDataArray[i][index_id] != null) {
                var metadataObject = {
                    id: { label: 'ID', value: contextualDataArray[i][index_id] },
                    title: { label: contextualDataArray[0][index_title], value: contextualDataArray[i][index_title]},
                    description: { label: contextualDataArray[0][index_description], value: contextualDataArray[i][index_description] },
                    vrDescription: { label: contextualDataArray[0][index_vrDescription], value: contextualDataArray[i][index_vrDescription] },
                    category: { label: contextualDataArray[0][index_category], value: contextualDataArray[i][index_category], url: contextualDataArray[i][index_categoryUrl] },
                    similarObjects: { label: contextualDataArray[0][index_similarObjects], value: contextualDataArray[i][index_similarObjects] },
                    location: { label: contextualDataArray[0][index_location], value: contextualDataArray[i][index_location] },
                    image: { label: contextualDataArray[i][index_imageCaption], url: contextualDataArray[i][index_image] }, //caption??
                    confidenceIndexObject: { label: contextualDataArray[0][index_confIndexObject], value: contextualDataArray[i][index_confIndexObject] },
                    confidenceIndexLocation: { label: contextualDataArray[0][index_confIndexLocation], value: contextualDataArray[i][index_confIndexLocation] },
                    clickable: { label: contextualDataArray[0][index_clickable], value: true },
                    ecarticoID: { label: contextualDataArray[0][index_ecarticoID], value: contextualDataArray[i][index_ecarticoID] },
                    rijksmuseumID: { label: contextualDataArray[0][index_rijksmuseumID], value: contextualDataArray[i][index_rijksmuseumID] },
                    adamnetCategories: { label: contextualDataArray[0][index_adamnetCategories], value: contextualDataArray[i][index_adamnetCategories] },
                    modelCreatorName: { label: contextualDataArray[0][index_modelCreatorName], value: contextualDataArray[i][index_modelCreatorName] },
                    modelCreatorOrganisation: { label: contextualDataArray[0][index_modelCreatorOrganisation], value: contextualDataArray[i][index_modelCreatorOrganisation] }
                }

                //overwrite default clickable value (true)
                if(index_clickable != -1) {
                    if(contextualDataArray[i][index_clickable] == 'TRUE') {
                        metadataObject.clickable.value = true;
                    } else if(contextualDataArray[i][index_clickable] == 'FALSE') {
                        metadataObject.clickable.value = false;
                    }
                }

                //console.log(metadataObject);
                //add metadata object to general array
                this.metadataObjectArray.push(metadataObject);
            }
        }
    }

    /**
    * @desc Set visibility metadata
    * @param meshID {str}
    * @param visibilityBool {Bool} Bool indicating if mesh should be visible
    * @todo find better way to store this info, now set as "clickable" value
    */
    setMeshHypothesisVisibilityMetadata(meshID, visibilityBool) {
        for(var i=0; i<this.metadataObjectArray.length; i++) {
            if(this.metadataObjectArray[i].id.value == meshID) {
                this.metadataObjectArray[i].clickable.value = visibilityBool;
                break;
            }
        }
    }

    /**
     * @descGet Get the number of data objects {int}
    */
    get numberOfMetadataObjects() {
        return this.metadataObjectArray.length;
    }

}

export default MetadataObjectStorage;