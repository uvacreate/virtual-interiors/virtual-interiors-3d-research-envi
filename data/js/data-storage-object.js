/**
 * @classdesc DataStorageObject creates an object in a custom triple-like format (for storing data). Currently only used for annotations.
 */
 class DataStorageObject {
    constructor(subject, relationLabel, object, isBidirectional) {
        this._subject = subject;
        this._relationLabel = relationLabel;
        this._object = object;
        if(isBidirectional == null) { 
            isBidirectional = false
        }
        this._isBidirectional = isBidirectional;
    }

    /**
    * @desc Get subject of data storage object triple
    */   
    get subject() {
        return this._subject;
    }

    /**
    * @desc Get relation label of data storage object triple
    */   
    get relationLabel() {
        return this._relationLabel;
    }

    /**
    * @desc Get object of data storage object triple
    */   
    get object() { 
        return this._object;
    }

    /**
    * @desc Get Boolean if data storage object triple is bidirectional
    */   
    get isBidirectional() {
        return this._isBidirectional;
    }
}

export default DataStorageObject;