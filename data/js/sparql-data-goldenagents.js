import SPARQLData from "./sparql-data.js";

/**
 * @classdesc SPARQLDataGoldenAgents contains functions to load SPARQL data from Golden Agents
 */
class SPARQLDataGoldenAgents extends SPARQLData {
    constructor() {
        super();
        //public sparql endpoint
        this.endpointUrl = 'https://sparql.goldenagents.org/';
        //label for this data source
        this.dataSourceLabel = 'Rijksmuseum (via Golden Agents)';
        //URL with information about data source
        this.dataSourceURL = 'https://www.goldenagents.org';

        //query to get artist / artwork information from golden agents, uses rijksmuseum URI
        this.SPARQL_Q_GET_RIJKSM_WORK_INFORMATION = 'PREFIX dc: <http://purl.org/dc/elements/1.1/>   SELECT DISTINCT ?work ?workYear ?workImage ?workLabel WHERE {   BIND(<_VAR1_> as ?work).  ?work dc:title ?workLabel.  FILTER langMatches( lang(?workLabel), "EN" ).   ?work <http://purl.org/dc/terms/created> ?workYear. FILTER langMatches( lang(?workYear), "EN" ).    ?workAggr <http://www.europeana.eu/schemas/edm/aggregatedCHO> ?work.    ?workAggr <http://www.europeana.eu/schemas/edm/isShownBy> ?workImage.    } LIMIT 10';
    }
    /**
    * @desc Load SPARQL data from adamnet b/o ecarticoID
    * @param ecarticoID {str}
    * @returns obj with results (todo: merge)
    */
    async getRijksmuseumDataForURI(rijksmuseumURI) {
        return this.getWorksObjectBasedOnSingleVariableQuery(rijksmuseumURI, this.SPARQL_Q_GET_RIJKSM_WORK_INFORMATION, this.endpointUrl, this.dataSourceLabel, this.dataSourceURL);
    }
}

export default SPARQLDataGoldenAgents;