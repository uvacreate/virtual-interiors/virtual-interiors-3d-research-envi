/**
 * @classdesc Camera class manages everything related to the camera, including collision detection (with the camera), gravity of the camera and visual filters
 */
class Camera {
    constructor(settings) {
        //default camera settings
        this._currentSetting;
        //variable for default babylonJS XR helper
        this._xrHelper;
        //var to know if XR mode is active
        this._xrIsActive = false;
        //variable for ambient occlusion pipeline
        this._ssao;
        //default rendering pipeline, used for lens effects, e.g. bloom (see enableLensEffects())
        this._defaultRenderingPipeline;

        //default camera positions
        //default settings from settings.js
        this.FIRST_PERSON_DEFAULT_POSITION = settings.FIRST_PERSON_DEFAULT_POSITION;
        this.FIRST_PERSON_DEFAULT_TARGET = settings.FIRST_PERSON_DEFAULT_TARGET;
        this.FIRST_PERSON_DEFAULT_ROTATION = settings.FIRST_PERSON_DEFAULT_ROTATION;
        this.ORBIT_DEFAULT_POSITION = settings.ORBIT_DEFAULT_POSITION;
        this.ORBIT_DEFAULT_TARGET = settings.ORBIT_DEFAULT_TARGET;
        this.ORBIT_DEFAULT_ALPHA = settings.ORBIT_DEFAULT_ALPHA; 
        this.ORBIT_DEFAULT_BETA = settings.ORBIT_DEFAULT_BETA;  
        this.ORBIT_DEFAULT_RADIUS = settings.ORBIT_DEFAULT_RADIUS;
        this.FIRST_PERSON_VR_DEFAULT_TARGET = settings.FIRST_PERSON_VR_DEFAULT_POSITION;
        this.FIRST_PERSON_VR_DEFAULT_POSITION = settings.FIRST_PERSON_VR_DEFAULT_TARGET;
        this.CAMERA_DEFAULT_ELLIPSOID = settings.CAMERA_DEFAULT_ELLIPSOID;
        this.DEFAULT_CAMERA = settings.DEFAULT_CAMERA;
        this.SCENE_GROUND_PLANE_NAME = settings.SCENE_GROUND_PLANE_NAME;
        this.GROUND_PLANE_DEFAULT_POSITION = settings.GROUND_PLANE_DEFAULT_POSITION;
        this.CEILING_MESH_NAME = settings.CEILING_MESH_NAME;

        this._doNotSetDefaultVRCameraProperty = false;
    }

    /**
     * @desc Activate a certain camera
     * @param scene {obj}
     * @param canvas {Canvas} HTML5 Canvas element
     * @param cameraSetting {str} ('orbit', 'first-person', 'phone-first-person', vr-first-person', 'vr-orbit', 'cardboard-vr-first-person')
    */
    set(scene, canvas, cameraSetting) {
        if(cameraSetting == null) {
            if(this.currentSetting == null) {
                //set the default camera
                cameraSetting = this.DEFAULT_CAMERA;
            } else {
                //set the currently defined camera setting
                cameraSetting = this.currentSetting;
            }
        }
        //set global var
        this.currentSetting = cameraSetting;

        //set (switch) the current camera in the application
        var currCam;

        //reset XR experience helper to avoid "enter XR" button on each screen
        if(this.xrHelper) {
            this.xrHelper.dispose(); 
            this.xrHelper = null;
        };
        
        if(cameraSetting == 'orbit') {
            //rotating camera, 'orbit' view
            currCam = new BABYLON.ArcRotateCamera("Camera", this.ORBIT_DEFAULT_ALPHA, this.ORBIT_DEFAULT_BETA, this.ORBIT_DEFAULT_RADIUS, this.ORBIT_DEFAULT_TARGET, scene);
            currCam.position = this.ORBIT_DEFAULT_POSITION;
            currCam.fov = 1;
            
            //set lower and upper limits for zooming in and out (to prevent e.g. upside down views)
            currCam.lowerRadiusLimit = 1;
            currCam.upperRadiusLimit = 30;
            //change mouse wheel precision for zooming (or multitouch zoom mac); otherwise too sensitive
            currCam.wheelPrecision = 100;
            
            this.disableGravity(scene, currCam);
        } else if(cameraSetting == 'first-person') {
            //Add universal (first person) camera or free camera (difference ??)
            currCam = new BABYLON.UniversalCamera("UniversalCamera", this.FIRST_PERSON_DEFAULT_POSITION, scene);
            
            //Set target to main elements scene		
            currCam.setTarget(this.FIRST_PERSON_DEFAULT_TARGET);
            currCam.rotation = this.FIRST_PERSON_DEFAULT_ROTATION;
            currCam.fov = 1.2;
            
            this.enableGravity(scene, currCam);
        } else if(cameraSetting == 'phone-first-person') {
            //ask for permission to use device orientation events (iOS)
            this.requestDeviceOrientationAccess();
            
            // Parameters : name, position, scene
            currCam = new BABYLON.DeviceOrientationCamera("DevOr_camera", this.FIRST_PERSON_DEFAULT_POSITION, scene);

            // Targets the camera to a particular position
            currCam.setTarget(this.FIRST_PERSON_DEFAULT_TARGET);
            currCam.rotation = this.FIRST_PERSON_DEFAULT_ROTATION;
            currCam.fov = 1.2;

            // Sets the sensitivity of the camera to movement and rotation
            currCam.angularSensibility = 10;
            currCam.moveSensibility = 10;
            currCam.setTarget(this.FIRST_PERSON_DEFAULT_TARGET);
            
            this.enableGravity(scene, currCam);
        } else if(cameraSetting == 'vr-first-person') { 
            currCam = this.activateVRCamera(scene, 'vr', 'first-person');
        } else if(cameraSetting == 'vr-orbit') { 
            currCam = this.activateVRCamera(scene, 'vr', 'orbit');
        } else if(cameraSetting == 'cardboard-vr-first-person') {
            currCam = this.activateVRCamera(scene, 'cardboard', 'first-person');
        } else {
            console.log('[error] No valid camera specified: ' + cameraSetting);
        }
        
        //fix wall clipping issue
        currCam.minZ = -1; //0.01
        
        //Perhaps: set the ellipsoid around the camera (e.g. your player's size)
        currCam.ellipsoid = this.CAMERA_DEFAULT_ELLIPSOID;
        
        if(cameraSetting == 'vr-first-person') {
            //prevent objects getting too close to camera
            currCam.minZ = 0.3; 
        }

        //now also set in setobjecttrans [fixme]
        if(cameraSetting == 'orbit') {
            //do not show ceiling in orbit mode
            this.hideCeiling(scene);
        } else {
            this.showCeiling(scene);
        }
        
        //reduce camera speed
        currCam.speed = 0.1;
        currCam.attachControl(canvas, true);

        //in XR, do not use ambient occlusion for performance purposes
        if(!this.xrIsActive) { 
            //disableAmbientOcclusion();
        }

        //activate camera
        scene.activeCamera = currCam;

        //if ssao (ambient occlusion) is enabled, attach it to to the current camera
        if(this.ssao) {
            scene.postProcessRenderPipelineManager.attachCamerasToRenderPipeline("ssao", scene.activeCamera);
        }
    }

    /**
     * @desc Activate the VR camera
     * @param platform {str} ('vr', 'cardboard')
     * @param cameraPerspective {str} ('first-person', 'orbit')
     * @todo Change structure
    */
    activateVRCamera(scene, platform, cameraPerspective) {
        var currCam;
        //ask for permission to use device orientation events (iOS)
        this.requestDeviceOrientationAccess();
        var metrics = BABYLON.VRCameraMetrics.GetDefault();
        
        //first, set the perspective
        if(cameraPerspective=='first-person') {
            //Parameters: name, position, scene, compensateDistortion, vrCameraMetrics
            var firstPersonVRPos, firstPersonVRTarg;
            if(!this.doNotSetDefaultVRCameraProperty) {
                firstPersonVRPos = this.FIRST_PERSON_VR_DEFAULT_POSITION;
                firstPersonVRTarg = this.FIRST_PERSON_VR_DEFAULT_TARGET;
            } else {
                firstPersonVRPos = scene.activeCamera.position;
                firstPersonVRTarg = scene.activeCamera.getTarget();
            }
            currCam = new BABYLON.VRDeviceOrientationFreeCamera("Camera",  firstPersonVRPos, scene, true, metrics);
            currCam.setTarget(firstPersonVRTarg);

            this.enableGravity(scene, currCam);
        } else {
            console.log('[error] No valid perspective specified');
        }
        
        //second, set the type of camera
        if(platform=='vr') {
            this.setupXRHelper(scene);	
        } else if(platform=='cardboard') {
            //VRHelper does not work on ios, do not use for now
        } else {
            console.log('[error] No valid platform specified');
        }
        return currCam;
    }

    /**
  * @desc WebXR experience helper
*/
async setupXRHelper(scene) {
	this.xrHelper = await scene.createDefaultXRExperienceAsync( {
		useMultiview : true,
		floorMeshes: [scene.getMeshByName(this.SCENE_GROUND_PLANE_NAME)],
		uiOptions: {
			referenceSpaceType:'local-floor'
		}
		//Note that the user height depends on the type of reference space you chose to your experience. Reference space type local-floor will deliver the user's height, but viewer (for example) will deliver what you defined as a height compensation in the XR Session Manager.
	});
	this.xrHelper.baseExperience.onInitialXRPoseSetObservable.add((xrCamera) => {
		console.log('[log] finished preparing xr session, set camera now');
		//FP viewpoint level now at y === 2.64
		//xrCamera.y = 2.64;
		var x = this.FIRST_PERSON_VR_DEFAULT_POSITION.x;
		//take elevation of ground as a basis for y position xrCamera
		var y = this.GROUND_PLANE_DEFAULT_POSITION.y;
		var z = this.FIRST_PERSON_VR_DEFAULT_POSITION.z;
		xrCamera.position = new BABYLON.Vector3(x, y, z);
	});
	this.xrHelper.input.xrSessionManager.onXRSessionInit.add(() => {
		console.log('[log] init xr session');
		this.xrIsActive = true;
	});
	this.xrHelper.input.xrSessionManager.onXRSessionEnded.add(() => {
		console.log('[log] exit xr session');
		this.xrIsActive = false;
	});
    }

    /**
     * @desc Enable gravity and collisions for the scene and current camera (needed for FPS movement)
     * @param camera {BABYLON.Camera} camera to activate gravity for
    */
    enableGravity(scene, camera) {
        //activate collision detection, right now only for the ground plane
        this.activateCollisionDetection([scene.getMeshByName(this.SCENE_GROUND_PLANE_NAME)]);
        //check collisions for camera and scene
        camera.checkCollisions = true;
        scene.collisionsEnabled = true;
        //Gravity vector defines the G-force. In a classic world such as Earth, the direction of the force of gravity is down (negative) along the Y axis
        scene.ellipsoid = this.CAMERA_DEFAULT_ELLIPSOID;
        scene.gravity = new BABYLON.Vector3(0, -9.81, 0);
        //apply gravity to camera
        camera.applyGravity = true;
    }

    /**
     * @desc Disable gravity and collisions for the scene and current camera
     * @param camera {BABYLON.Camera} camera to activate gravity for
    */
    disableGravity(scene, camera) {
        //deactivate collision detection, right now only for the ground plane
        this.deactivateCollisionDetection([scene.getMeshByName(this.SCENE_GROUND_PLANE_NAME)]);
        //do not check collisions for camera and scene
        camera.ellipsoid = new BABYLON.Vector3(0.01,0.01,0.01);
        camera.checkCollisions = false;
        scene.collisionsEnabled = false;
        //remove gravity from camera
        camera.applyGravity = false;
    }

    /**
     * @desc Turn on collision detection for a set of meshes
     * @param meshes {arr}
     * @todo Create parameters for meshes to do collision detection for
    */
    activateCollisionDetection(meshes) {
        //activate collision detection for (a set of) meshes (e.g., GLTF/GLB object)
        for (var i=0; i<meshes.length; i++) {
            meshes[i].checkCollisions = true;
        }
    }

    /**
     * @desc Turn off collision detection for a set of meshes
     * @param meshes {arr}
    */
    deactivateCollisionDetection(meshes) {
        //deactivate collision detection for (a set of) meshes (e.g., GLTF/GLB object)
        for (var i=0; i<meshes.length; i++) {
            meshes[i].checkCollisions = false;	
        }
    }

    /**
     * @desc Show the ceiling mesh
     * @todo Function only works after the data has been loaded (so not the first time setcamera is called)
    */
    showCeiling(scene) {
        var ceilingMesh = scene.getMeshByName(this.CEILING_MESH_NAME);
        if(ceilingMesh) {
            ceilingMesh.visibility = 1;
            ceilingMesh.isPickable = true;
        }
    }

    /**
     * @desc Hide the ceiling mesh
     * @todo Function only works after the data has been loaded (so not the first time setcamera is called)
    */
    hideCeiling(scene) {
        var ceilingMesh = scene.getMeshByName(this.CEILING_MESH_NAME);
        if(ceilingMesh) {
            ceilingMesh.visibility = 0;
            ceilingMesh.isPickable = false;
        }
    }

    /**
  * @desc toggle Ambient Occlusion (SSAO) (LQ / HQ / off). LQ uses less system resources than HQ, but less precise. LQ setting works in Chrome, not Firefox in current BJS version. HQ setting seems to work in both.
  * @param cam {obj}
    */
    toggleAmbientOcclusion(scene, cam) {
        if(this.ssao != null) {
            if(this.ssao.totalStrength == 0.5) { 
                console.log('[log] Ambient Occlusion: high quality setting');
                this.setAmbientOcclusionHQ();
            } else {
                //turn it off
                console.log('[log] Ambient Occlusion: off');
                this.disableAmbientOcclusion();
            }
        } else {
            console.log('[log] Ambient Occlusion: on (LQ)');
            this.enableAmbientOcclusion(scene, cam);
        }
    }

    /**
     * @desc Ambient Occlusion HQ setting (slower)
    */
    setAmbientOcclusionHQ() {
        //high quality setting
        this.ssao.totalStrength = 1;
        this.ssao.samples = 14;
    }

    /**
     * @desc Disable Ambient Occlusion
    */
    disableAmbientOcclusion() {
        if(this.ssao != null) {
            this.ssao.dispose();
            this.ssao = null;
        }
    }

    /**
     * @desc Enable Ambient Occlusion (standard settings, LQ, faster)
     * @param cam {BABYLON.Camera}
    */
    enableAmbientOcclusion(scene, cam) {
        if(this.ssao == null) {
            this.ssao = new BABYLON.SSAO2RenderingPipeline("ssao", scene, 0.5);
        }

        //ssao settings: 0.5 / totalstrength 1; samples: 14 = 10/11 FPS HQ ; totalstrength .5, samples: 4 = 16/17 FPS
        //default settings
        this.ssao.radius = 0.4;
        this.ssao.totalStrength = 0.5; //1    
        this.ssao.base = 0.15;
        this.ssao.expensiveBlur = false;
        this.ssao.samples = 4; //14
        this.ssao.minZAspect = 0.5;
        this.ssao.maxZ = 40;
        this.ssao.textureSamples = 0;

        // Attach camera to the SSAO render pipeline
        scene.postProcessRenderPipelineManager.attachCamerasToRenderPipeline("ssao", cam);
    }

    /**
     * @desc Toggle lens effects (experimental)
     * @param cam {Camera}
    */
    toggleLensEffects(scene, cam) {
        if(!this.defaultRenderingPipeline) {
            this.enableLensEffects(scene, cam, true);
        } else {
            this.disableLensEffects();
        }
    }

    /**
     * @desc Enable lens effects (experimental)
     * @param cam {BABYLON.Camera}
     * @param depthOfField {Boolean}
     * @todo see: https://doc.babylonjs.com/how_to/using_depth-of-field_and_other_lens_effects
     * @todo see: https://endoc.cnbabylon.com/api/classes/babylon.defaultrenderingpipeline.html
    */

    enableLensEffects(scene, cam, depthOfFieldProperty) {
        if(!this.defaultRenderingPipeline) { 
            this.defaultRenderingPipeline = new BABYLON.DefaultRenderingPipeline("default", true, scene, [cam]);
        }
        //bloom settings
        this.defaultRenderingPipeline.bloomScale = 0.25;
        this.defaultRenderingPipeline.bloomWeight = 0.2;
        this.defaultRenderingPipeline.bloomEnabled = false;
        //optionally activate depth of field (needs processing power)
        if(depthOfFieldProperty != null) {
            this.defaultRenderingPipeline.depthOfField.focusDistance  = 2500; // distance of the current focus point from the camera in millimeters considering 1 scene unit is 1 meter
            this.defaultRenderingPipeline.depthOfField.focalLength  = 50; // focal length of the camera in millimeters
            this.defaultRenderingPipeline.depthOfField.fStop  = 0.5; // aka F number of the camera defined in stops as it would be on a physical device
            this.defaultRenderingPipeline.depthOfFieldEnabled = true;
        } else {
            this.defaultRenderingPipeline.depthOfFieldEnabled = false;
        }
        //antialiasing settings
        this.defaultRenderingPipeline.fxaaEnabled = true;
        //fov settings
        this.defaultRenderingPipeline.cameraFov = cam.fov;
            console.log('[log] enabling lens effects');
    }

    /**
     * @desc Disable lens effects (experimental)
     * @todo see: https://doc.babylonjs.com/how_to/using_depth-of-field_and_other_lens_effects
     * @todo see: https://endoc.cnbabylon.com/api/classes/babylon.defaultrenderingpipeline.html
    */
    disableLensEffects() {
        if(this.defaultRenderingPipeline) {
            //dispose of the rendering pipeline and make var null
            this.defaultRenderingPipeline.dispose();
            this.defaultRenderingPipeline = null;
        }
        console.log('[log] disabling lens effects');
    }

    /* ******** camera position functions ********* */

    /**
     * @desc Animate a viewpoint, using viewpoint transition animation
     * @param position {BABYLON.Vector3}
     * @param rotation {BABYLON.Vector3} [optional]
    */
    animateViewPoint(cam, newPosition, newRotation, frameCount) {
        var speed = 45;
        if(frameCount == null) {
            frameCount = 300;
        }
        this.animateCameraToPosition(cam, speed, frameCount, newPosition);
        if(newRotation != null) {
            this.animateCameraRotationToPosition(cam, speed, frameCount, newRotation);
        }
    }

    /**
     * @desc Animate a camera's rotation 
     * @param camera {BABYLON.Camera}
     * @param speed {int}
     * @param frameCount {int}
     * @param newPos {Vector3}
    */
    animateCameraRotationToPosition(cam, speed, frameCount, newPos) {
        var ease = new BABYLON.CubicEase();
        ease.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
        BABYLON.Animation.CreateAndStartAnimation('at5', cam, 'rotation', speed, frameCount, cam.rotation, newPos, 0, ease);
    }

    /**
     * @desc Animate a camera's position 
     * @param camera {BABYLON.Camera}
     * @param speed {int}
     * @param frameCount {int}
     * @param newPos {Vector3}
    */
    animateCameraToPosition(cam, speed, frameCount, newPos) {
        var ease = new BABYLON.CubicEase();
        ease.setEasingMode(BABYLON.EasingFunction.EASINGMODE_EASEINOUT);
        BABYLON.Animation.CreateAndStartAnimation('at4', cam, 'position', speed, frameCount, cam.position, newPos, 0, ease);
    }

    /**
     * @desc Request device orientation access, to use for moving camera view (iOS 13+)
     * @todo only applies to iOS devices
    */
    requestDeviceOrientationAccess() {
        try {
           // DeviceOrientationEvent.requestPermission()
        }
        catch(err) {
            //only possible on ios device?
            console.log(err);
        }
    }

    /**
     * @desc Get current camera setting {Str}
    */
    get currentSetting() {
        return this._currentSetting;
    }

    /**
     * @desc Set current camera setting {Str}
    */
    set currentSetting(camSetting) {
        this._currentSetting = camSetting;
    }

    /**
     * @desc Get if XR is active or not {Bool}
    */
    get xrIsActive() {
        return this._xrIsActive;
    }

    /**
     * @desc Set if XR is active or not {Bool}
    */
    set xrIsActive(bool) {
        this._xrIsActive = bool;
    }

    /**
     * @desc Get SSAO object {BABYLON.SSAO2RenderingPipeline}
    */
    get ssao() {
        return this._ssao;
    }

    /**
     * @desc Set SSAO object {BABYLON.SSAO2RenderingPipeline}
    */
    set ssao(val) {
       this._ssao = val;
    }

    /**
     * @desc Get default rendering pipeline {BABYLON.DefaultRenderingPipeline}
    */
    get defaultRenderingPipeline() {
        return this._defaultRenderingPipeline;
    }

    /**
     * @desc Set default rendering pipeline {BABYLON.DefaultRenderingPipeline}
    */
    set defaultRenderingPipeline(val) {
        this._defaultRenderingPipeline = val;
    }

    /**
     * @desc Get optional value setting (do not set camera settings to default when switching camera) {Bool}
    */
    get doNotSetDefaultVRCameraProperty() {
        return this._doNotSetDefaultVRCameraProperty;
    }


    /**
     * @desc Set optional value setting (do not set camera settings to default when switching camera) {Bool}
    */
    set doNotSetDefaultVRCameraProperty(val) {
        this._doNotSetDefaultVRCameraProperty = val;
    }
}

export default Camera;