/**
 * @classdesc Lighting class manages lighting presets in a scene (specific lighting presets are in data/apps/default-app-settings.js (default settings) and data/apps/#/app-settings.js for each application.
*/
class Lighting {
    constructor() {
        this._activeLightsSetting;
        this._settings;
        this.shadowGenerator;
        this.hdrTexture;
    }

    /**
     * @desc Remove all lights from a scene
    */
    removeLights(scene) {
        //removes all lights from scene
        if (scene.lights.length > 0) {
            for (var i = 0; i < scene.lights.length; i++) {
                //some issues/bugs in babylonjs regarding lights: also set intensity to 0
                scene.lights[i].intensity = 0;
                //finally remove the light (can also be via ..dispose(), but this might give WebGL errors)
                scene.removeLight(scene.lights[i]);
            }
        }
        scene.lights = [];
    }

    /**
     * @desc Apply a basic lights setting (activated in app-settings.js)
     * @param numSetting {int} ID of the setting (1-3)
    */
    activateLightsSetting(scene, settings, cameraSetting, numLightSetting) {
        this.activeLightsSetting = numLightSetting;
        this.settings = settings;

        //reset lights
        this.removeLights(scene);

        //do not show shadows in VR (or other 3DGUI mode)
        var vrModeActivated = false;
        if (cameraSetting == 'vr-first-person') {
            vrModeActivated = true;
        }
        this.settings.applyLightingPreset(scene, this, numLightSetting, vrModeActivated);
    }

    /**
     * @desc Toggler for lighting setting (used in VR)
     * @todo uses settings stored by activateLightsSetting
    */
    toggleLightsSettingVR(scene) {
        var currentLightsSetting = this.activeLightsSetting;
        if (currentLightsSetting < 3) {
            currentLightsSetting++;
        } else {
            currentLightsSetting = 0;
        }
        if (this.settings != null) {
            this.activateLightsSetting(scene, this.settings, 'vr-first-person', currentLightsSetting);
        }
    }

    /**
       * @desc Adding a shadow filter to all meshes in the scene
       * @param pointLight {BABYLON.PointLight} babylon pointLight
     */
    addShadowsToMeshes(scene, pointLight, shadowType, biasSetting, normalBiasSetting, darknessSetting, penumbraSetting, highQualitySetting) {
        //PCF shadows benefit from the new hardware filtering functions available in Webgl2 and produce a smoother version of Poisson sampling.
        /*if(shadowGenerator) {
          shadowGenerator.dispose();
        }*/
        this.shadowGenerator = new BABYLON.ShadowGenerator(2048, pointLight);
        //use PCSS (might be too much for mobile platforms)

        if (biasSetting) {
            this.shadowGenerator.bias = biasSetting;
        }
        if (normalBiasSetting) {
            this.shadowGenerator.normalBias = normalBiasSetting;
        }
        if (darknessSetting) {
            this.shadowGenerator.setDarkness(darknessSetting);
        }

        this.shadowGenerator.enableSoftTransparentShadow = true;

        if (shadowType == null || shadowType == 'PCF') {
            //PCF
            this.shadowGenerator.usePercentageCloserFiltering = true;
            //As PCF requires more resources than can be available on small platforms, you can use the filteringQuality property to choose the best tradeoff between quality and performance depending on your experience (the lower the quality the bett/er the performance).

            if (highQualitySetting) {
                this.shadowGenerator.filteringQuality = BABYLON.ShadowGenerator.QUALITY_HIGH;
            } else {
                this.shadowGenerator.filteringQuality = BABYLON.ShadowGenerator.QUALITY_LOW;
            }

        } else if (shadowType = 'PCSS') {
            this.shadowGenerator.useContactHardeningShadow = true;
            //less smooth edges of the shadows
            this.shadowGenerator.filteringQuality = BABYLON.ShadowGenerator.QUALITY_LOW;
            //only for PCSS: penumbra is the region in which only a portion of the light source is obscured by the occluding body
            if (penumbraSetting) {
                contactHardeningLightSizeUVRatio = penumbraSetting;
            }
        }
        var meshes = scene.meshes;
        for (var i = 0; i < meshes.length; i++) {
            this.shadowGenerator.getShadowMap().renderList.push(meshes[i]);
            meshes[i].receiveShadows = true;
        }
    }

    /**
       * @desc Adding a "candle" animation to a certain light
       * @param scene {BABYLON.Scene} babylon scene
       * @param light {BABYLON.Light} babylon light which to apply animation to
     */
    addCandleAnimationToLight(scene, light) {
        var keys = [];
        var previous = null;
        for (var i = 0; i < 20; i++) {
            var rand = BABYLON.Scalar.Clamp(Math.random() * 10, 4, 15);
            if (previous) {
                if (Math.abs(rand - previous) < 0.1) {
                    continue;
                }
            }
            previous = rand;
            keys.push({
                frame: i,
                value: rand
            });
        }
        var anim = new BABYLON.Animation("anim", "intensity", 1, BABYLON.Animation.ANIMATIONTYPE_FLOAT, BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
        anim.setKeys(keys);
        light.animations.push(anim);
        scene.beginAnimation(light, 0, keys.length, true, 8);
    }

    /**
     * @desc Dispose of shadow generator
    */
    resetShadowGenerator() {
        if (this.shadowGenerator) {
            this.shadowGenerator.dispose();
        }
    }

    /**
     * @desc disable hdr lighting from default setting
    */
    disableHDRTexture(scene) {
        scene.environmentTexture = null;
        this.hdrTexture == null;
    }

    /**
     * @desc Getter for active lights setting
    */
    get activeLightsSetting() {
        return this._activeLightsSetting;
    }

    /**
     * @desc Setter for active lights setting
     * @param lightsSetting {int} 0-3
    */
    set activeLightsSetting(lightsSetting) {
        this._activeLightsSetting = lightsSetting;
    }

    /**
     * @desc Getter for active lights setting (0-3)
    */
    get settings() {
        return this._settings;
    }

    /**
     * @desc Getter for active lights setting (0-3)
    */
    set settings(val) {
        this._settings = val;
    }
}

export default Lighting;