import SPARQLData from "./sparql-data.js";

/**
 * @classdesc SPARQLDataAdamnet contains functions to load SPARQL data from Adamnet.
 */
class SPARQLDataAdamnet extends SPARQLData {
    constructor() {
        super();
        //public sparql endpoint
        this.endpointUrl = 'https://api.druid.datalegend.net/datasets/AdamNet/Heritage/services/Heritage/sparql';
        //label for this data source
        this.dataSourceLabel = 'Adamnet';
        //URL with information about data source
        this.dataSourceURL = 'https://www.adamlink.nl';

        //query to get artist / artwork information from adamnet, uses ecartico id
        this.SPARQL_Q_GET_ADAMNET_DATA_WORKS_BY_SAME_CREATOR = 'PREFIX dc: <http://purl.org/dc/elements/1.1/>	PREFIX sem: <http://semanticweb.cs.vu.nl/2009/11/sem/>	PREFIX skos: <http://www.w3.org/2004/02/skos/core#>	PREFIX foaf: <http://xmlns.com/foaf/0.1/>	PREFIX owl: <http://www.w3.org/2002/07/owl#>	SELECT ?work ?workImage ?workLabel ?workYear WHERE {	    ?item skos:prefLabel ?itemLabel;	        owl:sameAs ?sameas.	    FILTER regex(?sameas, "^https://www.vondel.humanities.uva.nl/ecartico/persons/_VAR1_$", "i").	    ?work foaf:depiction ?workImage;	        dc:title ?workLabel;	        dc:description ?workDescription;	        dc:creator ?item;	        sem:hasBeginTimeStamp ?startTimeStamp.	    BIND(YEAR(?startTimeStamp) AS ?workYear).	 } LIMIT 50';
        //query to any artwork of the same type as from the specified (ecartico) artist
        this.SPARQL_Q_GET_ADAMNET_DATA_WORKS_OF_SAME_TYPE = 'PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> PREFIX dc: <http://purl.org/dc/elements/1.1/> PREFIX sem: <http://semanticweb.cs.vu.nl/2009/11/sem/> PREFIX skos: <http://www.w3.org/2004/02/skos/core#> PREFIX foaf: <http://xmlns.com/foaf/0.1/> PREFIX owl: <http://www.w3.org/2002/07/owl#> SELECT DISTINCT ?item ?itemLabel ?work ?workImage ?workLabel ?workTitle ?workCreator ?workDescription ?workType WHERE { ?item skos:prefLabel ?itemLabel; owl:sameAs ?sameAs. FILTER(REGEX(?sameAs, "^https://www.vondel.humanities.uva.nl/ecartico/persons/_VAR1_$", "i")). ?workBySameCreator dc:creator ?item; dc:type ?workType. FILTER ( ?workType = "beeldhouwwerk" || ?workType = "plattegrond" ). ?work dc:type ?workType; sem:hasBeginTimeStamp ?startTimeStamp. FILTER ( ?startTimeStamp < "1800-01-01"^^xsd:date). FILTER ( ?startTimeStamp > "1600-01-01"^^xsd:date). ?work foaf:depiction ?workImage ; dc:title ?workLabel ; dc:creator ?workCreator ; dc:description ?workDescription. } LIMIT 50';
        this.SPARQL_Q_GET_ADAMNET_DATA_WORKS_OF_SAME_TYPE_V2 = 'PREFIX dct: <http://purl.org/dc/terms/> PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> PREFIX dc: <http://purl.org/dc/elements/1.1/> PREFIX sem: <http://semanticweb.cs.vu.nl/2009/11/sem/>  PREFIX skos: <http://www.w3.org/2004/02/skos/core#> PREFIX skosxl: <http://www.w3.org/2008/05/skos-xl#> PREFIX foaf: <http://xmlns.com/foaf/0.1/>  PREFIX owl: <http://www.w3.org/2002/07/owl#> PREFIX gvp: <http://vocab.getty.edu/ontology#> SELECT DISTINCT ?item ?itemLabel ?work ?workType ?workImage ?workLabel ?workCreator ?workDescription ?workTypeLabel WHERE { SERVICE <http://vocab.getty.edu/sparql> { FILTER(?selectedType = <_VAR1_>). ?selectedType skos:prefLabel ?selectedTypeLabel. FILTER(LANGMATCHES(LANG(?selectedTypeLabel), "nl")). ?selectedType gvp:broaderPreferred ?broaderType1. ?broaderType1 gvp:broaderPreferred ?broaderType2. ?broaderType2 gvp:broaderPreferred ?broaderType3.}?work dc:type ?workType.FILTER(?workType = ?selectedType || ?workType = ?broaderType1 || ?workType = ?broaderType2 || ?workType = ?broaderType3).?work sem:hasBeginTimeStamp ?startTimeStamp;foaf:depiction ?workImage ; dc:title ?workLabel ; dc:creator ?workCreator ; dc:description ?workDescription. FILTER ( ?startTimeStamp < "1800-01-01"^^xsd:date). FILTER ( ?startTimeStamp > "1600-01-01"^^xsd:date). ?workType skosxl:prefLabel ?workTypeGetty. ?workTypeGetty gvp:term ?workTypeLabel.} LIMIT 50';
        //MULTIVAR at location _MULTIVAR_POS_
        this.SPARQL_Q_GET_ADAMNET_DATA_WORKS_OF_SAME_TYPE_V3 = 'PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 	PREFIX dc: <http://purl.org/dc/elements/1.1/> 	PREFIX sem: <http://semanticweb.cs.vu.nl/2009/11/sem/>  	PREFIX foaf: <http://xmlns.com/foaf/0.1/>	SELECT DISTINCT ?work ?workImage ?workLabel ?workDescription ?workType ?workYear WHERE {	    VALUES ?itemTypeFilter { _MULTIVAR_POS_ }.	    ?work dc:type ?workType;	        sem:hasBeginTimeStamp ?startTimeStamp.	    FILTER ( ?workType = ?itemTypeFilter ).	    FILTER ( ?startTimeStamp > "1600-01-01"^^xsd:date && ?startTimeStamp < "1800-01-01"^^xsd:date ).	    ?work foaf:depiction ?workImage;	        dc:title ?workLabel;	        dc:creator ?workCreator;	        dc:description ?workDescription.	    BIND(YEAR(?startTimeStamp) AS ?workYear)	 } LIMIT 30';
        this.SPARQL_Q_GET_ADAMNET_DATA_WORKS_OF_SAME_TYPE_MULTIVAR = '_MULTIVAR_';
    }

    /**
     * @desc Load SPARQL data from adamnet b/o ecarticoID
     * @param ecarticoID {str}
     * @returns obj with results (todo: merge)
    */
    async getAdamnetData(ecarticoID) {
        return this.getWorksObjectBasedOnSingleVariableQuery(ecarticoID, this.SPARQL_Q_GET_ADAMNET_DATA_WORKS_BY_SAME_CREATOR, this.endpointUrl, this.dataSourceLabel, this.dataSourceURL);
    }

    /**
     * @desc Load SPARQL data from adamnet b/o ecarticoID
     * @param ecarticoID {str}
     * @returns obj with results (todo: merge)
    */
    async getAdamnetDataRelatedWorks(ecarticoID) {
        return this.getWorksObjectBasedOnSingleVariableQuery(ecarticoID, this.SPARQL_Q_GET_ADAMNET_DATA_WORKS_OF_SAME_TYPE, this.endpointUrl, this.dataSourceLabel, this.dataSourceURL);
    }

    /**
     * @desc Load SPARQL data from adamnet b/o ecarticoID
     * @param ecarticoID {str}
     * @returns obj with results (todo: merge)
    */
    async getAdamnetDataRelatedWorksAAT(aatURI) {
        return this.getWorksObjectBasedOnSingleVariableQuery(aatURI, this.SPARQL_Q_GET_ADAMNET_DATA_WORKS_OF_SAME_TYPE_V2, this.endpointUrl, this.dataSourceLabel, this.dataSourceURL);
    }

    /**
     * @desc Load SPARQL data from adamnet b/o adamnet categories
     * @param ecarticoID {str}
     * @returns obj with results (todo: merge)
    */
    async getAdamnetDataRelatedWorksAdamnetCategories(adamnetCategories) {
        if (adamnetCategories != '') {
            //adamnet keywords are , separated
            var adamnetKeywordArr = adamnetCategories.split(',');
            for (var i = 0; i < adamnetKeywordArr.length; i++) {
                if (adamnetKeywordArr[i].includes("vocab.getty.edu")) {
                    //change string to <URI> for sparql query
                    adamnetKeywordArr[i] = '<' + adamnetKeywordArr[i] + '>';
                } else {
                    //regular string, so should be encapsulated in quotes for sparql query
                    adamnetKeywordArr[i] = '"' + adamnetKeywordArr[i] + '"';
                }
            }
            //this query retrieves all images with a certain ecartico ID from the Adamnet-heritage set
            return this.getWorksObjectBasedOnMultiVariableQuery(adamnetKeywordArr, this.SPARQL_Q_GET_ADAMNET_DATA_WORKS_OF_SAME_TYPE_V3, this.SPARQL_Q_GET_ADAMNET_DATA_WORKS_OF_SAME_TYPE_MULTIVAR, this.endpointUrl, this.dataSourceLabel, this.dataSourceURL);
        }
        //return empty object
        return this.createWorksObject();
    }
}

export default SPARQLDataAdamnet;
