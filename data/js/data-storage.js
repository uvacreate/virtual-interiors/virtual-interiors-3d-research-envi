import DataStorageObject from './data-storage-object.js';

/**
 * @classdesc DataStorage stores data for application in a (pseudo) triple format. Can be replaced with e.g. connection to database or triple store. Currently only used for annotations.
 */
class DataStorage {
    constructor() {
        //var in this class to store all LOD objects in. Should only be accessed via getter.
        this._dataStorageObjectArray = [];
    }

    /**
    * @desc Filter triples by subject and relation
    */
    filterTriplesBySubjectRelation(subject, relationLabel) {
        var tripleArrForObj = this.filterByProperty(this.dataStorageObjectArray, "subject", subject, "relationLabel", relationLabel);
        if(tripleArrForObj.length>0) {
            //for now, only return the last item (each item is only stored once). TBD later.
            return tripleArrForObj[tripleArrForObj.length-1].object;
        } else {
            return null;
        }
    }

    /**
    * @desc Filter triples by subject only
    */
    filterTriplesBySubject(subject) {
        var tripleArrForObj = this.filterByProperty(this.dataStorageObjectArray, "subject", subject);
        if(tripleArrForObj.length>0) {
            return tripleArrForObj;
        } else {
            return null;
        }
    }

    /**
    * @desc Save a triple
    */
    saveTriple(subject, relationLabel, object, isBidirectional) {
        if(!this.tripleExists(subject, relationLabel, object)) {
            this.dataStorageObjectArray.push( this.createDataStorageObject(subject, relationLabel, object, isBidirectional) );
        }
    }

    /**
    * @desc Save triple as unique triple, i.e. only allow one triple for a subject + object combination
    * @param event {obj}
    */
    saveUniqueTriple(subject, relationLabel, object, isBidirectional) {
        var tripleIndex = this.getIndexOfTriple(subject, relationLabel);
        if(tripleIndex == -1) {
            this.dataStorageObjectArray.push( this.createDataStorageObject(subject, relationLabel, object, isBidirectional) );
        } else {
            this.removeTripleAtIndex(tripleIndex);
            this.dataStorageObjectArray.push( this.createDataStorageObject(subject, relationLabel, object, isBidirectional) );
        }
    }

    /**
    * @desc Delete a triple from the storage, based on subject, relationLabel, object
    */
    removeTriple(subject, relationLabel, object) {
        var tripleIndex = this.getIndexOfTriple(subject, relationLabel, object);
        if(tripleIndex != -1) {
            this.dataStorageObjectArray.splice(tripleIndex, 1);
        }
    }

   /**
    * @desc Delete a triple from the storage, only based on subject, relationLabel
    */    
    removeTripleBySubjectAndRelation(subject, relationLabel) {
        var tripleIndex = this.getIndexOfTriple(subject, relationLabel);
        if(tripleIndex != -1) {
            this.dataStorageObjectArray.splice(tripleIndex, 1);
        }
    }

    /**
    * @desc Delete a triple at a certain index number
    */
    removeTripleAtIndex(index) {
        this.dataStorageObjectArray.splice(index, 1);
    }

    /**
    * @desc Check if a triple exists in the storage
    */
    tripleExists(subject, relationLabel, object) {
        if(this.getIndexOfTriple(subject, relationLabel, object) == -1) {
            return false;
        } else {
            return true;
        }
    }

    /**
    * @desc Get the index of a triple in the datastorage array. object param is optional
    */
    getIndexOfTriple(subject, relationLabel, object) {
        for(var i=0; i<this.dataStorageObjectArray.length; i++) {
            if(this.dataStorageObjectArray[i].relationLabel == relationLabel) {
                if(object != null) {
                    if((this.dataStorageObjectArray[i].subject == subject && this.dataStorageObjectArray[i].object == object) ) {
                        return i;
                    }
                } else {
                    if((this.dataStorageObjectArray[i].subject == subject) ) {
                        return i;
                    }
                }
            }
        }
        return -1;
    }

    /**
    * @desc Create a new data storage object
    */
    createDataStorageObject(subject, relationLabel, object, isBidirectional) {
        return new DataStorageObject(subject, relationLabel, object, isBidirectional);
    }

    /**
    * @desc Get an object array based on a certain subject
    */
    getDataStorageObjectArrayForSubject(subject) {
        return this.filterByProperty(this.dataStorageObjectArray, "subject", subject);
    }

    /**
    * @desc Get full datastorage object array
    */
    get dataStorageObjectArray() {
        return this._dataStorageObjectArray;
    }

    /**
    * @desc Set full datastorage object array
    */
    set dataStorageObjectArray(val) {
        this._dataStorageObjectArray = val;
    }

    /**
    * @desc (helper) Get a multidimensional array filtered by a certain property
    */
    filterByProperty(array, prop, value, prop2, value2){
        var filtered = [];
        for(var i = 0; i < array.length; i++){ 
            var obj = array[i];

            if(obj[prop]==value) {
                //if second property has been given, also match that one
                if(prop2!=null && value2!=null) {
                    if(obj[prop2] == value2) {
                        filtered.push(obj)
                    }
                } else {
                    filtered.push(obj)
                }
            }
        }    
        return filtered;
    }
}

export default DataStorage;