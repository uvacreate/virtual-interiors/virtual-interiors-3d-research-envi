/**
 * @classdesc LinkedDataWorkObject creates objects to store a "work" (e.g. artwork) and its basic properties in
 */
 class LinkedDataWorkObject {
    constructor(id, title, image, year, creator, creatorLabel) {
        //work URI
        this.id = id;
        //work title
        this.title = title;
        //work image src
        this.image = image;
        //work creation year
        this.year = year;
        //work creator URI
        this.creator = creator;
        //work creator
        this.creatorLabel = creatorLabel;

        return {
            id: id,
            title: title,
            image : image,
            year : year,
            creator: creator,
            creatorLabel: creatorLabel
        } 
    }

}

export default LinkedDataWorkObject;