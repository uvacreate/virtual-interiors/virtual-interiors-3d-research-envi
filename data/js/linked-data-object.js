/**
 * @classdesc LinkedDataObject creates objects to store Linked Data (results from SPARQL queries) in
 */
 class LinkedDataObject {
    constructor(id, name, originalQuery, originalEndpoint, dataSourceLabel, dataSourceURL) {
        //URI of resource
        this.id = id;
        //name
        this.name = name;
        //orginal issued SPARQL query
        this.originalQuery = originalQuery;
        //URL to original public SPARQL endpoint
        this.originalEndpoint = originalEndpoint;
        //label for this data source
        this.dataSourceLabel = dataSourceLabel;
        //URL to refer to this data source (e.g. information page)
        this.dataSourceURL = dataSourceURL;
    }
}

export default LinkedDataObject;