/**
* @desc Dispatch a SPARQL query
* @todo No timeout included
*/
class SPARQLQueryDispatcher {
	constructor(endpoint) {
		this.endpoint = endpoint;
	}

	/**
	* @desc Dispatch a SPARQL query
	*/
	query(sparqlQuery) {
		const fullUrl = this.endpoint + '?query=' + encodeURIComponent(sparqlQuery);
		const headers = { 'Accept': 'application/sparql-results+json' };
		console.log('SPARQL query: ' + fullUrl);
		return fetch(fullUrl, { mode: 'cors', method: 'POST', headers }).then(body => body.json()).catch((error) => {
			console.error('Error:', error);
		});
	}
}

export default SPARQLQueryDispatcher;