//import jquery to more easily bind actions to e.g. buttons
var $ = require('jquery');

/**
 * @classdesc GUI2D class manages the custom 2D UI (created via Bootstrap), i.e. button overlays on top of 3D viewer, and sidebar. (note: use .bind(this) on anonymous functions passed as parameters)
 */
class GUI2D {
    constructor(settings, camera, resizeAppFunction) {
        this.settings = settings;
        //default content settings:
        //use resizeApp() function as parameter (in main.js) allowing to adjust dimensions of 3D scene upon resizing
        this.resizeApp = resizeAppFunction;
        //use getCameraSetting() function as parameter (in main.js) allowing to know current camera setting
        this.BOOTSTRAP_BREAKPOINTS = ['sm', 'md', 'ld', 'xl', 'xxl'];
        this.camera = camera;
    }

    /**
     * @desc Get property indicating if sidebar is visible
     * @return sideBarIsVisible {bool}
    */
    sideBarIsVisible() {
        if (window.getComputedStyle(columnSidebar).display == 'block') {
            return true;
        } else if (window.getComputedStyle(columnSidebar).display == 'none') {
            return false;
        } else {
            console.log('[err] Could not determine sidebar status');
        }
    }

    /**
     * @desc Show the sidebar in the interface and populate default contents
    */
    showSideBar() {
        if (this.sideBarCanBeShown()) {
            //show UI sidebar
            columnSidebar.style.display = 'block';
            //add breakpoint and up --> for visibility column
            if (!columnSidebar.classList.contains('d-md-block')) {
                columnSidebar.classList.add('d-md-block');
            }
            //make sure app proportions are still correct
            this.resizeApp();
        }
    }

    /**
     * @desc Hide a UI element (via CSS) with an ID in the DOM
    */
    hideUIElement(id) {
        id.style.display = 'none';
    }

    /**
     * @desc Show a UI element (via CSS) with an ID in the DOM
    */
    showUIElement(id) {
        id.style.display = 'block';
    }

    /**
     * @desc Rename a button UI element (can be any ID in the DOM with innerHTML)
    */
    renameButton(id, value) {
        id.innerHTML = value;
    }

    /**
     * @desc Initialize contents for the sidebar (invoked if sidebar is visible, not invoked if small window size). Currently there is a maximum of 6 carousel items (if less > hide).
     * @todo Function could be rewritten and made more efficient
    */
    initializeSideBarContents() {
        //default: show it
        imgCarousel.style.display = 'block';
        //Set general title
        textGeneralInfoTitle.innerHTML = this.settings.DEFAULT_TAB_GENERAL_TITLE;
        //Set general description
        textGeneralInfoDescription.innerHTML = this.settings.DEFAULT_TAB_GENERAL_DESCRIPTION;
        //Populate image carousel (bottom of sidebar)
        if (this.settings.DEFAULT_TAB_GENERAL_IMAGE) {
            imageGeneralInfo1.src = this.settings.DEFAULT_TAB_GENERAL_IMAGE;
            imageGeneralInfoCaption1.innerHTML = this.settings.DEFAULT_TAB_GENERAL_IMAGE_CAPTION;
        } else {
            //no contents - hide carousel
            imgCarousel.style.display = 'none';
        }
        if (this.settings.DEFAULT_TAB_GENERAL_IMAGE_2) {
            imageGeneralInfo2.src = this.settings.DEFAULT_TAB_GENERAL_IMAGE_2;
            imageGeneralInfoCaption2.innerHTML = this.settings.DEFAULT_TAB_GENERAL_IMAGE_2_CAPTION;
        } else {
            //delete carousel item
            if(document.getElementById('carouselItem2') != null) {
                carouselItem2.outerHTML = '';
            }
        }
        if (this.settings.DEFAULT_TAB_GENERAL_IMAGE_3) {
            imageGeneralInfo3.src = this.settings.DEFAULT_TAB_GENERAL_IMAGE_3;
            imageGeneralInfoCaption3.innerHTML = this.settings.DEFAULT_TAB_GENERAL_IMAGE_3_CAPTION;
        } else {
            //delete carousel item
            if(document.getElementById('carouselItem3') != null) {
                carouselItem3.outerHTML = '';
            }
        }
        if (this.settings.DEFAULT_TAB_GENERAL_IMAGE_4) {
            imageGeneralInfo4.src = this.settings.DEFAULT_TAB_GENERAL_IMAGE_4;
            imageGeneralInfoCaption4.innerHTML = this.settings.DEFAULT_TAB_GENERAL_IMAGE_4_CAPTION;
        } else {
            //delete carousel item
            if(document.getElementById('carouselItem4') != null) {
                carouselItem4.outerHTML = '';
            }
        }
        if (this.settings.DEFAULT_TAB_GENERAL_IMAGE_5) {
            imageGeneralInfo5.src = this.settings.DEFAULT_TAB_GENERAL_IMAGE_5;
            imageGeneralInfoCaption5.innerHTML = this.settings.DEFAULT_TAB_GENERAL_IMAGE_5_CAPTION;
        } else {
            //delete carousel item
            if(document.getElementById('carouselItem5') != null) {
                carouselItem5.outerHTML = '';
            }
        }
        if (this.settings.DEFAULT_TAB_GENERAL_IMAGE_6) {
            imageGeneralInfo6.src = this.settings.DEFAULT_TAB_GENERAL_IMAGE_6;
            imageGeneralInfoCaption6.innerHTML = this.settings.DEFAULT_TAB_GENERAL_IMAGE_6_CAPTION;
        } else {
            //delete carousel item
            if(document.getElementById('carouselItem6') != null) {
                carouselItem6.outerHTML = '';
            }
        }
        //Populate further information tab (e.g., related literature)
        if (this.settings.DEFAULT_TAB_FURTHER_INFORMATION) {
            textGeneralInfoFurtherInformation.innerHTML = this.settings.DEFAULT_TAB_FURTHER_INFORMATION;
        }
    }

    /**
     * @desc Hide the sidebar in the interface
    */
    hideSideBar() {
        //hide UI sidebar (e.g. for VR iPhone use..)
        columnSidebar.style.display = 'none';
        //add breakpoint and up --> for visibility column
        if (columnSidebar.classList.contains('d-md-block')) {
            columnSidebar.classList.remove('d-md-block');
        }
        //make sure app proportions are still correct 
        this.resizeApp();
    }

    /**
     * @desc Toggle between visible and invisible sidebar
    */
    toggleSideBar() {
        if (this.sideBarIsVisible()) {
            this.hideSideBar();
        } else if (!this.sideBarIsVisible() && this.sideBarCanBeShown()) {
            this.showSideBar();
        }
    }

    /**
     * @desc Determine whether the sidebar can be shown, dep on whether already visible, screen size, camera setting (disable for VR)
    */
    sideBarCanBeShown() {
        if (!this.viewPortIsSmall() && this.camera.currentSetting != 'vr-first-person' && this.camera.currentSetting != 'cardboard-vr-first-person') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @desc Check if the (Bootstrap) viewport is small {Bool}
    */
    viewPortIsSmall() {
        if (this.getViewport() == 'xs' || this.getViewport() == 'sm') {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @desc Open object list tab (search/filter panel)
    */
    openObjectListTab() {
        //select and deselect tabs
        this.selectTab(tabObjectList, tabObjectListContent);
        this.deselectTab(tabObject, tabObjectContent);
    }

    /**
     * @desc Open object tab (search/filter panel)
    */
    openObjectTab() {
        this.deselectTab(tabHome, tabHomeContent);
        this.deselectTab(tabObjectList, tabObjectListContent);
        this.selectTab(tabObject, tabObjectContent);
        this.displayTab(tabObject);
        //activate the default subtab (details) upon (re)opening object tab
        this.setDefaultTab('#tabObjectContent');
        //set default collapse element (details subtab)
        this.setDefaultCollapse('#collapseOne');
        //set default collapse element (linked data subtab)
        this.setDefaultCollapse('#collapseLODOne');
    }

    /**
     * @desc Do not display a bootstrap tab in the interface
     * @param tabId {str}
    */
    hideTab(tabId) {
        tabId.style.display = 'none';
    }

    /**
     * @desc Display a bootstrap tab in the interface
     * @param tabId {str}
    */
    displayTab(tabId) {
        tabId.style.display = 'block';
    }

    /**
     * @desc Deselect a bootstrap tab in the interface
     * @param tabId {str}
    */
    deselectTab(tabId, tabContentId) {
        tabId.classList.remove('active');
        tabContentId.classList.remove('show')
        tabContentId.classList.remove('active');
    }

    /**
     * @desc Select a bootstrap tab in the interface
     * @param tabId {str}
    */
    selectTab(tabId, tabContentId) {
        tabId.classList.add('active');
        tabContentId.classList.add('show')
        tabContentId.classList.add('active');
    }

    /**
     * @desc Select a default tab in the interface (currently: first tab)
     * @param tabId {str}
    */
    setDefaultTab(domObj) {
        //bootstrap behavior to show first tab
        $(domObj).find('.nav a:first').tab('show');
    }

    /**
     * @desc Select a default collapsable panel in the interface (currently: first panel])
     * @param tabId {str}
    */
    setDefaultCollapse(domObj) {
        $(domObj).collapse('show');
    }

    /**
     * @desc Show guided tour information
     * @param txt {str} Text to show
     * @param showReadMoreButton {Bool} show read more button in infopanel
     * @param guidedTourPosCnt {int} Current position in tour (starts from 0)
     * @param guidedTourLength {int} Total length of guided tour
    */
    showGuidedTourInfoAlert(txt, showReadMoreButton, guidedTourPosCnt, guidedTourLength) {
        //always hide other infobox
        this.hideInfoBoxAlert();
        alertGuidedTourInfoBoxTitle.innerHTML = 'Guided Tour ' + (guidedTourPosCnt + 1) + '/' + (guidedTourLength);
        //optionally show text for guided tour position
        if (txt != '') {
            alertGuidedTourInfoBoxDescription.innerHTML = txt;
        }
        //show button controls for guided tour
        spanGuidedTourControls.style.display = 'block';
        alertGuidedTourInfoBox.style.display = 'block';
        //optionally (if an object has been selected) show read more button
        if (showReadMoreButton) {
            //display in same row as prev/next buttons
            buttonGuidedTourMoreInfo.style.display = 'inline'
        } else {
            buttonGuidedTourMoreInfo.style.display = 'none'
        }
    }

    /**
     * @desc Hide guided tour infobox
    */
    hideGuidedTourInfoAlert() {
        alertGuidedTourInfoBox.style.display = 'none';
        //[fixme check if it can be shown]
        this.showSideBar();
    }

    /**
     * @desc Hide button to remove (textual) filters
    */
    hideFilterAlert() {
        alertTextFilterInput.style.display = 'none';
    }

    /**
     * @desc Show button to remove (textual) filters
    */
    showFilterAlert() {
        alertTextFilterInput.style.display = 'block';
    }

    /**
     * @desc Hide reset view alert button
    */
    hideResetViewAlert() {
        alertResetView.style.display = 'none';
    }

    /**
     * @desc Show reset view alert button (after changing settings in the view or in object display)
    */
    showResetViewAlert() {
        alertResetView.style.display = 'block';
    }

    /**
     * @desc Hide analysis layer legend info
    */
    hideInfoBoxAlert() {
        alertInfoBox.style.display = 'none';
    }

    /**
     * @desc Show analysis layer legend info
     * @param type {Str} 'introText' | 'object' | 'location' | 'customizeMesh'. Otherwise: hide infobox
    */
    showInfoBoxAlert(type, showCloseButton) {
        //always hide other infobox first
        this.hideGuidedTourInfoAlert();
        //make this infobox visible
        alertInfoBox.style.display = 'block';
        alertInfoBoxClose.style.display = 'none';
        //optionally show close button
        if (showCloseButton) {
            alertInfoBoxClose.style.display = 'block';
        }
        //depending on type, show different kinds of information
        if (type == 'introText') {
            alertInfoBoxTitle.innerHTML = 'Info: 3D viewer controls <i class="far fa-question-circle"></i>';
            alertInfoBoxDescription.innerHTML = '<i class="fas fa-mouse-pointer"></i>&nbsp;&nbsp;Use <b>mouse</b> to select objects<br><i class="far fa-hand-pointer"></i> <i class="fas fa-arrows-alt-h"></i>&nbsp;&nbsp;<b>Click and drag mouse</b> to rotate the view<br><i class="fas fa-arrow-circle-left"></i><i class="fas fa-arrow-circle-up"></i><i class="fas fa-arrow-circle-right"></i>&nbsp;&nbsp;1st-person camera: <b>arrow keys</b> to move around<br><i class="fas fa-arrow-circle-left"></i><i class="fas fa-arrow-circle-up"></i><i class="fas fa-arrow-circle-right"></i>&nbsp;&nbsp;Orbit camera: <b>arrow keys</b> to rotate view';
            //analysis layer: object certainty
        } else if (type == 'object') {
            alertInfoBoxTitle.innerHTML = 'Confidence index: objects';
            alertInfoBoxDescription.innerHTML = '<span class="uncertaintyOne">&#9673</span> 1. modeled after original object<br><span class="uncertaintyTwo">&#9673</span> 2. primary source + analogy with high certainty degree<br><span class="uncertaintyThree">&#9673</span> 3. primary source + analogy with doubt<br><span class="uncertaintyFour">&#9673</span> 4. uncertain';
            //analysis layer: location certainty
        } else if (type == 'location') {
            alertInfoBoxTitle.innerHTML = 'Confidence index: object locations';
            alertInfoBoxDescription.innerHTML = '<span class="uncertaintyOne">&#9673</span> 1. known/in original location<br><span class="uncertaintyTwo">&#9673</span> 2. inferred with certainty<br><span class="uncertaintyThree">&#9673</span> 3. inferred with doubt<br><span class="uncertaintyFour">&#9673</span> 4. uncertain';
            //customize mesh
        } else if (type == 'customizeMesh') {
            alertInfoBoxTitle.innerHTML = 'Object customization';
            alertInfoBoxDescription.innerHTML = '<span class="far fa-square alertInfoBoxIcon"></span> Press <b>q</b> for bounding box<br><span class="fas fa-arrows-alt alertInfoBoxIcon"></span> Press <b>w</b> to move object<br><span class="fas fa-sync-alt alertInfoBoxIcon"></span> Press <b>e</b> to rotate object<br><span class="fas fa-arrows-alt-v alertInfoBoxIcon"></span> Press <b>r</b> to scale object<br><span class="bg-color-five"><span class="fas fa-exclamation-triangle alertInfoBoxIcon"></span> Experimental: reload app to undo changes</span>'
        } else {
            //hide infobox
            alertInfoBox.style.display = 'none';
            console.log('[log] analysis layer type unknown');
        }
    }

    /**
     * @desc Show bootstrap alert panel with annotation
     * @todo revise function to support multiple annotations?
    */
    showAnnotationAlert() {
        alertAnnotationField.style.display = 'block';
    }

    /**
     * @desc Hide bootstrap alert panel with annotation
     * @todo revise function to support multiple annotations?
    */
    hideAnnotationAlert() {
        alertAnnotationField.style.display = 'none';
    }

    /**
     * @desc Get the currently entered search filter
     * @returns searchFilter {str}
    */
    getCurrentSearchFilter() {
        return inputFieldFilterInput.value;
    }

    /**
     * @desc Reset the currently entered search filter
    */
    setCurrentSearchFilter(filter) {
        inputFieldFilterInput.value = filter;
    }

    /**
     * @desc Reset the currently entered search filter
    */
    resetTextualSearchFilter() {
        //reset filter
        if (this.getCurrentSearchFilter != '') {
            this.setCurrentSearchFilter('');
            this.hideFilterAlert();
        }
    }

    /**
     * @desc Apply a "selected" style to a button
    */
    setButtonSelectionStyles(selectedButtonID, allButtonsIDArr) {
        //deselect all buttons
        this.setDeselectedStyleToButtonArray(allButtonsIDArr);
        //select specified button
        this.setSelectedStyleToButton(selectedButtonID);
    }

    /**
    * @desc Add "selected" style to a button
    */
    setSelectedStyleToButton(selectedButtonID) {
        if (!selectedButtonID.classList.contains('btnIsSelected')) {
            selectedButtonID.classList.add('btnIsSelected');
        }
    }

    /**
     * @desc Remove a "selected" style to a button array
    */
    setDeselectedStyleToButtonArray(allButtonsIDArr) {
        for (var i = 0; i < allButtonsIDArr.length; i++) {
            this.setDeselectedStyleToButton(allButtonsIDArr[i]);
        }
    }

    /**
     * @desc Remove "selected" style from a button
    */
    setDeselectedStyleToButton(deselectedButtonID) {
        if (deselectedButtonID.classList.contains('btnIsSelected')) {
            deselectedButtonID.classList.remove('btnIsSelected');
        }
    }

    /**
     * @desc Show label indicating current framerate
    */
    showFPSLabel() {
        spanFPSLabel.style.display = "block";
    }

    /**
     * @desc Hide label indicating current framerate
    */
    hideFPSLabel() {
        spanFPSLabel.style.display = "none";
    }

    /**
     * @desc Set current framerate value
    */
    setFPSLabel(val) {
        spanFPSLabel.innerHTML = val;
    }

    /**
     * @desc Reset current framerate value
    */
    resetFPSLabel() {
        this.setFPSLabel('');
    }

    /**
     * @desc Show works for linked data (including provenance text)
    */
    showLinkedDataWorks(domElt, dataObj, isWikidataSourceBool) {
        //use += since multiple LOD sources might be added to dom element
        //1 show provenance
        domElt.innerHTML += this.getLinkedDataProvenanceHTMLText(dataObj);
        //2 show actual results
        domElt.innerHTML += this.createImageThumbnailGalleryHTMLText(dataObj, isWikidataSourceBool);
        domElt.innerHTML += '<br>';
    }

    /**
     * @desc Create thumbnail gallery HTML text for Linked Data
    */
    createImageThumbnailGalleryHTMLText(dataObj, isWikidataImage) {
        var str = '';
        //add suffix to prevent retrieving huge wikidata images
        var imgSuffix = '';
        if (isWikidataImage) {
            imgSuffix = '?width=500px';
        }
        if (dataObj) {
            const divInFour = Math.round(dataObj.works.length / 4);
            var numWorks = dataObj.works.length;
            //limit max listed number of works
            if (numWorks > 21) { numWorks = 21; }
            for (var i = 0; i < numWorks; i++) {
                if (i == 0) {
                    str += '<section id="photos">';
                }
                str += '<img data-target="#imageOverlay" data-url="' + dataObj.works[i].id + '" data-title="' + dataObj.works[i].title + '" tooltip="' + dataObj.works[i].title + '" title="' + dataObj.works[i].title + ' (' + dataObj.works[i].year + ')' + '" data-creator="artist name" data-id="' + dataObj.id + '" data-placement="top" data-toggle="modal" data-tooltip="tooltip" class="clickableImg imgListItem"  src="' + dataObj.works[i].image + imgSuffix + '" data-url="' + dataObj.id + '">';
                if (i == dataObj.works.length - 1) {
                    str += '</div>';
                }
            }
        }
        return str;
    }

    /**
     * @desc Create provenance HTML text for Linked Data
    */
    getLinkedDataProvenanceHTMLText(dataObj) {
        var htmlTxt = '<i class="fas fa-link"></i> <a href="' + dataObj.dataSourceURL + '" target="_blank">' + dataObj.dataSourceLabel + '</a>';
        //2 show SPARQL query
        htmlTxt += ' (' + this.getYasguiUrlString(dataObj) + ')';
        return htmlTxt;
    }

    /**
     * @desc Get URL htmlText for linking directly to YasGUI, including current query
    */
    getYasguiUrl(dataObj) {
        //replace \t in original queries by newlines; encode as URI string
        var str = '';
        if (dataObj.originalQuery != null) {
            var queryWithNewLines = encodeURIComponent(dataObj.originalQuery.replace(/\t/g, '\n'));
            str = 'https://yasgui.triply.cc/index.html#query=' + queryWithNewLines + '&endpoint=' + dataObj.originalEndpoint;
        }
        return str;
    }

    /**
     * @desc Create htmlText for URL to Yasgui
    */
    getYasguiUrlString(dataObj) {
        return '<a href="' + this.getYasguiUrl(dataObj) + '" target="_blank">SPARQL</a>';
    }

    /**
     * @desc Show information about creator of work based on Ecartico
    */
    showEcarticoCreatorInformation(ecarticoObj) {
        //add creator information to "Original object details" panel
        textCreatorInformation.innerHTML = '<b>Creator:</b><br>';
        textCreatorInformation.innerHTML += 'Name: ' + ecarticoObj.name + '<br>';
        textCreatorInformation.innerHTML += 'Birthdate: ' + ecarticoObj.birthDate + '<br>';
        textCreatorInformation.innerHTML += 'Date of death: ' + ecarticoObj.deathDate + '<br>';
        textCreatorInformation.innerHTML += 'Read more: <i class="fas fa-link"></i> <a href="' + ecarticoObj.id + '" target="_blank">Ecartico</a> (' + this.getYasguiUrlString(ecarticoObj) + ')';
    }

    /**
     * @desc Show information about original objects based on Rijksmuseum data (from Golden Agents)
    */
    showRijksmuseumOriginalObjectInformation(rijksmuseumObj) {
        //use first item of "works" array
        if (rijksmuseumObj.works.length > 0) {
            textObjectInformation.innerHTML += '<b>Object:</b><br>';
            textObjectInformation.innerHTML += 'Title: ' + rijksmuseumObj.works[0].title + '<br>';
            textObjectInformation.innerHTML += 'Creation year: ' + rijksmuseumObj.works[0].year + '<br>';
            textObjectInformation.innerHTML += 'Read more: <i class="fas fa-link"></i> <a href="' + rijksmuseumObj.works[0].id + '" target="_blank">Rijksmuseum</a> (' + this.getYasguiUrlString(rijksmuseumObj) + ')';
        }
    }

    /**
     * @desc Show information about external relations (from Ecartico)
    */
    showEcarticoExternalRelationsInformation(ecarticoObj) {
        if (ecarticoObj.numberOfResults > 0) {
            var numResults = ecarticoObj.externalRelations.length + 1;
            //increase number by one since we also list Ecartico itself
            textExternalVocabularies.innerHTML = numResults + ' sources about creator from ' + this.getLinkedDataProvenanceHTMLText(ecarticoObj) + ':<br>';
            textExternalVocabularies.innerHTML += '<i class="fas fa-link"></i> <a data-tooltip="tooltip" title="' + ecarticoObj.id + '" href="' + ecarticoObj.id + '" target="_blank">vondel.humanities.uva.nl/ecartico</a><br>';
            for (var i = 0; i < ecarticoObj.externalRelations.length; i++) {
                textExternalVocabularies.innerHTML += '<i class="fas fa-link"></i> <a data-tooltip="tooltip" title="' + ecarticoObj.externalRelations[i].url + '" href="' + ecarticoObj.externalRelations[i].url + '" target="_blank">' + ecarticoObj.externalRelations[i].hostname + '</a><br>';
            }
            //set badge with #results ; add 1 since we manually added Ecartico to the list
            badgeHeadingLODOne.innerHTML = ecarticoObj.numberOfResults + 1;
        } else {
            //no Ecartico info found
            textExternalVocabularies.innerHTML = '';//textExternalVocabularies.innerHTML = '0 sources about creator from ' + this.getLinkedDataProvenanceHTMLText(ecarticoObj) + '<br>';
            //set badge with #results
            badgeHeadingLODOne.innerHTML = 0;
        }
    }

    /**
     * @desc Reset all dynamic UI elements to default values
    */
    resetDynamicUIElements() {
        //reset all stuff
        elementInfoCard.style.display = 'none';
        text3DModelInformation.innerHTML = textObjectInformation.innerHTML = textInfoCardTitle.innerHTML = textInfoCardDescription.innerHTML = textInfoCardCategory.innerHTML = textInfoCardCategory.innerHTML = textInfoCardAlternateHypothesis.innerHTML = imageInfoCardImage.src = imageInfoCardImageCaption.innerHTML = alertAnnotationField.innerHTML = '';
        //set of the fields to ''
        textCreatorInformation.innerHTML = textExternalVocabularies.innerHTML = textWorksByCreator.innerHTML = textWorksOfSameType.innerHTML = '';

        //set badges with LOD #results to default (0 results)
        badgeHeadingLODOne.innerHTML = badgeHeadingLODTwo.innerHTML = badgeHeadingLODThree.innerHTML = 0;

        textInfoCardSimilarObjects.style.display = 'none';
        textInfoCardAlternateHypothesis.style.display = 'none';
    }

    /**
    * @desc Toggle a menu with debugging options and make other debug buttons visible
    */
    toggleDebugMenu() {
        if ($(".btn-debug").css("display") == "none") {
            $(".btn-debug").css("display", "block");
        } else {
            $(".btn-debug").css("display", "none");
        }
    }

    /**
     * @desc Get progress from loading progress event fired by babylonjs loader
     * @param evt {evt} progress event
    */
    getProgress(evt) {
        loadingScreen.innerHTML = 'Loading scene meshes: ' + Math.round((evt.loaded / evt.total) * 100) + '% (' + (evt.loaded / 1000000).toFixed(1) + ' of ' + (evt.total / 1000000).toFixed(1) + ' MB)';
    }

    /* Various helpers */
    /**
    * @desc (helper) Get the current viewport (bootstrap)
    */
    getViewport() {
        // https://stackoverflow.com/a/8876069
        const width = Math.max(
            document.documentElement.clientWidth,
            window.innerWidth || 0
        )
        if (width <= 576) return 'xs'
        if (width <= 768) return 'sm'
        if (width <= 992) return 'md'
        if (width <= 1200) return 'lg'
        return 'xl'
    }

    /**
     * @desc (helper) Click the thumbnail of an image in the sidebar
     * @param evt {event}
    */
    clickImageThumbnail(evt) {
        //taken from 2D interface code//
        //reset values
        $('#imageOverlayImageCaption').html('');
        $('#imageOverlayImageTitle').html('');

        //get data display from event
        var src = evt.currentTarget.src;
        var url = evt.currentTarget.dataset.url;
        //var creator = evt.currentTarget.dataset.creator;
        var title = evt.currentTarget.dataset.title;
        var id = evt.currentTarget.dataset.id;
        if (src) {
            if (src.includes('video')) {
                modalMediaPlayerBody.innerHTML = this.settings.DEFAULT_MODAL_VIDEO;
                $(evt.currentTarget).attr('data-target', '#mediaOverlay');
            } else if (src.includes('audio')) {
                modalMediaPlayerBody.innerHTML = this.settings.DEFAULT_MODAL_AUDIO;
                $(evt.currentTarget).attr('data-target', '#mediaOverlay');
            } else {
                var imgUrl = src;
                //replace wikidata url with a higher res one [todo: check if this might cause issues for adamnet and other sources]
                imageOverlayImage.src = imgUrl.replace('100px', '600px');
                if (title) {
                    $('#imageOverlayImageTitle').html('<b>Title:</b> ' + title);
                }
                if (url) {
                    $('#imageOverlayImageCaption').html('<b>Source:</b> <a target="_blank" href="' + url + '">' + url + '</a>');
                } else {
                    $('#imageOverlayImageCaption').html('');
                }
                //taken from 2D interface code//
            }
            //here, update the currently shown image
        }
    }

    /**
     * @desc (helper function) Update all custom Bootstrap tooltips (e.g. when additional ones have been added dynamically)
    */
    updateBootstrapTooltips() {
        //activate tooltips bootstrap
        $(function () {
            //standard way to do it is, is via data-toggle
            //However, for some thumbnails data-toggle is already in use, so use data-tooltip instead 
            $('[data-tooltip="tooltip"]').tooltip();
        })
    }

    /**
     * @desc (helper function) Update all images via Jquery so they are clickable
    */
    updateJQueryClickableImages() {
        //update overlay image src
        $('.clickableImg').on('click', function (evt) {
            console.log('[log] modal for general image will be shown');
            this.clickImageThumbnail(evt);
        }.bind(this))
    }
}

export default GUI2D;