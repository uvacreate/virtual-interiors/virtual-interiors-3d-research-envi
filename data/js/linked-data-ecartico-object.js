/**
 * @classdesc LinkedDataWorkObject creates objects to store an Ecartico person and its basic properties in (could be extended further)
 */
import LinkedDataObject from "./linked-data-object.js";

class LinkedDataEcarticoObject extends LinkedDataObject {
    constructor(id, name, originalQuery, originalEndpoint, dataSourceLabel, dataSourceURL, birthDate, deathDate) {
        //get common properties from parent class
        super(id, name, originalQuery, originalEndpoint, dataSourceLabel, dataSourceURL);

        //properties specific to this class
        //birthdate of person denoted by Ecartico ID
        this.birthDate = birthDate;
        //deathdate of person denoted by Ecartico ID
        this.deathDate = deathDate;
        //array of external relations
        this.externalRelations = [];
    }

    /**
     * @descGet Get the number of results for the "external relations" in ecartico linked data {int}
    */
    get numberOfResults() {
        return this.externalRelations.length;
    }
}

export default LinkedDataEcarticoObject;