//use papa-parse to more easily load/parse csv files
var PapaParse = require('papaparse');

/**
 * @classdesc InputDataHelper class contains functions related to retrieving and validating input data (CSV and URL parameters).
 */
class InputDataHelper {
    constructor() {
    }

    /**
      * @desc Parses a CSV file using PapaParse plugin
      * @return Parsed data {arr} ([column1,column2,..][row1,row2,..])
    */
    parseCSV(url) {
        return new Promise((resolve, reject) => {
            PapaParse.parse(url, {
                download: true,
                //header: true,
                complete(results, url) {
                    if(results.errors != '') {
                        console.log('Error in parsing CSV : ' + results.errors);
                    }
                    console.log(results.meta);
                    console.log(results.data);
                    resolve(results.data);
                },
                error(err, url) {
                    reject(err);
                }
            })
        })
    }

    /**  
     * @desc Converts a string to a bool. This conversion will: match 'true', 'on', or '1' as true, ignore all white-space padding, ignore capitalization (case),'  tRue  ','ON', and '1   ' will all evaluate as true.
     * @param s {Str} input string 
    */
    parseBoolean(s) {
        // will match one and only one of the string 'true','1', or 'on' regardless
        // of capitalization and regardless of surrounding white-space.
        var regex = /^\s*(true|1|on)\s*$/i
        return regex.test(s);
    }

    /**  
    * @desc Check input given as a URL parameter
    * @param paramStr {Str} parameter string to check for in the URL parameter
    * @param defaultVal {Str} default value if no parameter is found
    * @param parseTypeFunction {Func} function to check/validate the input string
    */
    checkParameter(paramStr, defaultVal, parseTypeFunction) {
        //get the specified parameter string
        var param;
        //check for parameter, catch malformedURL expecptions
        try {
            var param = this.getParameterByName(paramStr);
        } catch (err) {
            alert('Error in one of the parameters (' + err + ').');
            console.log('error');
            return defaultVal;
        }
        //try to parse the value
        return this.parseInputData(param, defaultVal, parseTypeFunction, 'Error while parsing data value.')

    }

    /**
    * @desc Parse input data (given in URL parameter)
    * @param dataVal {Str} data value to parse
    * @param defaultVal {Str} default value if no correct data is found
    * @param parseTypeFunction {Func} function to check/validate the input string
    * @param errorStr {String} String to display when data is not correct
    */
    parseInputData(dataVal, defaultVal, parseTypeFunction, errorStr) {
        if (dataVal != null) {
            try {
                var parsed = parseTypeFunction(dataVal);
                return parsed;
            } catch (err) {
                alert(errorStr + ' (' + err + ').');
                console.log('error');
                return defaultVal;
            }
        } else {
            return defaultVal;
        }
    }

    /**
    * @desc (generic) get input file from browser-parameter (https://stackoverflow.com/questions/901115/how-can-i-get-query-string-values-in-javascript)
    * @param name {str}
    * @param url {str}
    */
    getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, "\\$&");
        var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, " "));
    }
}

export default InputDataHelper;