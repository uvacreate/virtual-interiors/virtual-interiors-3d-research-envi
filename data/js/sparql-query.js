/**
 * @desc SPARQLQueryDispatcher specifies different queries towards Linked Data SPARQL endpoints, and generates a query string using on different (singular, double, triple; array) variables provided as parameters
 */
 class SPARQLQuery {
	constructor() {
	}

    /**
    * @desc Do a single variable SPARQL query. Replace _VAR1_ in the query by the provided variable
    */
	singleVariableQuery( sparqlQueryStr, variable ) {
        return sparqlQueryStr.replace('_VAR1_', variable);
    }
    
    /**
    * @desc Do a double variable SPARQL query. Replace _VAR1_ in the query by the provided variable
    */
    doubleVariableQuery( sparqlQueryStr, variableOne, variableTwo ) {
        var q = sparqlQueryStr.replace('_VAR1_', variableOne);
        q = q.replace('_VAR2_', variableTwo);
        return q;
    }

    /**
    * @desc Do a triple variable SPARQL query. Replace _VAR1_ in the query by the provided variable
    */
    tripleVariableQuery( sparqlQueryStr, variableOne, variableTwo, variableThree ) {
        var q = sparqlQueryStr.replace('_VAR1_', variableOne);
        q = q.replace('_VAR2_', variableTwo);
        q = q.replace('_VAR3_', variableThree)
        return q;
    }

    /**
    * @desc Do a SPARQL query based on 'n' variables, based on an array. Replace _MULTIVAR_ for each part of the multi-array query. Replace _MULTIVAR_POS_ in the query by generated query string.
    */
    arrayVariableQuery( sparqlQueryStr, sparqlQueryVariable, variableArr ) {
        //create a custom sparql FILTER based on an array of variables
        var multiVarStr = '';
        const MAX_LENGTH = 70;
        var maxLength = variableArr.length;
        if(variableArr.length > MAX_LENGTH) { 
            maxLength = MAX_LENGTH;
        }
        for(var i=0; i<maxLength; i++) {
            multiVarStr += sparqlQueryVariable.replace('_MULTIVAR_', variableArr[i]); 
            if(i<maxLength - 1) {
                multiVarStr += ' || ';
            }
        }
        //finally, put the created filter in the right location of the sparql query
        return sparqlQueryStr.replace('_MULTIVAR_POS_', multiVarStr);
    }

    /**
    * @desc (Improved version) Do a SPARQL query based on 'n' variables, based on an array. Replace _MULTIVAR_ for each part of the multi-array query. Replace _MULTIVAR_POS_ in the query by generated query string.
    */    
    arrayVariableQueryV2( sparqlQueryStr, sparqlQueryVariable, variableArr, variablePrefix, variableSuffix, finalSuffix ) {      
        //create a custom sparql FILTER based on an array of variables
        var multiVarStr = '';
        const MAX_LENGTH = 500;
        var maxLength = variableArr.length;
        //limit query to a certain length to prevent errors, timeouts
        if(variableArr.length > MAX_LENGTH) { 
            console.log('[warning] capping query at maximum length ' + MAX_LENGTH + ' instead of ' + maxLength);
            maxLength = MAX_LENGTH;
        }
        for(var i=0; i<maxLength; i++) {
            var currVar = variableArr[i];
            if(variablePrefix != null) {
                currVar = variablePrefix + currVar;
            }
            multiVarStr += sparqlQueryVariable.replace('_MULTIVAR_', currVar);
            if(i==maxLength - 1) {
                multiVarStr += finalSuffix;
            } else {
                multiVarStr += variableSuffix + ' ';
            }
        }
        //finally, put the created filter in the right location of the sparql query, indicated by _MULTIVAR_POS_
        var retVal = sparqlQueryStr.replace('_MULTIVAR_POS_', multiVarStr);
        return retVal;
    }
}

export default SPARQLQuery;