import LinkedDataObject from "./linked-data-object.js";

/**
 * @classdesc LinkedDataWorksObject creates objects to store "works" (e.g. artworks) and their basic shared properties in
 */
class LinkedDataCreatorObject extends LinkedDataObject {
    constructor(id, name, originalQuery, originalEndpoint, dataSourceLabel, dataSourceURL) {
        //get common properties from parent class
        super(id, name, originalQuery, originalEndpoint, dataSourceLabel, dataSourceURL);

        //properties specific to this class
        //array of works
        this.works = [];      
    }

    /**
     * @descGet Get the number of works in the data object
    */
    get numberOfResults() {
        return this.works.length;
    }
}

export default LinkedDataCreatorObject;