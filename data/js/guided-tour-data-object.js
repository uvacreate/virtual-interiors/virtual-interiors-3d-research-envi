import InputDataHelper from "./input-data-helper";

/**
 * @classdesc GuidedTourDataObject class creates a Guided Tour position object, with various properties (most optional) used in a guided tour.
* */
class GuidedTourDataObject {
    /**
     * @desc Create a single data object to included in the guided tour (* obligatory parameter)
     * @param camPos {Vector3} position of camera (Vector3)*
     * @param camRot {Vector3} rotation of camera (Vector3)*
     * @param lightsS {int} lights setting to apply (#, corresponds to lighting setting in app-settings.js)
     * @param selectedMInScene {Str} indicates if a mesh should be selected in the scene by default
     * @param selectionM {Str} indicates if a selection filter should be activated (see main.js -> setSelectionFilter() for possible values, e.g. 'highlight-mesh')
     * @param clearFilter {Bool}  if true, clear selection filter upon moving to this guided tour position
     * @param lensEffectsEnabled {Bool} if true, activate lens effects (now: Ambient Occlusion)
     * @param meshArrToHide {Arr} Arr of Strings, indicating mesh names to hide in the scene
     * @param meshArrToShow {Arr} Arr of Strings, indicating mesh names to show in the scene
     * @param textToShow {Str} Text to show in the guided tour information panel
    */
    constructor(camPos, camRot, textToShow, lightsS, selectedMInScene, selectionM, clearFilter, lensEffectsEnabled, meshArrToHide, meshArrToShow) {
        this.inputDataHelper = new InputDataHelper();
        if(camPos == null || camRot == null) {
            console.log('[err] a camera position (Vector3) and camera rotation (Vector3) are required for a guided tour position');
        } else {
            this.camPos = camPos;
            this.camRot = camRot;
            this.textToShow = this.inputDataHelper.parseInputData(textToShow, '', String);
            this.lightsS = this.inputDataHelper.parseInputData(lightsS, null, parseInt);
            this.selectedMInScene = this.inputDataHelper.parseInputData(selectedMInScene, null, String);
            this.selectionM = this.inputDataHelper.parseInputData(selectionM, null, String);
            this.clearFilter = this.inputDataHelper.parseInputData(clearFilter, false, this.inputDataHelper.parseBoolean);
            this.lensEffectsEnabled = this.inputDataHelper.parseInputData(lensEffectsEnabled, false, this.inputDataHelper.parseBoolean);
            this.meshArrToHide = this.inputDataHelper.parseInputData(meshArrToHide, null, Array.isArray);
            this.meshArrToShow = this.inputDataHelper.parseInputData(meshArrToShow, null, Array.isArray);
        }
    }

    /** 
     * @desc get an object with all combined guided tour variables.
    */
    get dataObject() {
        return {            
            cameraPosition: this.camPos,
            cameraRotation: this.camRot,
            textToShow: this.textToShow,
            lightsSetting: this.lightsS,
            selectedMeshInScene: this.selectedMInScene,
            selectionMode: this.selectionM,
            clearFilter: this.clearFilter,
            lensEffectsEnabled: this.lensEffectsEnabled,
            meshArrToHide: this.meshArrToHide,
            meshArrToShow: this.meshArrToShow
        }
    }
}

export default GuidedTourDataObject;