import SPARQLData from "./sparql-data.js";

/**
 * @classdesc SPARQLDataEcartico contains functions to load SPARQL data from Ecartico
 */
class SPARQLDataEcartico extends SPARQLData {
    constructor() {
        super();
        //public sparql endpoint
        this.endpointUrl = 'https://data.create.humanities.uva.nl/sparql';
        //SPARQL query to issue
        this.sparqlQuery = 'PREFIX owl: <http://www.w3.org/2002/07/owl#> PREFIX schema: <http://schema.org/> PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#> SELECT * WHERE { GRAPH <https://data.create.humanities.uva.nl/id/ecartico/> { ?ecartico owl:sameAs ?externalRelations ; schema:name ?name ; schema:birthDate ?birthDate ; schema:deathDate ?deathDate . FILTER regex(?ecartico, "^https://www.vondel.humanities.uva.nl/ecartico/persons/_VAR1_$", "i"). }}';
        //label for this data source
        this.dataSourceLabel = 'Ecartico';
        //URL with information about data source
        this.dataSourceURL = 'http://vondel.humanities.uva.nl/ecartico/';
    }

    /**
     * @desc Load SPARQL data (related vocabularies) from CREATE endpoint b/o ecarticoID
     * @param ecarticoID {str}
     * @returns array with results (todo: as object)
    */
    async getEcarticoData(ecarticoID) {
        return this.getEcarticoObjectBasedOnSingleVariableQuery(ecarticoID, this.sparqlQuery, this.endpointUrl, this.dataSourceLabel, this.dataSourceURL);
    }

}

export default SPARQLDataEcartico;
