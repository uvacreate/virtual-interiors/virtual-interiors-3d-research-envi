import GuidedTourDataObject from "./guided-tour-data-object";

/**
 * @classdesc GuidedTour class manages the contents of a guided tour (display of guided tour is done in main.js).
* */
class GuidedTour {
    constructor() {
        this._guidedTourArray = [];
    }

    /**
     * @desc Add a position to the guided tour (array) (* obligatory parameter)
     * @param camPos {Vector3} position of camera (Vector3)*
     * @param camRot {Vector3} rotation of camera (Vector3)*
     * @param lightsS {int} lights setting to apply (#, corresponds to lighting setting in app-settings.js)
     * @param selectedMInScene {Str} indicates if a mesh should be selected in the scene by default
     * @param selectionM {Str} indicates if a selection filter should be activated (see main.js -> setSelectionFilter() for possible values, e.g. 'highlight-mesh')
     * @param clearFilter {Bool}  if true, clear selection filter upon moving to this guided tour position
     * @param lensEffectsEnabled {Bool} if true, activate lens effects (now: Ambient Occlusion)
     * @param meshArrToHide {Arr} Arr of Strings, indicating mesh names to hide in the scene
     * @param meshArrToShow {Arr} Arr of Strings, indicating mesh names to show in the scene
     * @param textToShow {Str} Text to show in the guided tour information panel
    */
    addGuidedTourPosition(camPos, camRot, textToShow, lightsS, selectedMInScene, selectionM, clearFilter, lensEffectsEnabled, meshArrToHide, meshArrToShow) {
        this.guidedTourArray.push(
            new GuidedTourDataObject(camPos, camRot, textToShow, lightsS, selectedMInScene, selectionM, clearFilter, lensEffectsEnabled, meshArrToHide, meshArrToShow).dataObject
        );
    }

    /**
     * @desc Get an array of GuidedTourDataObjects
     */
    get guidedTourArray() {
        return this._guidedTourArray;
    }

    /**
     * @desc Set an array of GuidedTourDataObjects
    */
    set guidedTourArray(val) {
        this._guidedTourArray = val;
    }
}

export default GuidedTour;