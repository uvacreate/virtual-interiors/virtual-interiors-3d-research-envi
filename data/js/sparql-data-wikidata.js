/**
 * @classdesc SPARQLDataWikidata contains functions to load SPARQL data from Wikidata
 */

import SPARQLData from "./sparql-data.js";

class SPARQLDataWikidata extends SPARQLData {
    constructor() {
        super();
        //public sparql endpoint
        this.endpointUrl = 'https://query.wikidata.org/sparql';
        //label for this data source
        this.dataSourceLabel = 'Wikidata';
        //URL with information about data source
        this.dataSourceURL = 'https://www.wikidata.org';

        //query to get artist / artwork information from wikidata, uses ecartico id
        this.SPARQL_Q_GET_WIKIDATA_DATA_WORKS_BY_SAME_CREATOR = 'SELECT ?item ?itemLabel ?itemImage ?work ?workLabel ?workImage ?workYear WHERE { 	    ?item wdt:P2915 "_VAR1_";	        wdt:P18 ?itemImage.	     ?work wdt:P170 ?item;	    	wdt:P18 ?workImage;	    	wdt:P571 ?workDate.	    BIND(YEAR(?workDate) as ?workYear).	    SERVICE wikibase:label { 	        bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en".	    }	 } LIMIT 50';
        this.SPARQL_Q_GET_WIKIDATA_DATA_WORKS_OF_SAME_TYPE = 'SELECT ?work ?workImage ?workLabel ?workYear	WHERE { 	    hint:Query hint:optimizer "None".	  	  VALUES ?inputValue { "_VAR1_" }.	  	    BIND(	    IF(?inputValue="300418022","300028094",	       ?inputValue)	    as ?aat).	  	    ?item wdt:P1014 ?aat. 		    ?item wdt:P279*|wdt:P31 ?broadCategories.	 	    BIND(	        IF(?broadCategories = wd:Q14745, wd:Q14745,	        IF(?broadCategories = wd:Q860861, wd:Q860861,	        IF(?broadCategories = wd:Q151771, wd:Q151771,  	        IF(?broadCategories = wd:Q750197, wd:Q3305213,	        IF(?broadCategories = wd:Q36794, wd:Q36794,	        IF(?broadCategories = wd:Q4006, wd:Q4006,	      "-1"))))))	    AS ?broadClass).		      FILTER(?broadClass != "-1").	  	      ?work wdt:P31 ?broadClass;  	    wdt:P571 ?workDate;	    wdt:P18 ?workImage.	  	    FILTER(?workDate > "1600-00-00"^^xsd:dateTime && ?workDate < "1700-00-00"^^xsd:dateTime).	  	    BIND(YEAR(?workDate) as ?workYear).	  	    SERVICE wikibase:label { bd:serviceParam wikibase:language "[AUTO_LANGUAGE],en". }  	}	LIMIT 30';
    }

    /**
     * @desc Load SPARQL data from wikidata b/o ecarticoID
     * @param wikidataID {str}
     * @param resultFunction {function}
    */
    async getWikidataData(ecarticoID) {
        return this.getWorksObjectBasedOnSingleVariableQuery(ecarticoID, this.SPARQL_Q_GET_WIKIDATA_DATA_WORKS_BY_SAME_CREATOR, this.endpointUrl, this.dataSourceLabel, this.dataSourceURL);
    }

    /**
     * @desc Load SPARQL data from wikidata b/o aatURI
     * @param aatURI {str}
    */
    async getWikidataDataRelatedWorksAAT(aatURI) {
        if (aatURI != '') {
            //wikidata query necessitates only AAT ID (last part of URI)
            //e.g. http://vocab.getty.edu/aat/300184633 --> 300184633
            var aatID = aatURI.split('/')[aatURI.split('/').length - 1];
            if (aatID != '') {
                return this.getWorksObjectBasedOnSingleVariableQuery(aatID, this.SPARQL_Q_GET_WIKIDATA_DATA_WORKS_OF_SAME_TYPE, this.endpointUrl, this.dataSourceLabel, this.dataSourceURL);
            };
        }
        //return empty creator object
        return this.createWorksObject()
    }
}

export default SPARQLDataWikidata;