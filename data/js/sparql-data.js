//use custom class for dispatching SPARQL queries
import SPARQLQueryDispatcher from './sparql-query-dispatcher.js';
//use custom class for creating SPARQL queries including variable values
import SPARQLQuery from './sparql-query.js';
//custom objects to store linked data in
import LinkedDataWorksObject from './linked-data-works-object.js';
import LinkedDataWorkObject from './linked-data-work-object.js';
import LinkedDataEcarticoObject from './linked-data-ecartico-object.js';

/**
 * @classdesc SPARQLData contains generic functions to load SPARQL data (subclasses contain specifics for each data source), and process it towards a common format (Works object, Ecartico object) used in the application
*/
class SPARQLData {
    constructor() { 
        //public sparql endpoint
        this.endpointUrl = '';
        //label for this data source
        this.dataSourceLabel = '';
        //URL with information about data source
        this.dataSourceURL = '';
    }

    /**
     * @desc (generic) Load SPARQL data
     * @param endPointUrl {str}
     * @param sparqlQuery {str}
     * @param resultFunction {function}
    */
    async loadSparqlData(endPointUrl, sparqlQuery) {
        if(endPointUrl==null || sparqlQuery==null) {
            console.log("[err] No endpoint URL or SPARQL query provided"); 
            return null;
        };
        const queryDispatcher = new SPARQLQueryDispatcher(endPointUrl);
        var returnValue = queryDispatcher.query(sparqlQuery).then((sparqlResultObj) => {
            //convert returned object (including result.. bindings..) to an array with the found results
            if(sparqlResultObj) {
                return this.getSparqlResultsArr(sparqlResultObj);
            } else {
                return null;
            }
        });
        return returnValue;
    }

    /**
     * @desc (generic) Get results from SPARQL data event
     * @param endPointUrl {str}
     * @param sparqlQuery {str}
     * @param resultFunction {function}
    */
    async getSparqlResultsArr(resArr) {
        if(resArr) {
            if(resArr.results) {
            if(resArr.results.bindings) {
                var resultsArr = resArr.results.bindings;
                if(resultsArr.length>0) {
                return resultsArr;
                }
            }
            }
        } else {
            console.log("[warning] no SPARQL results retrieved");
            //to avoid errors, always return empty array
            return [];
        }
    }

    /**
     * @desc (generic) Get a generic Works object from a single variable SPARQL query
    */
    async getWorksObjectBasedOnSingleVariableQuery(queryVariable, sparqlQuery, endpointUrl, dataSourceLabel, dataSourceURL) {
        if(queryVariable==null) {
            return this.createWorksObject();
        } else {
            var sparqlQueryWithVariable = new SPARQLQuery().singleVariableQuery(sparqlQuery, queryVariable);
            var sparqlResultArray = await this.loadSparqlData(endpointUrl, sparqlQueryWithVariable);
            return this.createWorksObject(sparqlResultArray,sparqlQueryWithVariable, endpointUrl, dataSourceLabel, dataSourceURL);
        }
    }

    /**
     * @desc (generic) Get a generic Works object from a multi variable SPARQL query
    */
    async getWorksObjectBasedOnMultiVariableQuery(queryVariableArr, sparqlQueryStr, sparqlQueryVar, endpointUrl, dataSourceLabel, dataSourceURL) {
        if(queryVariableArr==null) {
            return this.createWorksObject();
        } else {
            var sparqlQueryWithVariable = new SPARQLQuery().arrayVariableQueryV2(sparqlQueryStr, sparqlQueryVar, queryVariableArr, '', '', '' );
            var sparqlResultArray = await this.loadSparqlData(endpointUrl, sparqlQueryWithVariable);
            return this.createWorksObject(sparqlResultArray, sparqlQueryWithVariable, endpointUrl, dataSourceLabel, dataSourceURL);
        }
    }

    /**
     * @desc (generic) create an object to store creator & works information. A works object contains info on the sparql query, shared values (e.g. creator) and works
     * @param lodArr {arr} array with sparql results, result labels need to include "item", "work", "workLabel", "workImage"
    */
    createWorksObject(lodArr, sparqlQueryWithVariable, endPoint, dataSourceLabel, dataSourceURL) {
        //always create default artist/itemtype object
        var dataObj = new LinkedDataWorksObject('id', 'name', sparqlQueryWithVariable, endPoint, dataSourceLabel, dataSourceURL);

        if(lodArr != null && lodArr.length > 0) {
            //basic artist properties reside in all rows, so just take first row for those
            var lodObj = lodArr[0];

            //optional: id of linked data resource (e.g. URI of a creator)
            if(lodObj.item) {
                dataObj.id = lodObj.item.value;
            }
            //optional: label of linked data resource (e.g. name of a creator)
            if(lodObj.itemLabel) {
                dataObj.name = lodObj.itemLabel.value;
            }

            for(var i=0; i<lodArr.length; i++) {
                //optional workYear property
                var workYear = "no date";
                if(lodArr[i].workYear) {
                    workYear = lodArr[i].workYear.value
                }
                //optional workCreator property (URI)
                var workCreator = '';
                if(lodArr[i].workCreator) {
                    workYear = lodArr[i].workCreator.value
                }
                //optional workLabel property (URI)
                var workCreatorLabel = '';
                if(lodArr[i].workCreatorLabel) {
                    workYear = lodArr[i].workCreatorLabel.value
                }
                //create and return custom works object
                var workObj = new LinkedDataWorkObject(lodArr[i].work.value, lodArr[i].workLabel.value, lodArr[i].workImage.value, workYear, workCreator, workCreatorLabel);
                //do not push duplicate items (based on ID)
                if(dataObj.works.length == 0) {
                    dataObj.works.push(workObj);
                } else if(dataObj.works[dataObj.works.length-1].id != workObj.id) {
                    dataObj.works.push(workObj);
                } else {
                    //do not add duplicate content      
                }
            }  
        }
        return dataObj;
    }

    async getEcarticoObjectBasedOnSingleVariableQuery(queryVariable, sparqlQuery, endpointUrl, dataSourceLabel, dataSourceURL) {
        if(queryVariable==null) {
            return this.createEcarticoObject();
        } else {
            var sparqlQueryWithVariable = new SPARQLQuery().singleVariableQuery(sparqlQuery, queryVariable);
            var sparqlResultArray = await this.loadSparqlData(endpointUrl, sparqlQueryWithVariable);
            return this.createEcarticoObject(sparqlResultArray,sparqlQueryWithVariable, endpointUrl, dataSourceLabel, dataSourceURL);
        }
    }

    /**
     * @desc (generic) create a JS object to store ecartico results in
     * @param lodArr {arr} array with sparql results, result labels need to include "birthDate", "deathDate", "externalRelations"
    */
    createEcarticoObject(lodArr, sparqlQueryWithVariable, endPoint, dataSourceLabel, dataSourceURL) {
        //always create custom ecartico object
        var ecarticoObject = new LinkedDataEcarticoObject('', '', sparqlQueryWithVariable, endPoint, dataSourceLabel, dataSourceURL, '', '');

        if(lodArr != null && lodArr.length > 0) {
            //Birthdate / deathdate are the same for all rows of the lodArr, so just take the first row for these
            var lodObj = lodArr[0];
            //get values for birth/death date
            if(lodObj.birthDate.type=="typed-literal") {
                ecarticoObject.birthDate = lodObj.birthDate.value;
            } else {
                ecarticoObject.birthDate = 'unknown';
            }
            if(lodObj.deathDate.type=='typed-literal') {
                ecarticoObject.deathDate = lodObj.deathDate.value;
            } else {
                ecarticoObject.deathDate = 'unknown';
            }
            ecarticoObject.id = lodObj.ecartico.value;
            ecarticoObject.name = lodObj.name.value;

            //Get the external relations (i.e. identifiers in other datasets)
            for(var i=0; i<lodArr.length; i++) {
                var extRelURL = new URL(lodArr[i]['externalRelations'].value);
                var extRelObj = { url : extRelURL, hostname : extRelURL.hostname };
                ecarticoObject.externalRelations.push(extRelObj);
            }
        }
        return ecarticoObject;
    }
}

export default SPARQLData;