import * as BABYLON_GUI from 'babylonjs-gui';
import SPARQLDataAdamnet from './sparql-data-adamnet.js';

/**
 * @classdesc GUI3D class manages everything related to 3D UI. 3D UI is shown on small viewports (e.g. mobile, small browser window) and in VR (e.g. Oculus Quest). Experimental functionality. (note: use .bind(this) on functions passed as parameters)
 */
class GUI3D {
    constructor(settings, lighting, toggleUncertaintyFilterFunction) {
        /* variables used in this class */
        this.INFOPANEL_3D_POSITION = settings.INFOPANEL_3D_POSITION;
        this.INFOPANEL_3D_ROTATION = settings.INFOPANEL_3D_ROTATION;
        this.INFOPANEL_3D_MARGIN = settings.INFOPANEL_3D_MARGIN;
        this.INFOPLANE_3D_WIDTH = settings.INFOPLANE_3D_WIDTH;
        this.INFOPLANE_3D_HEIGHT = settings.INFOPLANE_3D_HEIGHT;
        this.INFOPLANE_3D_FONT_SIZE = settings.INFOPLANE_3D_FONT_SIZE;
        this.TITLEPLANE_3D_HEIGHT = settings.TITLEPLANE_3D_HEIGHT;
        this.BUTTON_PANEL_3D_POSITION = settings.BUTTON_PANEL_3D_POSITION;
        this.BUTTON_PANEL_3D_ROTATION = settings.BUTTON_PANEL_3D_ROTATION;
        //variable to store/access BJS native GUI3D elements
        this.threeDimensionalInterfaceElementsArr = [];
        //variable for BJS native GUI3D manager
        this.gui3DManager;
        //BJS scene (from main class)
        this.scene;
        //lighting of BJS scene
        this.lighting = lighting;
        //other details
        this.selectedMeshName;
        this.ecarticoID;
        this.adamnetCategories;
        this.aatURI;
        //access function located in main.js class
        this.toggleUncertaintyFilter = toggleUncertaintyFilterFunction;
    }

    /**
     * @desc Initialize 3D GUI settings and create info/button panels.
    */
    create3DGUI(scene, selectedMeshName, imageToDisplay, titleToDisplay, descrToDisplay, ecarticoID, adamnetCategories, aatURI) {
        //update reference to selectedmesh
        this.selectedMeshName = selectedMeshName;
        this.ecarticoID = ecarticoID;
        this.adamnetCategories = adamnetCategories;
        this.aatURI = aatURI;
        //remove previous panels (if existing)
        this.removeAllPanelsFrom3DGUI();
        //set scene reference in this class to the one from main.js
        this.scene = scene;
        //add lighting class reference
        //this.lighting = lighting;
        //Create all information panels and navigation
        this.createAllPanels(selectedMeshName, imageToDisplay, titleToDisplay, descrToDisplay)
    }

    /**
     * @desc Create 3D GUI info/button panels.
    */
    createAllPanels(selectedMeshName, imageToDisplay, titleToDisplay, descrToDisplay) {
        titleToDisplay = String(titleToDisplay);
        descrToDisplay = String(descrToDisplay);
        this.gui3DManager = new BABYLON_GUI.GUI3DManager(this.scene);
        //create VR buttons and pass on image url
        this.createVRButtonPanel(imageToDisplay, selectedMeshName, titleToDisplay, descrToDisplay);
        //only show information if available
        //[fixme] Due to TypeError, cast variables to String
        this.add3DInformationPlaneToScene(titleToDisplay, descrToDisplay, null, false, this.removeAllPanelsFrom3DGUI.bind(this), null, null, null, null, 1, 1, this.INFOPANEL_3D_MARGIN);
    }

    /**
     * @desc Create panel of 5 buttons in VR
     * @param imageURL {str} Image URL
     * @todo reinstate lights setting; uncertainty filter
    */
    createVRButtonPanel(imageURL, selectedMeshName, titleToDisplay, descrToDisplay) {
        //first remove previous menu, if visible
        this.removePanelFrom3DGUI('NavCylPan');
        //create anchor for positioning
        var anchor = new BABYLON.TransformNode("NavPanelAnchor");
        //set temporary values (until in all settings files)
        if (this.BUTTON_PANEL_3D_POSITION == null) {
            this.BUTTON_PANEL_3D_POSITION = this.INFOPANEL_3D_POSITION;
        }
        if (this.BUTTON_PANEL_3D_ROTATION == null) {
            this.BUTTON_PANEL_3D_ROTATION = this.INFOPANEL_3D_ROTATION;
        }
        //add std rotation of (0,0,0) for bugfix, see below
        anchor = this.createInformationPanel3DAnchor(anchor, this.scene.activeCamera, this.BUTTON_PANEL_3D_POSITION, new BABYLON.Vector3(0, 0, 0));
        //create a GUI cylinder panel to show information planes in
        var panel = this.createGUICylinderPanel("NavCylPan", 2, 1, 1.5, 0.03);
        //Add this to the list of 3D GUI elements (for later removal)
        this.addUIElementTo3DGUI(panel);
        //use a transform node to position the panel
        panel.linkToTransformNode(anchor);
        //if text: show only a text, if imageURL: show only an image
        var buttonOne = this.createGUIMeshButton3D(0.16, 0.14, null, 'data/images/icon-image-colored.svg', true, this.showImagePanelInVR.bind(this), [imageURL, 'Object image:', titleToDisplay, descrToDisplay], null, 'vrButton1');
        panel.addControl(buttonOne);
        var buttonTwo = this.createGUIMeshButton3D(0.16, 0.14, null, 'data/images/icon-sun-colored.svg', false, this.lighting.toggleLightsSettingVR.bind(this.lighting), this.scene, null, 'vrButton2');
        panel.addControl(buttonTwo);
        var buttonThree = this.createGUIMeshButton3D(0.16, 0.14, null, 'data/images/icon-chart-line-colored.svg', false, this.toggleUncertaintyFilter, null, null, 'vrButton3');
        panel.addControl(buttonThree);
        var buttonFour = this.createGUIMeshButton3D(0.16, 0.14, null, "data/images/icon-link-colored.svg", true, this.createVRLinkedDataPanel.bind(this), selectedMeshName, null, 'vrButton4');
        panel.addControl(buttonFour);
        //bind original execution context (https://stackoverflow.com/questions/56994584/this-returning-undefined-after-function-call/56997919)
        var buttonFive = this.createGUIMeshButton3D(0.16, 0.14, null, "data/images/icon-close-colored.svg", true, this.removeAllPanelsFrom3DGUI.bind(this), null, null, 'vrButton5');
        panel.addControl(buttonFive);
        //fix issue with panel rotation
        this.bugFixPanelRotation(panel, this.BUTTON_PANEL_3D_ROTATION.y, null);
    }

    /**
     * @desc Create circular Linked Data panel (with images) in VR
     * @param selectedMeshName {str} Name of mesh that has been selected in the scene
    */
    async createVRLinkedDataPanel(selectedMeshName) {
        //get adamnet data via ecartico id
        var sparqlDataAdamnet = new SPARQLDataAdamnet();
        //get standard LOD obj (see sparqlData -> creatorObject)
        var lodObjLeft = await sparqlDataAdamnet.getAdamnetData(this.ecarticoID).catch(error => console.error(error));
        //use linked data adamnet function from main.js
        var lodObjRight = await (this.getAdamnetRelatedWorks(sparqlDataAdamnet, this.adamnetCategories, this.aatURI, this.ecarticoID));

        //create anchor for positioning
        var anchor = new BABYLON.TransformNode("VRNavPanelAnchor");

        //create a GUI cylinder panel to show information planes in
        var panel = this.createGUICylinderPanel("NavCylPan", 10, 5, 1.1, 0.075);
        //Add this to the list of 3D GUI elements (for later removal)
        this.addUIElementTo3DGUI(panel);
        //use a transform node to position the panel
        panel.linkToTransformNode(anchor);
        //set position of anchor
        anchor.position = new BABYLON.Vector3(1.42, 4.83, 4);

        //set where the left and right side of the panel occur (by index number of the panel). In the middle panel, no LOD images should be shown.
        //[left side] [middle panel] [right side]
        var startIndexLeftSide = 0;
        var endIndexLeftSide = 19;
        var startIndexRightSide = 30;
        var endIndexRightSide = 49;

        //set blockLayout to avoid issues with button placement
        panel.blockLayout = true;
        //left panel
        this.createVRLODButtonPanelRegion(panel, startIndexLeftSide, endIndexLeftSide, lodObjLeft, true);
        //empty part in the middle
        this.createVRLODButtonPanelRegion(panel, endIndexLeftSide + 1, startIndexRightSide - 1, null, false, '←      same artist', '→      same type');
        //right panel
        this.createVRLODButtonPanelRegion(panel, startIndexRightSide, endIndexRightSide, lodObjRight, false);
        panel.blockLayout = false;

        //Bugfix: rotation has to be done after object creation, otherwise panel doesn't appear correctly anymore (!)
        //https://forum.babylonjs.com/t/how-to-rotate-planepanel-or-any-volume-based-panel/13949/3
        this.bugFixPanelRotation(panel, null, 180);
    }

    /**
     * @desc Initialize 3D GUI settings and create it info/button panels.
     * @todo Currently duplicated from same function in main.js class
    */
    async getAdamnetRelatedWorks(sparqlData, adamnetCategories, aatURI, ecarticoID) {
        var adamnetResultObjRelWorks;
        if (adamnetCategories != null && adamnetCategories != '') {
            adamnetResultObjRelWorks = await sparqlData.getAdamnetDataRelatedWorksAdamnetCategories(adamnetCategories).catch(error => console.error(error));
        } else if (aatURI != null && aatURI != '') {
            adamnetResultObjRelWorks = await sparqlData.getAdamnetDataRelatedWorksAAT(aatURI).catch(error => console.error(error));
        } else if (ecarticoID != null && ecarticoID != '') {
            adamnetResultObjRelWorks = await sparqlData.getAdamnetDataRelatedWorks(ecarticoID).catch(error => console.error(error));
        }
        return adamnetResultObjRelWorks;
    }

    /**
     * @desc Create circular Linked Data button panel in VR
     * @param selectedMeshName {str} Name of mesh that has been selected in the scene
    */
    createVRLODButtonPanelRegion(panel, startIndex, endIndex, lodObj, isReversed, leftLabel, rightLabel) {
        //array to store the buttons in
        var buttonArr = [];
        var cnt = 0;
        //width of each button panel
        var w = 0.32;
        //height of each button panel
        var h = 0.28;
        const MAX_TXT_LENGTH = 32;
        //first create an array of buttons
        for (var i = startIndex; i < endIndex + 1; i++) {
            //standard (placeholder) image, transparent/empty, so invisible
            var imgUrl = 'data/images/transparent-image.png';
            var buttonID = 'vrLODButton' + i;
            //optional text in button
            var txt = null;
            //text for label above image
            var txtLabel = 'Linked data image:';
            if (lodObj) {
                if (cnt < lodObj.works.length) {
                    //set an LOD image url in place of the transparent image
                    imgUrl = lodObj.works[cnt].image;
                    txtLabel = lodObj.works[cnt].title;
                    //limit length of text
                    if (txtLabel.length > MAX_TXT_LENGTH + 4) {
                        txtLabel = txtLabel.substring(0, MAX_TXT_LENGTH) + ' ...';
                    }
                    cnt++;
                }
            } else {
                if (leftLabel != null && i == startIndex + 4) {
                    //set a textual label instead of an image
                    txt = leftLabel;
                } else if (rightLabel != null && i == startIndex + 9) {
                    txt = rightLabel;
                }
            }
            //button can contain either a text (txt) or an image (imgUrl)
            var button = this.createGUIMeshButton3D(w, h, txt, imgUrl, true, this.showImagePanelInVR.bind(this), [imgUrl, txtLabel], null, buttonID, '70');
            buttonArr.push(button);
        }

        //now add these buttons to the display, reversed if necessary (so buttons appear from the middle to the sides)
        if (!isReversed) {
            for (var i = 0; i < buttonArr.length; i++) {
                panel.addControl(buttonArr[i]);
            }
        } else {
            for (var i = buttonArr.length - 1; i >= 0; i--) {
                panel.addControl(buttonArr[i]);
            }
        }
    }

    /**
     * @desc Change the icon and behavior of a button which has already been added to the scene
     * @param buttonName {str} Name of button in BJS scene 
     * @param iconURL {str}
     * @param clickFunction {function}
     * @param clickFunctionParameter {var}
    */
    changeVRButtonIconAndBehavior(buttonName, iconURL, clickFunction, clickFunctionParameter) {
        //select button in scene
        var button = this.scene.getNodeByName(buttonName);
        var container = this.scene.getNodeByName('NavPanelAnchor');
        //BUG? in BabylonJS >5.0.0 alpha ~12, onObservable data disappears from mesh 
        //For this version -> change to button.metadata.GUI3D.control;
        //exchange image texture
        this.addImageTextureToMaterial(button.material, iconURL);
        //add new behavior to button (needs to be accessed via button.metadata._!)
        this.addBehaviorTo3DButton(button.metadata.GUI3D.control, clickFunction, clickFunctionParameter);
    }

    /**
     * @desc Show 3D information panel with object image. Uses parameter array to specify more than one parameter using the ClickFunctionParameter system
     * @param paramArr {Arr} Arr[0]: imageURL, Arr[1]: txtLabel
    */
    showImagePanelInVR(paramArr) {
        if (paramArr) {
            var imageURL = paramArr[0];
            var txtLabel = paramArr[1];
            var titleToDisplay = paramArr[2];
            var descrToDisplay = paramArr[3];
            if (imageURL) {
                //add the label and image to an information panel
                this.add3DInformationPlaneToScene(txtLabel, null, imageURL, null, this.removeAllPanelsFrom3DGUI.bind(this), null, null, null, null, 1, 2, this.INFOPANEL_3D_MARGIN);
                this.changeVRButtonIconAndBehavior('vrButton1', 'data/images/icon-text-colored.svg', /*this.showInformationPanelInVR.bind(this)*/ function () { this.create3DGUI(this.scene, this.selectedMeshName, imageURL, titleToDisplay, descrToDisplay) }.bind(this), null);
            } else {
                console.log("[warning] No imageURL specified (showImagePanelInVR)");
            }
        }
    }

    /**
     * @desc Show (default) 3D text panel
     * @param imageURL {str} Image URL
    */
    showInformationPanelInVR() {
        //remove navigation panel
        this.removePanelFrom3DGUI('NavCylPan');
        //remove main information panels
        this.removeInformationPanelsFrom3DGUI();
        //show object info for currently selected mesh
        var currMesh = getSelectedMeshID();
    }

    /**
     * @desc Create 1 or multiple panels with information in the 3D space
     * @param title {Str} header text
     * @param text {Str} paragraph text
     * @param imageURL {str} Image URL
     * @param inGuidedTour {Bool} whether in Guided tour
     * @param clickFunction {Func} function to invoke when clicking button
     * @param clickFunction {Func} parameter to pass to click function
     * @param pos {} position of info panel
     * @param rot {} rotation of info panel
     * @param name {Str} name of info panel (internal)
     * @param numCols {int} # of columns to show (1-2)
     * @param numRows {int} # of rows to show (1-2)
     * @param margin {int} margin between subpanels (title <> text/image)
     * @param useTransparentMaterial {Bool} Use a transparent material (e.g. for image alpha)
     * @todo Optimize display of information in 3D scene; colors
    */
    add3DInformationPlaneToScene(title, text, imageURL, inGuidedTour, clickFunction, clickFunctionParameter, pos, rot, name, numCols, numRows, margin, useTransparentMaterial) {
        if (useTransparentMaterial == null) {
            useTransparentMaterial = true;
        }
        var anchor = new BABYLON.TransformNode("textPanelAnchor");

        //define # columns for the information panel (1 to 2)
        if (numCols == null) { numCols = 1; }
        //define # rows for the information panel (1 to 2)
        if (numRows == null) { numRows = 2; }
        if (margin == null) { margin = 0.1 }

        anchor = this.createInformationPanel3DAnchor(anchor, this.scene.activeCamera, pos, rot, inGuidedTour);

        //if text: show only a text, if imageURL: show only an image
        if (imageURL) {
            //create image button
            var imgButton = this.createGUIMeshButton3D(this.INFOPLANE_3D_WIDTH, this.INFOPLANE_3D_HEIGHT, null, imageURL, useTransparentMaterial, clickFunction, clickFunctionParameter, null, "infoPanelImage", null, true);
            //add button to GUI panel
            this.addUIElementTo3DGUI(imgButton);
            imgButton.linkToTransformNode(anchor);
        }

        if (text) {
            var txtButton = this.createGUIMeshButton3D(this.INFOPLANE_3D_WIDTH, this.INFOPLANE_3D_HEIGHT, text, null, useTransparentMaterial, clickFunction, clickFunctionParameter, null, "infoPanelText");
            this.addUIElementTo3DGUI(txtButton);
            txtButton.linkToTransformNode(anchor);
        }

        //caption
        if (title) {
            var titleHeight = 0.21;
            if(this.TITLEPLANE_3D_HEIGHT) {
                titleHeight = this.TITLEPLANE_3D_HEIGHT;
            }
             //INFOPLANE_3D_HEIGHT/5
            //if title: show title above text, if imageURL: show only an image
            var txtButton2 = this.createGUIMeshButton3D(this.INFOPLANE_3D_WIDTH, titleHeight, title, null, useTransparentMaterial, clickFunction, clickFunctionParameter, null, "infoPanelHeader");
            //panel.addControl(txtButton2);
            this.addUIElementTo3DGUI(txtButton2);
            //margin title<>text/image
            if (margin == null) {
                margin = 0.85;
            }
            //position title above text within container
            txtButton2.position = new BABYLON.Vector3(0, margin, -0.001);
            txtButton2.linkToTransformNode(anchor);

        }
        //fix issue with panel rotation
        //this.bugFixPanelRotation(anchor,this.BUTTON_PANEL_3D_ROTATION.y,null);
    }

    /**
     * @desc Anchor to position a 3D information panel
     * @param anchor {BABYLON.TransformNode} anchor for positioning
     * @param pos {int} position of info panel
     * @param rot {int} rotation of info panel
     * @param inGuidedTour {Bool} whether in guided tour
     * @todo optimize/improve this function
    */
    createInformationPanel3DAnchor(anchor, camera, pos, rot, inGuidedTour) {
        //position the information panel based on given parameters
        if (pos != null && rot != null) {
            anchor.position = pos;
            anchor.rotation = rot;
        } else if (this.INFOPANEL_3D_POSITION == null || this.INFOPANEL_3D_ROTATION == null) {
            //position the information based on the camera's position
            this.removeInformationPanelsFrom3DGUI();
            anchor.position = new BABYLON.Vector3(camera.position.x, camera.position.y, camera.position.z);
            anchor.rotation = new BABYLON.Vector3(0, camera.rotation.y, camera.rotation.z);
        } else if (inGuidedTour) {
            //position the information based on the camera's position
            anchor.position = new BABYLON.Vector3(camera.position.x, camera.position.y, camera.position.z);
            anchor.rotation = new BABYLON.Vector3(0, camera.rotation.y, camera.rotation.z);
        } else {
            //position the information based on predefined position/rotation
            this.removeInformationPanelsFrom3DGUI();
            anchor.position = this.INFOPANEL_3D_POSITION;
            anchor.rotation = this.INFOPANEL_3D_ROTATION;
        }
        //the quaternion is used when in VR - then do not show the image panel (in miniature mode)
        if (camera.deviceRotationQuaternion) {
            //text += '   quatern:' + camera.deviceRotationQuaternion.x.toFixed(2) +',' + camera.deviceRotationQuaternion.y.toFixed(2) + ',' + camera.deviceRotationQuaternion.z.toFixed(2) + ',' + camera.deviceRotationQuaternion.w.toFixed(2);
            anchor.rotationQuaternion = new BABYLON.Quaternion(camera.deviceRotationQuaternion.x, camera.deviceRotationQuaternion.y, camera.deviceRotationQuaternion.z, camera.deviceRotationQuaternion.w);
        }
        return anchor;
    }

    /**
     * @desc remove 3D elements from scene
    */
    remove3DGUIElements() {
        for (var i = 0; i < this.threeDimensionalInterfaceElementsArr.length; i++) {
            var elementToRemove = this.threeDimensionalInterfaceElementsArr[i];
            //do not remove dynamically added 3D elements which were specified in the metadata
            // var infObj = getMeshInformationMetadataObject(getIDFromMeshName(elementToRemove.name));

            if (/*!infObj &&*/ elementToRemove.name != 'button3D') {
                this.gui3DManager.removeControl(this.threeDimensionalInterfaceElementsArr[i]);
                this.threeDimensionalInterfaceElementsArr.splice(i, 1);
            }
        }
    }

    /**
     * @desc create 3D GUI Cylinder panel
     * @param name {Str} internal name
     * @param cols {int} #cols
     * @param rows {int} #rows
     * @param radius {int} radius of cylinder
     * @param margin {int} margins between subpanels
    */
    createGUICylinderPanel(name, cols, rows, radius, margin) {
        var panel = new BABYLON_GUI.CylinderPanel();
        if (name) {
            panel.name = name;
        } else {
            panel.name = 'informationPlane3D';
        }
        //a panel container can contain multiple rows and columns
        panel.columns = cols;
        panel.rows = rows;
        //panel.blockLayout = true;
        panel.radius = radius;
        panel.margin = margin;
        return panel;
    }

    /**
     * @desc Plane to create a button from
     * @param w {int} width
     * @param h {int} height
     * @param name {Str} internal name
    */
    createPlane(w, h, name) {
        if (name == null) {
            name = "billboard";
        }
        var planeForButton = new BABYLON.MeshBuilder.CreatePlane(name, { width: w, height: h }, this.scene);
        return planeForButton;
    }

    /**
     * @desc Create 3D GUI Mesh button
     * @param w {int} width
     * @param h {int} height
     * @param imageURL
     * @param useTransparentMaterial 
     * @param clickFunction
     * @param clickFunctionParameter
     * @param fillStyle {Str} style to fill in panel with (background color, alpha). Alpha not properly working (dynamically) at the moment.
    */
    createGUIMeshButton3D(w, h, text, imageURL, useTransparentMaterial, clickFunction, clickFunctionParameter, fillStyle, name, fontSize, preserveAspectRatio) {
        if (fontSize == null) {
            fontSize = this.INFOPLANE_3D_FONT_SIZE;
        }
        var textureWidth = 1000 * w;
        var textureHeight = 1000 * h;

        //-> first create a plane for the button
        var planeForButton = this.createPlane(w, h, name);

        //-> then convert the plane into a meshbutton3d container
        var customButton = new BABYLON_GUI.MeshButton3D(planeForButton, "pushButton_" + name);
        var buttonMaterial = new BABYLON.StandardMaterial("Mat", this.scene);

        planeForButton.material = buttonMaterial;

        //default: text (even if both text and image defined)
        if (text != null) {
            this.addTextTextureToMaterial(buttonMaterial, textureWidth, textureHeight, text, useTransparentMaterial, fontSize, fillStyle);
        } else {
            this.addImageTextureToMaterial(buttonMaterial, imageURL, planeForButton, preserveAspectRatio);
        }
        this.addBehaviorTo3DButton(customButton, clickFunction, clickFunctionParameter);

        return customButton;
    }

    /**
     * @desc Add "click" functionality to a 3D button (with 0-1 parameters)
    */
    addBehaviorTo3DButton(customButton, clickFunction, clickFunctionParameter) {
        if (customButton == null) {
            console.log('[warning] customButton not defined');
            return null;
        } else {
            //first, remove previous behaviors (if any)
            if (customButton.onPointerUpObservable != null) {
                customButton.onPointerUpObservable.clear();
            }
            //then add current one	
            customButton.onPointerUpObservable.add(function () {
                if (clickFunctionParameter != null) {
                    clickFunction(clickFunctionParameter);
                } else if (clickFunction != null) {
                    clickFunction();
                }
            });
            return customButton;
        }
    }

    /**
     * @desc wrapping text for texturing purposes, used for 3D labels
    */
    wrapText(context, text, x, y, maxWidth, lineHeight) {
        var words = text.split(' ');
        var line = '';
        for (var n = 0; n < words.length; n++) {
            var testLine = line + words[n] + ' ';
            var metrics = context.measureText(testLine);
            var testWidth = metrics.width;
            if (testWidth > maxWidth && n > 0) {
                context.fillText(line, x, y);
                line = words[n] + ' ';
                y += lineHeight;
            } else {
                line = testLine;
            }
        }
        context.fillText(line, x, y);
    }

    /**
     * @desc Remove all information panels from 3D GUI (header, text and image)
    */
    removeInformationPanelsFrom3DGUI() {
        //remove mesh within meshbutton3d container to remove the panels from the scene
        this.removePanelFrom3DGUI('pushButton_infoPanelHeader');
        this.removePanelFrom3DGUI('pushButton_infoPanelText');
        this.removePanelFrom3DGUI('pushButton_infoPanelImage');
    }

    /**
     * @desc Remove all 3D information panels from the 3D GUI
    */
    removeAllPanelsFrom3DGUI() {
        this.removePanelFrom3DGUI('NavCylPan');
        this.removeInformationPanelsFrom3DGUI();
        this.remove3DGUIElements();
    }

    /**
     * @desc Remove a 3D information panel from the 3D GUI
     * @param panelName {Str}
    */
    removePanelFrom3DGUI(panelName) {
        //potentially remove the last element of an array, so count backwards
        for (var i = this.threeDimensionalInterfaceElementsArr.length - 1; i >= 0; i--) {
            var elementToRemove = this.threeDimensionalInterfaceElementsArr[i];
            if (elementToRemove.name == panelName) {
                if (this.gui3DManager.containsControl(elementToRemove)) {
                    this.gui3DManager.removeControl(elementToRemove);
                    this.threeDimensionalInterfaceElementsArr.splice(i, 1);
                }
            }
        }
    }

    /**
     * @desc Add a 3D GUI element to the this.scene and store it in an array with 3D GUI elements
     * @param uiElement {BABYLON_GUI._}
    */
    addUIElementTo3DGUI(uiElement) {
        //add button to GUI manager
        this.gui3DManager.addControl(uiElement);
        //store 3d ui element in arr to later retrieve
        this.threeDimensionalInterfaceElementsArr.push(uiElement);
    }

    /**
     * @desc Create a Material with an image texture (transparency enabled for e.g. PNGs)
     * @param imageURL {str}
     * @return buttonMaterial {BABYLON.StandardMaterial}
    */
    createImageTextureMaterialWithAlpha(imageURL, preserveAspectRatio) {
        //transparent image material
        var buttonMaterial = new BABYLON.StandardMaterial("Mat", this.scene);
        this.addImageTextureToMaterial(buttonMaterial, imageURL, planeForButton, preserveAspectRatio);
        return buttonMaterial;
    }

    /**
     * @desc Add an image texture to a given material
     * @param buttonMaterial {BABYLON.StandardMaterial}
     * @param imageURL {str}
    */
    async addImageTextureToMaterial(buttonMaterial, imageURL, planeForButton, preserveAspectRatio) {
        //hide plane until the texture has been loaded, to avoid black images being shown

        if (planeForButton) {
            planeForButton.visibility = 0;
        }

        //get the image data and dimension information, to be able to extract width/height
        let imgBlobDimArr = await this.fetchImageAsBlob(imageURL);
        var imgBlob = imgBlobDimArr[0];
        //use image dimensions to improve texture display (aspect ratio)
        var imgW = imgBlobDimArr[1];
        var imgH = imgBlobDimArr[2];
        //calculate aspect ratio for image to display and for infoplane in which it will be displayed
        var aspectRatio = imgW / imgH;
        var aspRatioOfInfoPlane = this.INFOPLANE_3D_WIDTH/this.INFOPLANE_3D_HEIGHT;
        //default setting for aspect ratio of displayed image
        var aspectRatioW, aspectRatioH;
        aspectRatioW = aspectRatioH = 1;
        //do some light changes in the aspect ratio when needed
        if(aspRatioOfInfoPlane > 1.5 || aspectRatio < 0.75) {
            //very wide infoplane or very tall image
            aspectRatioW = 0.5;
        }
        if(aspRatioOfInfoPlane < 0.75 || aspectRatio > 1.5 ) {
            //very tall infoplane or very wide image 
            aspectRatioH = 0.5;
        }

        if (planeForButton && preserveAspectRatio == true) {
            planeForButton.scaling = new BABYLON.Vector3(aspectRatioW, aspectRatioH, 1);
        }

        buttonMaterial.diffuseTexture = new BABYLON.Texture(imgBlob, this.scene);
        buttonMaterial.diffuseTexture.hasAlpha = true;

        buttonMaterial.specularTexture = new BABYLON.Texture(imgBlob, this.scene);
        buttonMaterial.specularTexture.hasAlpha = true;

        buttonMaterial.emissiveTexture = new BABYLON.Texture(imgBlob, this.scene);
        buttonMaterial.emissiveTexture.hasAlpha = true;

        buttonMaterial.ambientTexture = new BABYLON.Texture(imgBlob, this.scene);
        buttonMaterial.ambientTexture.hasAlpha = true;

        buttonMaterial.disableLighting = true;

        if (planeForButton) {
            planeForButton.visibility = 1;
        }
    }

    /**
     * @desc Fix an issue: the dimensions of an image are only available after retrieving it. However, for the LOD functionality in VR, the 3D panels must know the dimensions in advance. Therefore, prefetch the images.
     * @param imageURL {str}
    */
    async fetchImageAsBlob(imageURL) {
        return new Promise((resolve, reject) => {
            fetch(imageURL)
                .then(r => r.arrayBuffer())
                .then(buffer => {
                    const blob = new Blob([buffer], { type: this.getMIMEType(imageURL) }) //type: 'image/jpeg'
                    const img = new Image()
                    img.src = URL.createObjectURL(blob)
                    img.onload = function () {
                        resolve([img.src, img.width, img.height]);
                    }
                }).catch((error) => {
                    console.error('Error:', error);
                });
        })
    }

    /**
     * @desc get the MIME type for a certain image
     * @todo Could be improved .. for linked data case
     * @param buttonMaterial {BABYLON.StandardMaterial}
     * @param imageURL {str}
    */
    getMIMEType(imgURL) {
        if (imgURL.toLowerCase().endsWith('.jpg')) {
            return 'image/jpg';
        } else if (imgURL.toLowerCase().endsWith('.png')) {
            return 'image/png';
        } else if (imgURL.toLowerCase().endsWith('.svg')) {
            return 'image/svg+xml';
        } else {
            //console.log('[warning] Unknown MIME type (getMIMEType) for: ' + imgURL);
            //set standard case for LOD: .jpg
            return 'image/jpg';
        }
    }

    /**
     * @desc Add a textual texture to a given material
     * @param buttonMaterial {BABYLON.StandardMaterial}
     * @param textureWidth {int}
     * @param textureHeight {int}
     * @param text {str}
     * @param hasAlpha {bool}
    */
    addTextTextureToMaterial(buttonMaterial, textureWidth, textureHeight, text, hasAlpha, fontSize, fillStyle) {
        var buttonTexture = new BABYLON.DynamicTexture("dynamic texture", { width: textureWidth, height: textureHeight }, this.scene);

        if (hasAlpha == null) {
            hasAlpha = true;
        }

        buttonTexture.hasAlpha = hasAlpha;

        //disable lighting to prevent the texture picking up regular lights in the scene
        buttonMaterial.disableLighting = true;

        buttonMaterial.diffuseTexture = buttonTexture;

        var textureContext = buttonTexture.getContext();
        textureContext.font = "normal " + fontSize + "px 'Open Sans',Calibri,Arial";
        textureContext.save();

        //some issues with custom rgb opacity, to be checked
        //background
        if (fillStyle == null) {
            textureContext.fillStyle = "rgba(0, 0, 0, 0.5)";
        } else {
            textureContext.fillStyle = fillStyle;
        }
        textureContext.fillRect(0, 0, textureWidth, textureHeight);

        //text
        textureContext.fillStyle = "white";
        textureContext.textAlign = "centre";

        //use wrapText function to enable multiline functionality
        this.wrapText(textureContext, text, 25, fontSize * 1.15, textureWidth - 25, fontSize * 1.15);
        textureContext.restore();
        buttonTexture.update();

        buttonMaterial.opacityTexture = buttonTexture;
        buttonMaterial.emissiveTexture = buttonTexture;
    }

    /**
     * @desc Bugfix for BabylonJS issue with panel rotation. See: https://forum.babylonjs.com/t/how-to-rotate-planepanel-or-any-volume-based-panel/13949/3
     * @param selectedMeshName {str} Name of mesh that has been selected in the scene
    */
    bugFixPanelRotation(panel, yRotationInRadians, yRotationInDegrees) {
        //rotation has to be done after object creation, otherwise panel doesn't appear correctly anymore
        if (yRotationInRadians != null) {
            yRotationInDegrees = yRotationInRadians * 180 / Math.PI;
        }
        BABYLON.Tools.SetImmediate(() => panel.node.rotate(BABYLON.Axis.Y, yRotationInDegrees * Math.PI / 180, BABYLON.Space.LOCAL));
    }

}

export default GUI3D;