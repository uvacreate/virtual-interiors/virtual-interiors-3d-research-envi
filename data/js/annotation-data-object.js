/**
 * @classdesc Annotation class manages storage of annotations, in a custom triple format (currently only saved for duration of session)
 */
 class AnnotationDataObject {
    constructor(linkedDataStorage, id) {
        this._linkedDataStorage = linkedDataStorage;
        this._id = id;
    }

    /**
    * @desc Set the text of a user annotation
    */
    set userAnnotation_text(value) {
        this.saveData(this._id, "hasUserAnnotationText", value);
    }

    /**
    * @desc Get the text of a user annotation
    */
    get userAnnotation_text() {
        return this.retrieveData(this._id, "hasUserAnnotationText");
    }

    /**
    * @desc Generic save function to save a data item
    */
    saveData(subj, rel, obj, biDir) {
        this._linkedDataStorage.saveUniqueTriple(subj, rel, obj, biDir)
    }

    /**
    * @desc Generic retrieve function to retrieve a data item
    */
    retrieveData(subj, rel) {
        return this._linkedDataStorage.filterTriplesBySubjectRelation(subj,rel);
    }
}

export default AnnotationDataObject;