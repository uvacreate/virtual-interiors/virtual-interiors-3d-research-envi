#script to run the application

echo ""
echo "2. running Virtual Interiors 3D application"
echo ""
echo "Notes:"
echo "-App selection: localhost:3000/index.html?app=sample [app name]"
echo "-Low quality mode: localhost:3000/index.html?app=1&lq=true [true/false]"
echo "-Camera selection: localhost:3000/index.html?cam=first-person [orbit, first-person, vr-first-person, phone-first-person, cardboard-vr-first-person]"

npm start